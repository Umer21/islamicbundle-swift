//
//  TimelineVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import GooglePlaces
import SQLite
import CoreLocation
import GoogleMaps
import Adhan
import UserNotifications

class TimelineVC: UIViewController {
    let resueIdentifierImageCell = "ImageCell"
    let resueIdentifierIslamicBundleCell = "IslamicBundleCell"
    let resueIdentifierChannelCell = "ChannelCell"
    let resueIdentifierVerseCell = "VerseCell"
    let resueIdentifierGreetingCell = "GreetingCell"
    let resueIdentifierDuaCell = "DuaCell"
    let resueIdentifierNamesCell = "NamesCell"
    let resueIdentifierRabanaCell = "RabanaCell"
    let resueIdentifierAllahNamesCell = "AllahNamesCell"
    let resueIdentifierShahadahCell = "ShahadahCell"
    let resueIdentifierRamdanCell = "RamdanCell"
    
    
    var indicator = UIActivityIndicatorView()
    let locationManager = CLLocationManager()
    let geocoder = GMSGeocoder()
    
    var Lat = "12.2122"
    var Long = "65.3434"
    
    var dayLeftRamdan = 0
    var isShowRamzanDaysLeft = false
    var ramzanDateString = ""
    var isShowRamzanDay = false
    var ramzanTimingShare = ""
    
    
    var arrAllahNames = [AllahName]()
    var arrQuran = [quran]()
    var arrDuas = [Hisnul]()
    var arrBoynames = [BoyNames]()
    var arrRabbana = [Rabbana]()
    var counter = 30
    
    //var arrQuran = []()
    let arrNamazName = ["Fajr","Sunrise"]
    let arrNamazNameMinutes = [02,04]
    var strTZ: String = ""
    
    var prayerIndex: Int?
    var prayerName: String?
    
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBAction func locationTapped(_ sender: Any) {
        
        showActionSheetForLocation()
    }
    
    @IBOutlet weak var locationButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCalendarRamzan()
        
        let currentDate = Date().string(format: "yyyy-MM-dd")
        let currentDateForNames = Date().string(format: "M/d/yyyy")
        //printMyFonts()
        tblView.separatorStyle = .none
        arrAllahNames = DatabaseHelper.getAllahNames()
        arrQuran = DatabaseHelper.getDataWithTranslationByDates(date: currentDate, current_table: "abc")
        arrDuas = DatabaseHelper.getDetailDuaRandom()
        arrBoynames = DatabaseHelper.getNameByDate(date: currentDateForNames)
        arrRabbana = DatabaseHelper.getRandomRabbanaInfo()
        
    }
    
    func getCalendarRamzan(){
        let date = Date()
        
        var dateComponents = DateComponents()
        dateComponents.year = 2023
        dateComponents.month = 3
        dateComponents.day = 23
        
        let userCalendar = Calendar(identifier: .gregorian)
        let ramzanDateTime = userCalendar.date(from: dateComponents)!
        
        ramzanDateString = ramzanDateTime.string(format: "dd MMMM yyyy")
        dayLeftRamdan = daysBetween(start: date, end: ramzanDateTime)
        
        print("Days Left = \(dayLeftRamdan)")
        
        if dayLeftRamdan < 25 {
            isShowRamzanDaysLeft = true
        }else{
            isShowRamzanDaysLeft = false
        }
        
        if dayLeftRamdan <= 0 {
            isShowRamzanDay = true
        }
        
        //isShowRamzanDaysLeft = false
        
    }
    
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        setNavBar()
        
        DispatchQueue.main.asyncAfter(deadline: .now() +  10.0, execute: {
            print("--------TimeLineVC-ViewWillAppear-----")
              resetNotificationsIfCountZero()
        })
        
        self.tabBarController?.tabBar.isHidden = false
        
        tblView.reloadData()
        
    }
    
    func gotoSettings() {
        let alertController = UIAlertController (title: "Location service are off. Please allow to fetch resturants.", message: "Go to Settings?", preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func setNavBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let settingButton = UIBarButtonItem(image: UIImage(named: "icon_nav_setting"), style: .plain, target: self, action: #selector(settingsTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [settingButton]
        //self.navigationController?.navigationBar.tintColor = kPurpleColor
        //self.navigationItem.title = "Dashboard"
        var currentLoc: String = ""
        if isKeyPresentInUserDefaults(key: "CurrentLocation"){
            currentLoc = getValueForKey(keyValue: "CurrentLocation")
        }
        else{
            currentLoc = "Mecca, Saudia Arabia"
        }
        
        
        self.locationButton .setTitle("\(currentLoc)", for: .normal)
        self.locationButton.titleLabel?.lineBreakMode = .byTruncatingTail
    }
    
    @objc func settingsTapped() {
        gotoSettingsScreen()
    }
    
    func gotoSettingsScreen() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingsConttollerID) as! SettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getCurrentLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //If user denied location services
        let status = CLLocationManager.authorizationStatus()
        if status == .denied {
            gotoSettings()
        }
    }
    
    func getaddress(latitude: Double, longitude: Double) {
        
        let coordinate = CLLocationCoordinate2DMake(latitude,longitude)
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                self.locationButton .setTitle("\(lines[0])", for: .normal)
                self.locationButton.titleLabel?.lineBreakMode = .byTruncatingTail
                //print("-------\(lines[0])")
                //currentAdd(returnAddress: currentAddress)
                
                setDefaultValue(keyValue: "CurrentLocation", valueIs: lines[0])
                
                resetPrayerNotificationsOnSettingsChange()
            }
            
            self.locationManager.stopUpdatingLocation()
        }
        
        tblView.reloadData()
    }
    
    func showActionSheetForLocation() {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: nil, message: "Select your location", preferredStyle: .actionSheet)
        
        let cameraActionButton = UIAlertAction(title: "Locate Me", style: .default)
        { action -> Void in
            print("Locate me")
            self.getCurrentLocation()
            
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton = UIAlertAction(title: "Set Location Manually", style: .default)
        { action -> Void in
            print("Set Location Manually")
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.navigationController?.navigationBar.barTintColor = kPurpleColor
            autocompleteController.delegate = self
            //UINavigationBar.appearance().tintColor = UIColor.white
            //let searchBarTextAttributes: [String : AnyObject] = [NSAttributedString.Key.foregroundColor.rawValue: UIColor.white, NSAttributedString.Key.font.rawValue: UIFont.systemFont(ofSize: UIFont.systemFontSize)]
            
            //UITextField.appearance(whenContainedInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
            //UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
            
            self.present(autocompleteController, animated: true, completion: nil)
            
        }
        actionSheetController.addAction(galleryActionButton)
        
        let dismissActionButton = UIAlertAction(title: "Dismiss", style: .cancel) { action -> Void in
            print("Dismiss")
        }
        actionSheetController.addAction(dismissActionButton)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            actionSheetController.popoverPresentationController?.sourceView = self.locationButton
            actionSheetController.popoverPresentationController!.sourceRect = CGRect(x: 0,y: -10,width: 1.0,height: 1.0)
            
        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension TimelineVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations ==== \(locValue.latitude) \(locValue.longitude)")
        // print("place name = \(latLong(lat: Double (locValue.latitude), long: Double(locValue.longitude)))")
        
        indicator.stopAnimating()
        //apiResturants(Lat: "\(locValue.latitude)", Long: "\(locValue.longitude)")
        Lat = "\(locValue.latitude)"
        //print("Latitude === -----------------\(Lat)")
        Long = "\(locValue.longitude)"
        //print("Longitude=== -------------------\(Long)")
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (locValue.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (locValue.longitude))
        setTimeZone(lat: locValue.latitude, lng: locValue.longitude)
        
        NotificationCenter.default.post(name: Notification.Name("UPDATE_PRAYER_TIMINGS"), object: nil, userInfo: nil)
       /* let location = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
        let timeZone = TimezoneMapper.latLngToTimezone(location)
        
        if let tz = timeZone {
            
            print("---before-splitted--\(String(describing: tz))")
            
            let s = String(describing: tz)
            let splittedTz = s.split(separator: " ")
            
            setDefaultValue(keyValue: "TimeZone", valueIs: String(splittedTz[0]))
            print("--after--splitted---\(splittedTz[0])")
        }
        
        print("----get-timeZone---\(getValueForKey(keyValue: "TimeZone"))")*/
        getaddress(latitude: locValue.latitude, longitude: locValue.longitude)
    }
}

extension TimelineVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isShowRamzanDaysLeft {
            return 11
        }else{
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isShowRamzanDaysLeft {
            
            if indexPath.row == 0 {
                return 220
            }else if indexPath.row == 1 {
                return 220
            }else if indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 {
                return 340
            } else if indexPath.row == 4 || indexPath.row == 6 || indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 {
                return UITableView.automaticDimension
            }else{
                return 350
            }
            
        }else{
            if indexPath.row == 0 {
                return 220
            }else if indexPath.row == 1{
                return 220
            }else if indexPath.row == 2 || indexPath.row == 4 {
                return 340
            } else if indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 8 {
                return UITableView.automaticDimension
            }else if indexPath.row == 7 {
                return UITableView.automaticDimension
            }else{
                return 350
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isShowRamzanDaysLeft {
            
            if (indexPath.row == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierImageCell, for: indexPath as IndexPath) as! ImageCell
            
                cell.selectionStyle = .none
                //cell.lblCountDownTime.text = "12:00:00"
                let currentDate = Date().string(format: "dd MMMM yyyy")
                cell.lblDate.text = currentDate
                cell.lblIslamicDate.text = getHijriDate(gregorianDate: currentDate)
                
                //cell.lblNamaz.text =
                //yyyy-MM-dd HH:mm:ssZ
                
                
                let cpdetail = getCurrentPrayerDetail()
                print(cpdetail)
                
                switch cpdetail[0].NextNamazName! {
                case Prayer.fajr:
                    print("Fajr")
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Fajr")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Fajr"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 0
                    prayerName = "Fajr"
                    break
                case Prayer.sunrise:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Sunrise")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Sunrise"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 1
                    prayerName = "Sunrise"
                    break
                case Prayer.dhuhr:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Dhuhr")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Dhuhr"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 2
                    prayerName = "Dhuhr"
                    break
                case Prayer.asr:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Asr")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Asr"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 3
                    prayerName = "Asr"
                    break
                case Prayer.maghrib:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Maghrib")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Maghrib"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 4
                    prayerName = "Maghrib"
                    break
                case Prayer.isha:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Isha")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Isha"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 5
                    prayerName = "Isha"
                    break
                default:
                    print("no Namaz")
                }
                
                let alarmStatus = getNamazAlarmValue(index: prayerIndex!, namaz: prayerName!)
                print("Alarm---prayerIndex: \(prayerIndex)")
                print("Alarm---prayerName: \(prayerName)")
                print("Alarm---status: \(alarmStatus)")
                if alarmStatus == "1"{
                    cell.btnImgAlarm.setImage(UIImage(named: "icon_dashboard_bell"), for: .normal)
                }
                else{
                    cell.btnImgAlarm.setImage(UIImage(named: "icon_dashboard_unbell"), for: .normal)
                }
                
                cell.btnImgAlarm.addTarget(self, action: #selector(fAlarmChangeStatus), for: .touchUpInside)
                cell.btnFullClick.addTarget(self, action: #selector(GotoPrayerScreen), for: .touchUpInside)
                
                
                return cell
                
            }else if (indexPath.row == 1) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierIslamicBundleCell, for: indexPath as IndexPath) as! IslamicBundleCell
                
                cell.btnShare.addTarget(self, action: #selector(shareText), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 2) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierRamdanCell, for: indexPath as IndexPath) as! RamdanCell
                
                /*
                let aftarTime = getValueForKey(keyValue: "ComingIftarTime")
                let sehrTime = getValueForKey(keyValue: "ComingFajarTime")
                cell.lblDaysLeft.text = "Iftaar\n \(aftarTime) and Sehr at \(sehrTime)"
                */
                let aftarTime = getValueForKey(keyValue: "ComingIftarTime")
                let sehrTime = getValueForKey(keyValue: "ComingFajarTime")
                
                if isShowRamzanDay == false {
                    cell.lblDate.isHidden = false
                    cell.lblDaysLeft.text = "\(self.dayLeftRamdan) days left"
                    cell.lblDate.text = self.ramzanDateString
                    
                }else{
                    cell.lblDate.isHidden = true
                    cell.lblDaysLeft.text = "Iftar\n \(aftarTime) and Sehr at \(sehrTime)"
                }
                
                ramzanTimingShare = "Iftar\n \(aftarTime) and Sehr at \(sehrTime)"
                
                cell.btnShare.addTarget(self, action: #selector(shareRamadanTimming), for: .touchUpInside)
                cell.btnView.addTarget(self, action: #selector(gotoFastingTimeScreen), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 3) {
                
                
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierChannelCell, for: indexPath as IndexPath) as! ChannelCell
                
                cell.btnShareChannel.addTarget(self, action: #selector(shareTextChannel), for: .touchUpInside)
                
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 4) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierVerseCell, for: indexPath as IndexPath) as! VerseCell
                
                let quran_verse = arrQuran
                cell.lblSurahInfo.text = quran_verse[0].Surah_Name! + "(" + String(quran_verse[0].Surah_No!) + ":" + String (quran_verse[0].Ayah_No!) + ")"
                
                cell.lblSurahArabic.text = quran_verse[0].Ayat
                cell.lblSurahTranslation.text = quran_verse[0].En_Translation!
                
                
                
                let shareText = ("\(quran_verse[0].Surah_Name ?? "") (\(quran_verse[0].Surah_No ?? 00):\( quran_verse[0].Ayah_No ?? 00))\n\n\(quran_verse[0].Ayat ?? "")\n\n\( quran_verse[0].En_Translation ?? "")")
                
                cell.btnShareQuran.accessibilityHint = shareText
                cell.btnShareQuran.addTarget(self, action: #selector(shareTextDailyVerse), for: .touchUpInside)
                
                cell.btnViewMoreQuran.addTarget(self, action: #selector(ViewMoreDailyVerse), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 5) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierGreetingCell, for: indexPath as IndexPath) as! GreetingCell
                
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 6) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierDuaCell, for: indexPath as IndexPath) as! DuaCell
                
                let quran_duas = arrDuas
                cell.lblDuaArabic.text = quran_duas[0].Dua
                cell.lblDuaTranslation.text = quran_duas[0].Translation
                
                let shareText = ("\(quran_duas[0].Dua ?? "")\n\n\(quran_duas[0].Translation ?? "")")
                
                cell.btnShareDua.accessibilityHint = shareText
                
                cell.btnShareDua.addTarget(self, action: #selector(shareTextDua), for: .touchUpInside)
                cell.btnViewMoreDua.addTarget(self, action: #selector(ViewMoreDailyDua), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 7) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierNamesCell, for: indexPath as IndexPath) as! NamesCell
                
                let names = arrBoynames
                if names.count > 0 {
                    cell.lblNameArabic.text = names[0].NameArabic ?? ""
                    cell.lblNameEnglish.text = names[0].BoyName
                    cell.lblMeaning.text = names[0].NameMeaning
                    
                    let shareText = ("\(names[0].NameArabic ?? "")\n\n\(names[0].BoyName ?? "")\n\n\(names[0].NameMeaning ?? "")")
                    
                    cell.btnShareKidsName.accessibilityHint = shareText
                }
                
                cell.btnShareKidsName.addTarget(self, action: #selector(shareTextKidsName), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
            }else if (indexPath.row == 8) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierRabanaCell, for: indexPath as IndexPath) as! RabanaCell
                
                let rabbana = arrRabbana
                cell.lblRabbanaArabic.text = rabbana[0].RabbanaArabic
                cell.lblRabbanaTranslation.text = rabbana[0].RabbanaTranslation
                
                let shareText = ("\(rabbana[0].RabbanaArabic ?? "")\n\n\(rabbana[0].RabbanaTranslation ?? "")")
                
                cell.btnShareRabbana.accessibilityHint = shareText
                
                cell.btnShareRabbana.addTarget(self, action: #selector(shareTextRabbana), for: .touchUpInside)
                
                cell.btnViewMoreRabbana.addTarget(self, action: #selector(ViewMoreRabbana), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
            }else if (indexPath.row == 9) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierAllahNamesCell, for: indexPath as IndexPath) as! AllahNamesCell
                
                
                if let randomItem = arrAllahNames.randomItem() {
                    cell.lblName.text = randomItem.name!
                    cell.lbldesc.text = randomItem.desc!
                    cell.imgView.image = UIImage(named: "name_\(String(describing: randomItem.Id!))")
                    
                    let shareText = ("\(randomItem.name ?? "")\n\n\(randomItem.desc ?? "")")
                    
                    cell.btnShareNames.accessibilityHint = shareText
                }
                cell.imgView.image = cell.imgView.image?.withRenderingMode(.alwaysTemplate)
                cell.imgView.tintColor = UIColor.white
                
                cell.btnShareNames.addTarget(self, action: #selector(shareTextNames), for: .touchUpInside)
                cell.btnViewMoreAllahNames.addTarget(self, action: #selector(ViewMoreAllahNames), for: .touchUpInside)
                
                
                cell.selectionStyle = .none
                return cell
            }
            else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierShahadahCell, for: indexPath as IndexPath) as! ShahadahCell
                
                cell.btnShareShahadah.addTarget(self, action: #selector(shareImageShahadah), for: .touchUpInside)
                cell.btnViewShahadah.addTarget(self, action: #selector(ViewMoreShahadah), for: .touchUpInside)
                
                cell.selectionStyle = .none
                return cell
            }
        }else{
            if (indexPath.row == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierImageCell, for: indexPath as IndexPath) as! ImageCell
                
                
                
                cell.selectionStyle = .none
                //cell.lblCountDownTime.text = "12:00:00"
                let currentDate = Date().string(format: "dd MMMM yyyy")
                cell.lblDate.text = currentDate
                cell.lblIslamicDate.text = getHijriDate(gregorianDate: currentDate)
                
                //cell.lblNamaz.text =
                //yyyy-MM-dd HH:mm:ssZ
                
                
                let cpdetail = getCurrentPrayerDetail()
                print(cpdetail)
                
                switch cpdetail[0].NextNamazName! {
                case Prayer.fajr:
                    print("Fajr")
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Fajr")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Fajr"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 0
                    prayerName = "Fajr"
                    break
                case Prayer.sunrise:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Sunrise")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Sunrise"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 1
                    prayerName = "Sunrise"
                    break
                case Prayer.dhuhr:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Dhuhr")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Dhuhr"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 2
                    prayerName = "Dhuhr"
                    break
                case Prayer.asr:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Asr")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Asr"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 3
                    prayerName = "Asr"
                    break
                case Prayer.maghrib:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Maghrib")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Maghrib"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 4
                    prayerName = "Maghrib"
                    break
                case Prayer.isha:
                    
                    setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Isha")
                    setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
                    
                    cell.lblNamaz.text = "Isha"
                    cell.lblTime.text = cpdetail[0].NextNamazTime!
                    
                    prayerIndex = 5
                    prayerName = "Isha"
                    break
                default:
                    print("no Namaz")
                }
                
                let alarmStatus = getNamazAlarmValue(index: prayerIndex!, namaz: prayerName!)
                print("Alarm---prayerIndex: \(prayerIndex)")
                print("Alarm---prayerName: \(prayerName)")
                print("Alarm---status: \(alarmStatus)")
                if alarmStatus == "1"{
                    cell.btnImgAlarm.setImage(UIImage(named: "icon_dashboard_bell"), for: .normal)
                }
                else{
                    cell.btnImgAlarm.setImage(UIImage(named: "icon_dashboard_unbell"), for: .normal)
                }
                
                cell.btnImgAlarm.addTarget(self, action: #selector(fAlarmChangeStatus), for: .touchUpInside)
                cell.btnFullClick.addTarget(self, action: #selector(GotoPrayerScreen), for: .touchUpInside)
                
                
                return cell
                
            }else if (indexPath.row == 1) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierIslamicBundleCell, for: indexPath as IndexPath) as! IslamicBundleCell
                
                cell.btnShare.addTarget(self, action: #selector(shareText), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 2) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierChannelCell, for: indexPath as IndexPath) as! ChannelCell
                
                cell.btnShareChannel.addTarget(self, action: #selector(shareTextChannel), for: .touchUpInside)
                
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 3) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierVerseCell, for: indexPath as IndexPath) as! VerseCell
                
                let quran_verse = arrQuran
                cell.lblSurahInfo.text = quran_verse[0].Surah_Name! + "(" + String(quran_verse[0].Surah_No!) + ":" + String (quran_verse[0].Ayah_No!) + ")"
                
                cell.lblSurahArabic.text = quran_verse[0].Ayat
                cell.lblSurahTranslation.text = quran_verse[0].En_Translation!
                
                
                
                let shareText = ("\(quran_verse[0].Surah_Name ?? "") (\(quran_verse[0].Surah_No ?? 00):\( quran_verse[0].Ayah_No ?? 00))\n\n\(quran_verse[0].Ayat ?? "")\n\n\( quran_verse[0].En_Translation ?? "")")
                
                cell.btnShareQuran.accessibilityHint = shareText
                cell.btnShareQuran.addTarget(self, action: #selector(shareTextDailyVerse), for: .touchUpInside)
                
                cell.btnViewMoreQuran.addTarget(self, action: #selector(ViewMoreDailyVerse), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 4) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierGreetingCell, for: indexPath as IndexPath) as! GreetingCell
                
                cell.delegate = self
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 5) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierDuaCell, for: indexPath as IndexPath) as! DuaCell
                
                let quran_duas = arrDuas
                cell.lblDuaArabic.text = quran_duas[0].Dua
                cell.lblDuaTranslation.text = quran_duas[0].Translation
                
                let shareText = ("\(quran_duas[0].Dua ?? "")\n\n\(quran_duas[0].Translation ?? "")")
                
                cell.btnShareDua.accessibilityHint = shareText
                
                cell.btnShareDua.addTarget(self, action: #selector(shareTextDua), for: .touchUpInside)
                cell.btnViewMoreDua.addTarget(self, action: #selector(ViewMoreDailyDua), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
                
            }else if (indexPath.row == 6) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierNamesCell, for: indexPath as IndexPath) as! NamesCell
                
                let names = arrBoynames
                cell.lblNameArabic.text = names[0].NameArabic ?? ""
                cell.lblNameEnglish.text = names[0].BoyName
                cell.lblMeaning.text = names[0].NameMeaning
                
                let shareText = ("\(names[0].NameArabic ?? "")\n\n\(names[0].BoyName ?? "")\n\n\(names[0].NameMeaning ?? "")")
                
                cell.btnShareKidsName.accessibilityHint = shareText
                
                cell.btnShareKidsName.addTarget(self, action: #selector(shareTextKidsName), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
            }else if (indexPath.row == 7) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierRabanaCell, for: indexPath as IndexPath) as! RabanaCell
                
                let rabbana = arrRabbana
                cell.lblRabbanaArabic.text = rabbana[0].RabbanaArabic
                cell.lblRabbanaTranslation.text = rabbana[0].RabbanaTranslation
                
                let shareText = ("\(rabbana[0].RabbanaArabic ?? "")\n\n\(rabbana[0].RabbanaTranslation ?? "")")
                
                cell.btnShareRabbana.accessibilityHint = shareText
                
                cell.btnShareRabbana.addTarget(self, action: #selector(shareTextRabbana), for: .touchUpInside)
                
                cell.btnViewMoreRabbana.addTarget(self, action: #selector(ViewMoreRabbana), for: .touchUpInside)
                cell.selectionStyle = .none
                return cell
                
            }else if (indexPath.row == 8) {
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierAllahNamesCell, for: indexPath as IndexPath) as! AllahNamesCell
                
                
                if let randomItem = arrAllahNames.randomItem() {
                    cell.lblName.text = randomItem.name!
                    cell.lbldesc.text = randomItem.desc!
                    cell.imgView.image = UIImage(named: "name_\(String(describing: randomItem.Id!))")
                    
                    let shareText = ("\(randomItem.name ?? "")\n\n\(randomItem.desc ?? "")")
                    
                    cell.btnShareNames.accessibilityHint = shareText
                }
                cell.imgView.image = cell.imgView.image?.withRenderingMode(.alwaysTemplate)
                cell.imgView.tintColor = UIColor.white
                
                cell.btnShareNames.addTarget(self, action: #selector(shareTextNames), for: .touchUpInside)
                cell.btnViewMoreAllahNames.addTarget(self, action: #selector(ViewMoreAllahNames), for: .touchUpInside)
                
                
                cell.selectionStyle = .none
                return cell
            }
            else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: resueIdentifierShahadahCell, for: indexPath as IndexPath) as! ShahadahCell
                
                cell.btnShareShahadah.addTarget(self, action: #selector(shareImageShahadah), for: .touchUpInside)
                cell.btnViewShahadah.addTarget(self, action: #selector(ViewMoreShahadah), for: .touchUpInside)
                
                cell.selectionStyle = .none
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            //gotoMyProfileScreen()
        }
        
        if indexPath.row == 1 {
            if indexPath.row == 0 {
                //gotoFiltersScreen()
            }
            if indexPath.row == 1 {
                //gotoFieldsToShow()
            }
            if indexPath.row == 2 {
                //gotoMyQRScreen()
            }
        }
        
        if indexPath.section == 3 {
            if indexPath.row == 0 {
                //logoutMethod()
            }
        }
    }
    
    func gotoDuaCategoryScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kDailyDuaListViewControllerID) as? DailyDuaListVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoSurahListScreen() {
        let vc = UIStoryboard.init(name: kQuranSurahListStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kQuranSurahListControllerID) as? QuranSurahListVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func gotoFastingTimeScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kFastingTimingsControllerID) as? FastingTimingsVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func shareText() {
        
        let shareText = "Islamic Bundle (Powered by Inabia) \n\nThe popular Muslim Community App. Islamic Bundle is all in one Islamic applications and consists of different features. \n\nDownload app now \nIslamic Bundle: \nhttps://itunes.apple.com/us/app/myapp/idxxxxxxxx?ls=1&mt=8"
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func shareRamadanTimming(){
        let shareText = ramzanTimingShare
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func GotoPrayerScreen() {
        
       self.tabBarController?.selectedIndex = 1
    }
    
    @objc func fAlarmChangeStatus() {
        
        let alarmStatus = getNamazAlarmValue(index: prayerIndex!, namaz: prayerName!)
        print("---------alarm-status------\(alarmStatus)")
        if alarmStatus == "1"{
            setNamazAlarmValue(index: prayerIndex!, namaz: prayerName!, value: "0")
            setDefaultValue(keyValue: "Sound-" + prayerName!, valueIs: "0")
        }
        else{
            setNamazAlarmValue(index: prayerIndex!, namaz: prayerName!, value: "1")
            setDefaultValue(keyValue: "Sound-" + prayerName!, valueIs: "1")
        }
        
        tblView.reloadData()
        
       /*if posit == 0 {
           setNamazAlarmValue(index: currentPosition!, namaz: currentPrayer!, value: "0")
       }
       else{
           setNamazAlarmValue(index: currentPosition!, namaz: currentPrayer!, value: "1")
       }*/
    }
    
    @objc func shareTextChannel() {
        
        let shareText = "https://www.youtube.com/watch?v=YftMup4fmDg"
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
    }
    
    func gotoLiveChannelScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kLiveChannelControllerID) as! LiveChannelVC
        //vc.fromType = currPrayer
        //vc.currentPosition = currPosition
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func shareTextDailyVerse(sender: UIButton) {
        
        let shareText = sender.accessibilityHint
        
        let vc = UIActivityViewController(activityItems: [shareText ?? ""], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func ViewMoreDailyVerse(sender: UIButton) {
        gotoSurahListScreen()
        
    }
    
    @objc func ViewMoreDailyDua(sender: UIButton) {
        gotoDuaCategoryScreen()
        
    }
    
    @objc func ViewMoreRabbana(sender: UIButton) {
        self.tabBarController?.selectedIndex = 3
        
    }
    
    @objc func ViewMoreAllahNames(sender: UIButton) {
        gotoNameListScreen()
        
    }
    
    @objc func ViewMoreShahadah(sender: UIButton) {
        gotoShahadahScreen()
        
    }
    
    func gotoShahadahScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kShahadahViewControllerID) as? ShahadahVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoNameListScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kName99ListViewControllerID) as? Allah99NamesVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func shareTextDua(sender: UIButton) {
        
        let shareText = sender.accessibilityHint
        
        let vc = UIActivityViewController(activityItems: [shareText ?? ""], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func shareTextKidsName(sender: UIButton) {
        
        let shareText = sender.accessibilityHint
        
        let vc = UIActivityViewController(activityItems: [shareText ?? ""], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func shareTextRabbana(sender: UIButton) {
        
        let shareText = sender.accessibilityHint
        
        let vc = UIActivityViewController(activityItems: [shareText ?? ""], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func shareTextNames(sender: UIButton) {
        
        let shareText = sender.accessibilityHint
        
        let vc = UIActivityViewController(activityItems: [shareText ?? ""], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func shareImageShahadah() {
        
        if let image = UIImage(named: "bg_shahadah_dashboard") {
            let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
            present(vc, animated: true)
        }
    }
    
}
extension TimeInterval{
    
    func stringFromTimeInterval() -> String {
        
        let time = NSInteger(self)
        
        let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        let hours = (time / 3600)
        
        return String(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms)
        
    }
}

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if isKeyPresentInUserDefaults(key: "TimeZone"){
            let tz = getValueForKey(keyValue: "TimeZone")
            formatter.timeZone = TimeZone(identifier: tz)!
        }
        else{
            let tz = "Asia/Riyadh"
            formatter.timeZone = TimeZone(identifier: tz)!
        }
        //formatter.timeZone = TimeZone(identifier: tz)
        
        return formatter.string(from: self)
    }
}



extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

extension TimelineVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name)) \(place.formattedAddress)")
        print("Components \(String(describing: place.addressComponents))")
        locationButton.setTitle(place.formattedAddress, for: .normal)
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (place.coordinate.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (place.coordinate.longitude))
        
        setDefaultValue(keyValue: "CurrentLocation", valueIs: place.formattedAddress!)
        
        setTimeZone(lat: place.coordinate.latitude, lng: place.coordinate.longitude)
        
        tblView.reloadData()
        
        resetPrayerNotificationsOnSettingsChange()
        
        NotificationCenter.default.post(name: Notification.Name("UPDATE_PRAYER_TIMINGS"), object: nil, userInfo: nil)
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension TimelineVC: GreetingsDelegate {
    func btnTapped(tag: Int) {
        
        print("Menu Clicked")
        print("Menu-Pos--\(tag)")
        
        var shareText = "ahlan_wasahlan"
        
        switch tag {
        case 0:
            shareText = "ahlan_wasahlan"
        case 1:
            shareText = "allahu_akbar"
        case 2:
            shareText = "ameen"
        case 3:
            shareText = "bismillah"
        case 4:
            shareText = "greeting_alhumdulillah"
        case 5:
            shareText = "greeting_aoa"
        case 6:
            shareText = "greeting_eid"
        case 7:
            shareText = "greeting_jazakallah"
        case 8:
            shareText = "greeting_qadar"
        case 9:
            shareText = "greeting_ramadan"
        case 10:
            shareText = "greeting_ramadan"
        case 11:
            shareText = "Inshallah"
        case 12:
            shareText = "subhanallah"
        default:
            shareText = "ahlan_wasahlan"
        }
        
        print("tag-----> \(tag)")
        print("Image name---. \(shareText)")
         
        if let image = UIImage(named: shareText) {
             let vc = UIActivityViewController(activityItems: [image], applicationActivities: [])
             present(vc, animated: true)
         }
        
    }
}

extension TimelineVC: YoutubeDelegate {
    func collectionTapped(tag: Int) {
        
        print("Collection Clicked")
        print("Collection-Pos--\(tag)")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kLiveChannelControllerID) as! LiveChannelVC
        vc.fromType = tag
        //vc.currentPosition = currPosition
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

