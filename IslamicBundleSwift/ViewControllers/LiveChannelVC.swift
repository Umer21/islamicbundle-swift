//
//  LiveChannelVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 25/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import YouTubePlayer
import Alamofire
import SwiftyJSON

class LiveChannelVC: UIViewController {

    
    @IBOutlet weak var youtubePlayer: YouTubePlayerView!
    var fromType: Int = 0
    var url = "http://islamicbundle.com/tv/youtube_tv/youtube.php"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("------ViewDidLoad-------")
        
        setNavBar()
        getYoutubeID()
    }
    
    
    
    func setNavBar() {
        UIApplication.shared.isStatusBarHidden = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    
    
    func getYoutubeID() {
        AF.request(url, method: .get)
        .responseJSON { response in
            if response.data != nil {
                do {
                    let json = try JSON(data: response.data!)
                    let madinah = json["youtube"][0]["madinah"].string
                    let makkah = json["youtube"][0]["makkah"].string
                    if madinah != nil {
                        //print("\(makkah ?? "")---\(madinah ?? "")")
                        self.playYoutubeChannel(makkahStr: makkah ?? "", madinahStr: madinah ?? "")
                    }
                } catch {
                   print(error)
                   
                }
            }
        }
        
    }
    
    func playYoutubeChannel(makkahStr: String, madinahStr: String){
        
        youtubePlayer.playerVars = [
                   "playsinline": "0",
                   "controls": "0",
                   "showinfo": "0",
                   "rel" : "0",
                   "autoplay": "0"
                   ] as YouTubePlayerView.YouTubePlayerParameters
        
        if (fromType == 0){
            self.title = "Live Makkah"
            youtubePlayer.loadVideoID(makkahStr)
            
            if (youtubePlayer.ready) {
                youtubePlayer.play()
            }
        }
        else{
            self.title = "Live Madinah"
            youtubePlayer.loadVideoID(madinahStr)
            
            if (youtubePlayer.ready) {
                youtubePlayer.play()
            }
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("------viewDidAppear-------")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("------viewWillAppear-------")
        self.tabBarController?.tabBar.isHidden = true
        UIApplication.shared.isStatusBarHidden = false
         
    }
    
    
   
}
