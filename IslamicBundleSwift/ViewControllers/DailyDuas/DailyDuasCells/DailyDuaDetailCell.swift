//
//  DailyDuaDetailCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 17/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

protocol DailyDuaDetailDelegate {
    func bookmarkTapped(tag: Int)
    
}

class DailyDuaDetailCell: UITableViewCell {
    
    var delegate: DailyDuaDetailDelegate?

    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var lblDuaEnglish: UILabel!
    @IBOutlet weak var lblDuaArabic: UILabel!
    @IBOutlet weak var lblDuaIndex: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnBookmarkTapped(_ sender: Any) {
        
        let button = sender as! UIButton
        self.delegate?.bookmarkTapped(tag: button.tag)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
