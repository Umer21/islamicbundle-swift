//
//  DailyDuaBookmarkListCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 21/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

protocol DailyDuaBookmarkListDelegate {
    func bookmarkTappedd(tag: Int)
    
}

class DailyDuaBookmarkListCell: UITableViewCell {
    
    var delegate: DailyDuaBookmarkListDelegate?

    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var lblBTranslation: UILabel!
    @IBOutlet weak var lblBDua: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnBookmarkTapped(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.bookmarkTappedd(tag: button.tag)
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
