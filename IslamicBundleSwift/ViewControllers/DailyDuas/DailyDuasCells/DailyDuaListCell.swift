//
//  DailyDuaListCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class DailyDuaListCell: UITableViewCell {

    @IBOutlet weak var lblDuaEnglish: UILabel!
    @IBOutlet weak var lblDuaArabic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
