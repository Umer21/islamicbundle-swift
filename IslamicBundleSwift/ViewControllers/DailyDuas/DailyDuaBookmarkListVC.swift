//
//  DailyDuaBookmarkListVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 21/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class DailyDuaBookmarkListVC: UIViewController {
    
    @IBOutlet weak var lblNoData: UILabel!
    var arrHisnulBookmarks = [HisnulBookmark]()
    var arrDuaDetail = [HisnulDetail]()
    
    let cellSpacingHeight: CGFloat = 6
    var pos: Int?

    @IBOutlet weak var bookmarkTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        getBookmarkDuas()
        
        bookmarkTableview.reloadData()
        bookmarkTableview.separatorStyle = .none
        bookmarkTableview.backgroundColor = .none
        bookmarkTableview.separatorColor = UIColor.clear
        
        self.lblNoData.center = self.view.center
    }
    
    func getBookmarkDuas(){
        arrHisnulBookmarks = DatabaseHelper.GetAllHisnulBookmark()
        
        if self.arrHisnulBookmarks.count > 0 {
            self.lblNoData.isHidden = true
            self.bookmarkTableview.isHidden = false
            
        }else{
            self.lblNoData.isHidden = false
            self.bookmarkTableview.isHidden = true
        }
        //arrDuaDetail = DatabaseHelper.getAllDetailDuaCategoryWise()
        
        for index in arrHisnulBookmarks.indices {
            print("\(index)")
            arrDuaDetail.append(DatabaseHelper.getDuaDetailForBookmark(id: arrHisnulBookmarks[index].Id!, cid: arrHisnulBookmarks[index].Dua!))
        }
    }

    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Bookmark Dua List"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}

extension DailyDuaBookmarkListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrDuaDetail.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! DailyDuaBookmarkListCell
        
        let model = self.arrDuaDetail[indexPath.section]
        
        cell.delegate = self
        cell.btnBookmark.tag = indexPath.section
        
        cell.lblBDua.text = model.Dua
        cell.lblBTranslation.text = model.Translation
        
        cell.layer.cornerRadius = 18
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        pos = indexPath.section
        
        gotoBookmarkDuaDetailScreen()
        
    }
    
    func gotoBookmarkDuaDetailScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kDailyDuaBookmarkDetailControllerID) as? DailyDuaBookmarkDetailVC
        vc?.pos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}


extension DailyDuaBookmarkListVC: DailyDuaBookmarkListDelegate {
    func bookmarkTappedd(tag: Int) {
        
        
        let model = self.arrDuaDetail[tag]
        let ffid = model.Id! + String(tag)
        print("ffid---->  \(ffid)")
        if (DatabaseHelper.CheckHisnulDuaExist(h_id: model.Id!, h_cid: model.chapterId!)){
            DatabaseHelper.DeleteHisnulDua(hisnul_id: model.Id!, hisnul_cid: model.chapterId!)
        }
        else{
            DatabaseHelper.InsertHisnulDua(hisnulId: model.Id!, hisnulDua: model.chapterId!, hisnulTranslation: "11", audio: "1")
        }
        
        arrHisnulBookmarks.removeAll()
        arrDuaDetail.removeAll()
        getBookmarkDuas()
        bookmarkTableview.reloadData()
        
    }
}
