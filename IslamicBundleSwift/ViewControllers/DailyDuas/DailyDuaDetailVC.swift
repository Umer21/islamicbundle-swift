//
//  DailyDuaDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 17/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class DailyDuaDetailVC: UIViewController {
    
    var arrDuaDetail = [HisnulDetail]()
    let cellSpacingHeight: CGFloat = 0
    var clickedPos: Int = 0

    
    @IBOutlet weak var duaDetailTableView: UITableView!
    
    
    @IBAction func btnPrevious(_ sender: Any) {
        if (clickedPos > 1)
        {
            clickedPos -= 1
            arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(clickedPos))
            duaDetailTableView.reloadData()
            
            multLineLabeltitle()
        }
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if (clickedPos < 132)
        {
            clickedPos += 1
            arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(clickedPos))
            duaDetailTableView.reloadData()
            
            multLineLabeltitle()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        duaDetailTableView.separatorStyle = .none
        duaDetailTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(clickedPos))
        
        multLineLabeltitle()
        
        
    }
    
    func multLineLabeltitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = NSTextAlignment.left
        label.text = "\(clickedPos)/132 \n\(arrDuaDetail[0].Category_Name ?? "")"
        self.navigationItem.titleView = label
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

extension DailyDuaDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrDuaDetail.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = duaDetailTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! DailyDuaDetailCell
        
        let model = self.arrDuaDetail[indexPath.section]
        
        cell.delegate = self
        cell.btnBookmark.tag = indexPath.section
        
        cell.lblDuaIndex.text = (String)(indexPath.section + 1)
        cell.lblDuaArabic.text = model.Dua
        cell.lblDuaEnglish.text = model.Translation
        
        if (DatabaseHelper.CheckHisnulDuaExist(h_id: model.Id!, h_cid: model.chapterId!)){
            
            
            cell.btnBookmark.setImage(UIImage(named: "bookmarkb"), for: UIControl.State.normal)
        }
        else{
            
            cell.btnBookmark.setImage(UIImage(named: "bookmark"), for: UIControl.State.normal)
        }
        //cell.btnBookmark.backgroundImage(for: UIControl.State.normal)
        //cell.btnBookmark.setBackgroundImage(UIImage(named: "bookmark"), for: UIControl.State.normal)
       // cell.btnBookmark.tintColor = UIColorFromRGB(rgbValue: 0x6f2da8)
        //cell.layer.cornerRadius = 17
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        //clickedPos = indexPath.section
        
       // gotoNamesDetailScreen()
        
    }
    
    /*func gotoNamesDetailScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kName99DetailViewControllerID) as? Allah99NamesDetailVC
        vc?.arrNameList = arrAllahNames
        vc?.clickedPos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }*/
    
}

extension DailyDuaDetailVC: DailyDuaDetailDelegate {
    func bookmarkTapped(tag: Int) {
        
        
        let model = self.arrDuaDetail[tag]
        let ffid = model.Id! + String(tag)
        print("ffid---->  \(ffid)")
        if (DatabaseHelper.CheckHisnulDuaExist(h_id: model.Id!, h_cid: model.chapterId!)){
            DatabaseHelper.DeleteHisnulDua(hisnul_id: model.Id!, hisnul_cid: model.chapterId!)
        }
        else{
            DatabaseHelper.InsertHisnulDua(hisnulId: model.Id!, hisnulDua: model.chapterId!, hisnulTranslation: "11", audio: "1")
        }
        duaDetailTableView.reloadData()
        
    }
}
