//
//  DailyDuaBookmarkDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 22/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class DailyDuaBookmarkDetailVC: UIViewController {

    @IBOutlet weak var bookmarkDetailTableView: UITableView!
    var arrHisnulBookmarks = [HisnulBookmark]()
    var arrDuaDetail = [HisnulDetail]()
    
    let cellSpacingHeight: CGFloat = 6
    var pos: Int?
    
    
    @IBAction func btnPrevious(_ sender: Any) {
        if (pos! > 0)
        {
            pos! -= 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            bookmarkDetailTableView.reloadData()
            viewSlideInFromLeftToRight(views: bookmarkDetailTableView)
            
            multLineLabeltitle()
        }
    }
    
    
    @IBAction func btnNext(_ sender: Any) {
        if (pos! < arrHisnulBookmarks.count - 1)
        {
            pos! += 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            bookmarkDetailTableView.reloadData()
            viewSlideInFromRightToLeft(views: bookmarkDetailTableView)
            
            multLineLabeltitle()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        getBookmarkDuaDetail()
        
        bookmarkDetailTableView.reloadData()
        bookmarkDetailTableView.separatorStyle = .none
        bookmarkDetailTableView.backgroundColor = .none
        bookmarkDetailTableView.separatorColor = UIColor.clear
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(sleft))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.bookmarkDetailTableView.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(sright))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.bookmarkDetailTableView.addGestureRecognizer(swipeLeft)
        
        swipeLeft.delegate = self
        swipeRight.delegate = self
        
        multLineLabeltitle()
    }
    
    @objc func sleft(){
        print("Swiped left")
        
        if (pos! > 0)
        {
            pos! -= 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            bookmarkDetailTableView.reloadData()
            viewSlideInFromLeftToRight(views: bookmarkDetailTableView)
            
            multLineLabeltitle()
        }
    }
    
    @objc func sright(){
        print("Swiped right")
        
        if (pos! < arrHisnulBookmarks.count - 1)
        {
            pos! += 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            bookmarkDetailTableView.reloadData()
            viewSlideInFromRightToLeft(views: bookmarkDetailTableView)
            
            multLineLabeltitle()
        }
    }
    
    func getBookmarkDuaDetail(){
        arrHisnulBookmarks = DatabaseHelper.GetAllHisnulBookmark()
        //arrDuaDetail = DatabaseHelper.getAllDetailDuaCategoryWise()
        
        for index in arrHisnulBookmarks.indices {
            print("\(index)")
            arrDuaDetail.append(DatabaseHelper.getDuaDetailForBookmark(id: arrHisnulBookmarks[index].Id!, cid: arrHisnulBookmarks[index].Dua!))
        }
    }
    
    func viewSlideInFromRightToLeft(views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition()
        transition!.duration = 0.5
        transition!.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition!.type = CATransitionType.push
        transition!.subtype = CATransitionSubtype.fromRight
        transition!.delegate = self as? CAAnimationDelegate
        views.layer.add(transition!, forKey: nil)
    }
    
    func viewSlideInFromLeftToRight(views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition()
        transition!.duration = 0.5
        transition!.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition!.type = CATransitionType.push
        transition!.subtype = CATransitionSubtype.fromLeft
        transition!.delegate = self as? CAAnimationDelegate
        views.layer.add(transition!, forKey: nil)
    }
    
    func multLineLabeltitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.text = "\(pos! + 1 ?? 0)/\(arrHisnulBookmarks.count)"
        self.navigationItem.titleView = label
    }
    
    func setNavBar()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}

extension DailyDuaBookmarkDetailVC: UIGestureRecognizerDelegate{
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
}

extension DailyDuaBookmarkDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        switch indexPath.section {
        case 0:
            let cell = bookmarkDetailTableView.dequeueReusableCell(withIdentifier: "cell1", for:  indexPath as IndexPath) as! DailyDuaBookmarkDetailCell1
            
            cell.lblBDDua.text = arrDuaDetail[pos!].Dua
            
            
            
            return cell
            
        case 1:
            
            let cell = bookmarkDetailTableView.dequeueReusableCell(withIdentifier: "cell2", for:  indexPath as IndexPath) as! DailyDuaBookmarkDetailCell2
            
            cell.lblBDTranslation.text = arrDuaDetail[pos!].Translation
            
            return cell
            
        default:
            break
        }
        
        return cell1!
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
    }
    
    
}
