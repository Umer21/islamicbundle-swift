//
//  DailyDuaListVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class DailyDuaListVC: UIViewController {

    @IBOutlet weak var DailyDuaListTableView: UITableView!
    var arrHisnul = [HisnulCategoriesModel]()
    var searchedRecords = [HisnulCategoriesModel]()
    let cellSpacingHeight: CGFloat = 6
    var pos: Int?
    @IBOutlet weak var searchDuas: UISearchBar!
    var isSearching = false
    @IBOutlet weak var searchView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        arrHisnul = DatabaseHelper.getDuaCategories()
        DailyDuaListTableView.separatorStyle = .none
        DailyDuaListTableView.backgroundColor = .none
        DailyDuaListTableView.separatorColor = UIColor.clear
    }
    

   func setNavBar() {
    searchView.isHidden = true
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
    self.navigationController?.navigationBar.tintColor = UIColor.white
    self.title = "Daily Dua"
    let bookmarkButton = UIBarButtonItem(image: UIImage(named: "bookmark"), style: .plain, target: self, action: #selector(bookmarkTapped))
    
    let searchButton = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(searchTapped))
    //self.navigationItem.leftBarButtonItem = signOutButton
    
    self.navigationItem.rightBarButtonItems = [bookmarkButton, searchButton]
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
   }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    @objc func bookmarkTapped() {
        gotoDuaBookmarkListScreen()
    }
    
    @objc func searchTapped() {
        if !searchView.isHidden{
            searchView.isHidden = true
        }else{
            searchView.isHidden = false
        }
    }

}

extension DailyDuaListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if isSearching {
            return searchedRecords.count
        }else{
            return arrHisnul.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! DailyDuaListCell
        
        var model = HisnulCategoriesModel()
        
        if isSearching {
            model = self.searchedRecords[indexPath.section]
        }else{
            model = self.arrHisnul[indexPath.section]
        }
        
        cell.lblDuaArabic.text = model.ArabicDua
        cell.lblDuaEnglish.text = model.EnglishDua
        
        cell.layer.cornerRadius = 18
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0;//Choose your custom row height
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        pos = indexPath.section
        
        gotoDuaDetailScreen()
        
    }
    
    func gotoDuaDetailScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kDailyDuaDetailViewControllerID) as? DailyDuaDetailVC
        vc?.clickedPos = pos! + 1
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func gotoDuaBookmarkListScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kDailyDuaBookmarkListControllerID) as? DailyDuaBookmarkListVC
       // vc?.pos = pos! + 1
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}

extension DailyDuaListVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
         searchedRecords = arrHisnul.filter({(searchObject ) -> Bool in
            if(searchObject.EnglishDua != nil) {
                let searh = "\(String(describing: searchObject.EnglishDua))"
                let val = searh.contains(searchText)
                return val
            }
            return false
        })
         
        //searchedRecords = arrAyats.filter({$0.prefix(searchText.count).lowercased() == searchText.lowercased()})
        
        if searchText == "" {
            print("search clear")
        }
        
        isSearching = true
        self.DailyDuaListTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        isSearching = false
        self.DailyDuaListTableView.reloadData()
    }
}
