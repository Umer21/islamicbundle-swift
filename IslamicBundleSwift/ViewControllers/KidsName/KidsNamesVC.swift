//
//  KidsNamesVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umar on 1/17/23.
//  Copyright © 2023 Umar Farooq. All rights reserved.
//

import UIKit

class KidsNamesVC: UIViewController {

    var isBoyName = true
    var pos: Int?
    var arrkidsNamesBoys = [KidsName]()
    var arrkidsNamesGirls = [KidsName]()
    var searchedRecords = [KidsName]()
    var isSearching = false
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tbleViewNames: UITableView!
    @IBOutlet weak var btnBoy: UIButton!
    @IBOutlet weak var btnGirl: UIButton!
    
    @IBAction func boyTapped(_ sender: Any) {
        isBoyName = true
        self.tbleViewNames.reloadData()
    }
    
    @IBAction func girlTapped(_ sender: Any) {
        isBoyName = false
        self.tbleViewNames.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        arrkidsNamesBoys = DatabaseHelper.getKidsNames(type: "Boys")
        arrkidsNamesGirls = DatabaseHelper.getKidsNames(type: "Girls")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setNavBar() {
        searchView.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Kids Name"
        
        let searchButton = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(searchTapped))
        self.navigationItem.rightBarButtonItems = [searchButton]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @objc func searchTapped() {
        if !searchView.isHidden{
            searchView.isHidden = true
        }else{
            searchView.isHidden = false
        }
    }
    

}

extension KidsNamesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if isSearching {
            return searchedRecords.count
            
        }else{
            if (isBoyName){
                return self.arrkidsNamesBoys.count
            }else{
                return self.arrkidsNamesGirls.count
            }
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! NamelistCell
        
        var model = KidsName()
        
        
        if isSearching {
            model = self.searchedRecords[indexPath.section]
        }else{
            
            if (isBoyName){
                model = self.arrkidsNamesBoys[indexPath.section]
            }else{
                model = self.arrkidsNamesGirls[indexPath.section]
            }
        }
        
        
        
        cell.lblAllahName.text = model.name
        cell.lblArabic.text = model.arabic
        //cell.layer.cornerRadius = 17
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        pos = indexPath.section
        gotoNamesDetailScreen()
        
    }
    
    func gotoNamesDetailScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kKidsDetailViewControllerID) as? KidsDetailVC
        
        var arr = [KidsName]()
        var title = ""
        if isSearching {
            arr = self.searchedRecords
        }else{
            if (isBoyName){
                arr = self.arrkidsNamesBoys
                title = "Boy Name"
            }else{
                arr = self.arrkidsNamesGirls
                title = "Girl Name"
            }
        }
        
        
        
        
        vc?.arrNameList = arr
        vc?.titleName = title
        vc?.indexPoint = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}

extension KidsNamesVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        var arrForFilter = [KidsName]()
        if (isBoyName){
            arrForFilter = self.arrkidsNamesBoys
        }else{
            arrForFilter = self.arrkidsNamesGirls
        }
        
         searchedRecords = arrForFilter.filter({(searchObject ) -> Bool in
            if(searchObject.name != nil) {
                let searh = "\(String(describing: searchObject.name))"
                let val = searh.contains(searchText)
                return val
            }
            return false
        })
         
        //searchedRecords = arrAyats.filter({$0.prefix(searchText.count).lowercased() == searchText.lowercased()})
        
        if searchText == "" {
            print("search clear")
        }
        
        isSearching = true
        self.tbleViewNames.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        isSearching = false
        self.tbleViewNames.reloadData()
    }
}
