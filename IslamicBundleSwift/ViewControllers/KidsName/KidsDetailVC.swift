//
//  KidsDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umar on 1/17/23.
//  Copyright © 2023 Umar Farooq. All rights reserved.
//

import UIKit

class KidsDetailVC: UIViewController {
    
    @IBOutlet weak var kidsNamesDetailTableView: UITableView!
    var arrNameList = [KidsName]()
    var indexPoint: Int = 0
    var titleName = ""

    @IBAction func btn_Previous(_ sender: Any) {
        
        if (indexPoint > 0) {
            indexPoint -= 1
            kidsNamesDetailTableView.reloadData()
        }
    }
    
    
    @IBAction func btn_Next(_ sender: Any) {
        
        if (indexPoint < arrNameList.count - 1) {
            indexPoint += 1
            kidsNamesDetailTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        self.kidsNamesDetailTableView.backgroundView = UIImageView(image: UIImage(named: "bg_kids.png"))
        self.kidsNamesDetailTableView.separatorStyle = .none
    }
    
    func setNavBar() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = self.titleName
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let settingButton = UIBarButtonItem(image: UIImage(named: "icon_nav_share"), style: .plain, target: self, action: #selector(settingsTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [settingButton]
    }
    
    @objc func settingsTapped() {
        
        let shareText = "\(String(describing: arrNameList[indexPoint].name!))\n\(arrNameList[indexPoint].meaning!)\n\(arrNameList[indexPoint].arabic!)"
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
    }

}

extension KidsDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        switch indexPath.section {
        case 0:
            let cell = kidsNamesDetailTableView.dequeueReusableCell(withIdentifier: "cell1", for:  indexPath as IndexPath) as! NameDetailCell1
            
            cell.lblTransliteration.text = arrNameList[indexPoint].name
            cell.lblMeaningD.text = arrNameList[indexPoint].arabic
            cell.backgroundColor = .clear
            
            
            return cell
            
        case 1:
            
            let cell = kidsNamesDetailTableView.dequeueReusableCell(withIdentifier: "cell2", for:  indexPath as IndexPath) as! NameDetailCell2
            
            cell.lblNameDescriptionD.text = arrNameList[indexPoint].meaning
            cell.backgroundColor = .clear
            
            return cell
            
            
        default:
            break
        }
        
        return cell1!
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
    }
    
    
}
