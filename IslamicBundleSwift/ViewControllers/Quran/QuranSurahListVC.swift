//
//  QuranSurahListVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 06/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class QuranSurahListVC: UIViewController {

    
    @IBOutlet weak var surahListTableView: UITableView!
    
    var arrSurahs = [SurahList]()
    var selectedPosition: Int64 = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        arrSurahs = DatabaseHelper.getSurahList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
         
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Quran"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let settingButton = UIBarButtonItem(image: UIImage(named: "icon_nav_setting"), style: .plain, target: self, action: #selector(settingsTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [settingButton]
    }
    
    @objc func settingsTapped() {
        gotoSettingsScreen()
    }
    
    func gotoSettingsScreen() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingsConttollerID) as! SettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
   

}

extension QuranSurahListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrSurahs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! QuranListCell
        
        let model = self.arrSurahs[indexPath.section]
        
        if let sIndex = model.sID
        {
            cell.lblSurahIndex.text = ("\(String(sIndex)).")
        }
        
        cell.lblSurahName.text = model.SurahName
        cell.lblSurahMeaning.text = (model.SurahNameMeaning!)
        
        if let sTotalAyaat = model.SurahTotalAyaat
        {
            cell.lblSurahMeaning.text = ("\(model.SurahNameMeaning!) (\(sTotalAyaat))")
        }
        
        //cell.imgMore.image = UIImage(named: arrMoreImage1[indexPath.row])
        let rowIndex = indexPath.section + 1
        cell.imgSurah.image = UIImage(named: "sura_\(rowIndex)")
        
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        selectedPosition = arrSurahs[indexPath.section].SurahNo!
        
        
        gotoQuranDetailScreen()
        
    }
    
    func gotoQuranDetailScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kQuranDetailControllerID) as! QuranDetailVC
        vc.selectedPos = Int64(selectedPosition)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
