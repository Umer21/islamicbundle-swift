//
//  QuranDetailCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 09/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

protocol QuranDetailDelegate {
    //func menuTapped(tag: Int)
    //func shareTapped(tag: Int)
    //func playTapped(tag: Int)
    //func favTapped(tag: Int)
}

class QuranDetailCell: UITableViewCell {

    var delegate: QuranDetailDelegate?
    
    @IBOutlet weak var viewMenuIB: UIView!
    @IBOutlet weak var btnFavIB: UIButton!
    @IBOutlet weak var btnPlayIB: UIButton!
    @IBOutlet weak var btnShareIB: UIButton!
    @IBOutlet weak var btnMenuIB: UIButton!
    @IBOutlet weak var viewParentHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTranslation: UILabel!
    
   /* @IBAction func btnMenuTapped(_ sender: Any) {
        
        let button = sender as! UIButton
        self.delegate?.menuTapped(tag: button.tag)
    }*/
    
   /* @IBAction func btnFavTapped(_ sender: Any) {
        
        let button = sender as! UIButton
        self.delegate?.favTapped(tag: button.tag)
    }
    @IBAction func btnShareTapped(_ sender: Any) {
        
        let button = sender as! UIButton
        self.delegate?.shareTapped(tag: button.tag)
    }
    
    @IBAction func btnPlayTapped(_ sender: Any) {
        
        let button = sender as! UIButton
        self.delegate?.playTapped(tag: button.tag)
    }*/
    
    @IBOutlet weak var lblTransliteration: UILabel!
    @IBOutlet weak var lblAyat: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
