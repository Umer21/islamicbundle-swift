//
//  QuranListCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 06/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class QuranListCell: UITableViewCell {

    @IBOutlet weak var imgSurah: UIImageView!
    @IBOutlet weak var lblSurahMeaning: UILabel!
    @IBOutlet weak var lblSurahName: UILabel!
    @IBOutlet weak var lblSurahIndex: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
