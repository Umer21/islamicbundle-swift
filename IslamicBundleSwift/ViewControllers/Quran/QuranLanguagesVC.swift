//
//  QuranLanguagesVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 18/11/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD

class QuranLanguagesVC: UIViewController {

    @IBOutlet weak var tblLanguages: UITableView!
    let languageArr = ["English", "Urdu", "Indonesian", "Bengali", "German", "Hindi", "Malaysian", "Norwegian", "Russian", "Chinese","Tamil", "Albanian" , "Bosnian", "Czech" , "Dutch" , "Italian" , "Japanese" , "Nigerian", "Pashto" , "Persian" , "Polish" , "Portuguese", "Romanian","Somali" , "Spanish", "Swedish" , "Tatar" , "Thai" , "Turkish", "Uzbek"]
    
    let languageIconArr = ["english", "urdu", "indonesian", "bengali", "german", "hindi", "malaysian", "norwegian", "russian", "chinese","tamil", "albanian" , "bosnian", "czech" , "dutch" , "italian" , "japanese" , "hausa", "pashto" , "persian" , "polish" , "portuguese", "romanian","somali","spanish", "swedish" , "tatar" , "thai" , "turkey", "uzbek"]
    
    let languageSize = [0, 1500000, 1200000, 2400000, 1100000, 2500000, 1600000, 860000, 1600000, 858000, 3400000, 1100000, 889000, 955000, 1100000, 943000, 1400000, 1000000, 1700000, 1700000, 993000, 944000, 955000, 873000, 976000, 1100000, 2300000, 2800000, 826000, 2600000]
    
    let languageUrls = ["", url_urdu, url_indonesian, url_bengali, url_german,url_hindi, url_malaysian, url_norway, url_russian, url_chinese,url_tamil, url_albanian, url_bosnain, url_czech, url_dutch, url_italian,
        url_japanese, url_nigerian, url_pashto, url_persian, url_polish, url_portughese, url_romanian, url_somali, url_spanish, url_swedish, url_tatar, url_thai,url_turkish, url_uzbek]
    
    let languageTable = [TABLE_ENGLISH, TABLE_URDU, TABLE_INDONESIAN, TABLE_BENGALI,
        TABLE_GERMAN, TABLE_HINDI, TABLE_MALAYSIAN, TABLE_NORWEGIAN, TABLE_RUSSIAN, TABLE_CHINESE, TABLE_TAMIL, TABLE_ALBANIAN, TABLE_BOSNIAN, TABLE_CZECH, TABLE_DUTCH, TABLE_ITALIAN, TABLE_JAPANESE, TABLE_NIGERIAN, TABLE_PASHTO, TABLE_PERSIAN, TABLE_POLISH, TABLE_PORTUGHESE, TABLE_ROMANIAN, TABLE_SOMALI, TABLE_SPANISH, TABLE_SWEDISH, TABLE_TATAR, TABLE_THAI, TABLE_TURKISH, TABLE_UZBEK]
    
    let languageName = ["eng_lang", "urdu_lang", "indonesian_lang", "flag_bengali", "flag_german", "flag_hindi", "flag_malay", "flag_norwegian", "flag_russian","flag_chinese", "flag_hindi", "albanian", "bosnian", "czech", "dutch", "italian", "japanese", "flag_hausa", "pashto", "persian", "polish", "portuguese",
        "romanian", "somali", "spanish", "swedish", "tatar", "thai", "turkey", "uzbek"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        //print("---11-----\(DatabaseHelper.tableExist(tableName: TABLE_ENGLISH))")
        //print("---22-----\(DatabaseHelper.tableExist(tableName: TABLE_THAI))")
    }
    
    func setNavBar()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Quran Languages"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }

}

extension QuranLanguagesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.languageArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! LanguageCell
        cell.lblLanguageName.text = languageArr[indexPath.section]
        cell.imgLanguage.image = UIImage(named: languageIconArr[indexPath.section])
        
        let currLanguage = getValueForKey(keyValue: "SelectedLanguageIndex")
        //print("Current-Language \(currLanguage)")
        
        if (indexPath.section == Int(currLanguage)) {
            
            cell.imgCheck.image = UIImage(named: "icon_langchk")
        }
        else{
            
            cell.imgCheck.image = UIImage(named: "icon_langunchk")
        }
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        //print("You tapped cell number \(indexPath.section).")
        
        if (DatabaseHelper.tableExist(tableName: languageTable[indexPath.section])) {
            setDefaultValue(keyValue: "SelectedLanguageIndex", valueIs: String (indexPath.section))
            setDefaultValue(keyValue: "LanguageName", valueIs: languageName[indexPath.section])
            setDefaultValue(keyValue: "LanguageTable", valueIs: languageTable[indexPath.section])
            
        }
        else {
            showDialog(index: indexPath.section)
        }
        
        tblLanguages.reloadData()
        
    }
    
    func showDialog(index: Int)
    {
        let optionMenu = UIAlertController(title: nil, message: "Download Translation first", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Download", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            setDefaultValue(keyValue: "SelectedLanguageIndex", valueIs: String (index))
            setDefaultValue(keyValue: "LanguageName", valueIs: self.languageName[index])
            setDefaultValue(keyValue: "LanguageTable", valueIs: self.languageTable[index])
            
            let url = URL(string: self.languageUrls[index])
            self.downloadUsingAlamofire(url: url!, urlStr: self.languageUrls[index], indx: index)
           
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        optionMenu.addAction(cancelAction)
        optionMenu.addAction(yesAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    // MARK: - Load URL
    func downloadUsingAlamofire(url: URL, urlStr: String, indx: Int) {
        
        //audioUrl should be of type URL
        let audioFileName = String((url.lastPathComponent)) as NSString

        //path extension will consist of the type of file it is, m4a or mp4
        let pathExtension = audioFileName.pathExtension
        
        /*let window = UIApplication.shared.windows.first
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)*/
        
        let hud = JGProgressHUD(style: .dark)
        hud.vibrancyEnabled = true
        if arc4random_uniform(2) == 0 {
            hud.indicatorView = JGProgressHUDPieIndicatorView()
        }
        else {
            hud.indicatorView = JGProgressHUDRingIndicatorView()
        }
        hud.detailTextLabel.text = "0% Complete"
        hud.textLabel.text = "Downloading"
        hud.show(in: self.view)
       
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

            // the name of the file here I kept is yourFileName with appended extension
            //documentsURL.appendPathComponent("yourFileName."+pathExtension)
            documentsURL.appendPathComponent(audioFileName as String)
            return (documentsURL, [.removePreviousFile])
        }
        
        AF.download(urlStr, to: destination)
            .downloadProgress { progress in
                let percentageCount = Float(progress.completedUnitCount) / Float(self.languageSize[indx]) * 100
            
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
                    self.incrementHUD(hud, progresss: (Int)(percentageCount), iindex: indx)
                }
        }
        .response { response in
            debugPrint(response)
            
            //hud.dismiss(afterDelay: 0.0)
            /*
            if (checkFileExist(fileName: self.languageTable[indx])) {//+ ".sql")
                
            }*/
            
            if response.fileURL != nil {
                
                let fileManager = FileManager.default
                let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
                //let finalLanguageURL = documentsUrl.first!.appendingPathComponent(self.languageTable[indx] + ".sql")
                let finalLanguageURL = documentsUrl.first!.appendingPathComponent("download.php")
                if let input = FileHandle(forReadingAtPath: finalLanguageURL.path)
                {
                    let scanner = StreamScanner(source: input, delimiters: CharacterSet(charactersIn: ";\n"))
                    var fullName = ""
                    
                    while let line: String = scanner.read()
                    {
                        if (!(line.contains("BEGIN TRANSACTION") || (line.contains("COMMIT")) || line.contains("-- CREATE") || line.contains("-- USE"))) {
                            fullName.append(line)
                        }
                    }
                    
                    let fullNameArr = fullName.components(separatedBy: "INSERT")
                    //DatabaseHelper.CreateInsertLanguageTable(querry: fullNameArr[0])
                    //DatabaseHelper.CreateInsertLanguageTable(querry: "INSERT " + fullNameArr[1])
                    
                    var counter = 0
                    for query in fullNameArr {
                        if counter == 0 {
                            DatabaseHelper.CreateInsertLanguageTable(querry: fullNameArr[0])
                        }else{
                            DatabaseHelper.CreateInsertLanguageTable(querry: "INSERT " + query)
                        }
                        counter = counter + 1
                    }
                }
               
                
            }
        }
    }
    
    func incrementHUD(_ hud: JGProgressHUD, progresss : Int, iindex : Int) {
        let progress1 = progresss//previousProgress + 1
        hud.progress = Float(progress1)/100.0
        if progress1 <= 100 {
            hud.detailTextLabel.text = "\(progress1)% Complete"
        }
        
        if progress1 == 100 || progress1 > 95 {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success"
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0)
                
                self.tblLanguages.reloadData()
                
            }
        }
    }
}
