//
//  LanguageCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 18/11/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class LanguageCell: UITableViewCell {

    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblLanguageName: UILabel!
    @IBOutlet weak var imgLanguage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
