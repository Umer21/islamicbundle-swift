//
//  QuranDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 09/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import AVFoundation

class QuranDetailVC: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var quranTableView: UITableView!
    var arrSurahs = [SurahDetail]()
    var selectedPos: Int64 = 0
    var fileUrl = "https://inabia.com/Apps/quranicsurahs/"//http://codinghomes.com/quranicsurahs/
    var audio_url = ""
    var concat_surah = ""
    var audioPlayer = AVAudioPlayer()
    var isPlaying = false
    var timer:Timer!
    var selectedSection = 0
    var languageTable = ""
    let languageIconArr = ["english", "urdu", "indonesian", "bengali", "german", "hindi", "malaysian", "norwegian", "russian", "chinese","tamil", "albanian" , "bosnian", "czech" , "dutch" , "italian" , "japanese" , "hausa", "pashto" , "persian" , "polish" , "portuguese", "romanian","somali","spanish", "swedish" , "tatar" , "thai" , "turkey", "uzbek"]
    
    var currLanguage = 0
    var idx = IndexPath()
    var lastselectedIndex = 0
    var menuClicked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        
        /*do {
            audioPlayer =  try AVAudioPlayer(contentsOf: url)
        } catch {
            // can't load file
        }*/
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        setNavBar()
        print(selectedPos)
        languageTable = getValueForKey(keyValue: "LanguageTable")
        if (languageTable == "") {
            languageTable = TABLE_ENGLISH
        }
        print("Current-Table: \(languageTable)")
        arrSurahs = DatabaseHelper.getSurahDetail(pos: selectedPos, currTable: languageTable)
        setDefaultValue(keyValue: "Index", valueIs: "")
        
        quranTableView.reloadData()
        
    }
    
    func setNavBar()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Surah"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        currLanguage = Int (getValueForKey(keyValue: "SelectedLanguageIndex")) ?? 0
        print("currLanguageIndex: \(currLanguage)")
        
       // let settingButton = UIBarButtonItem(image: UIImage(named: languageIconArr[currLanguage]), style: .plain, target: self, action: #selector(quranLanguageTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        let settingButton = UIBarButtonItem().initRightButton(imageNamed: languageIconArr[currLanguage], target: self, selector: #selector(quranLanguageTapped))
        
        self.navigationItem.rightBarButtonItems = [settingButton]
    }
    
    @objc func quranLanguageTapped() {
        gotoQuranLanguagesScreen()
    }
    
    func gotoQuranLanguagesScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kQuranLanguagesControllerID) as! QuranLanguagesVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension QuranDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrSurahs.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        70
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! QuranDetailCell
        
        let model = self.arrSurahs[indexPath.section]
        
        cell.lblAyat.text = model.Ayat
        cell.lblTransliteration.text = ("\(model.AyahNo). \(model.Transliteration)")
        cell.lblTranslation.text = ("\(model.AyahNo). \(model.EnTranslation)")
        
        cell.delegate = self
        cell.btnMenuIB.tag = indexPath.section
        cell.btnShareIB.tag = indexPath.section
        cell.btnPlayIB.tag = indexPath.section
        cell.btnFavIB.tag = indexPath.section
        cell.viewMenuIB.tag = indexPath.section
        
        cell.btnMenuIB.addTarget(self, action: #selector(menuTapped), for: .touchUpInside)
        cell.btnShareIB.addTarget(self, action: #selector(shareTapped), for: .touchUpInside)
        cell.btnPlayIB.addTarget(self, action: #selector(playTapped), for: .touchUpInside)
        cell.btnFavIB.addTarget(self, action: #selector(favTapped), for: .touchUpInside)
        
        if model.FavAyah == 1{
            cell.btnMenuIB.setImage(UIImage(named: "icon_fav_mark"), for: UIControl.State.normal)
        }
        else{
            cell.btnMenuIB.setImage(UIImage(named: "icon_menu_mark"), for: UIControl.State.normal)
        }
        
        concat_surah = "\(String(format: "%03d", model.SurahNo))\(String(format: "%03d", model.AyahNo))"
        if (checkFileExist(fileName: "\(concat_surah).mp3")){
            //print("File already exist")
            
            //cell.btnPlayIB.setImage(UIImage(named: "icon_play_quran"), for: UIControl.State.normal)
            if isPlaying {
                cell.btnPlayIB.setImage(UIImage(named: "icon_home_pause"), for: UIControl.State.normal)
            }
            else {
                cell.btnPlayIB.setImage(UIImage(named: "icon_play_quran"), for: UIControl.State.normal)
            }
            
            //initAVPlayer()
        }
        else{
            //print("File not exist")
            cell.btnPlayIB.setImage(UIImage(named: "icon_home_download"), for: UIControl.State.normal)
        }
        
        /*if model.isClicked {
            cell.viewMenuIB.isHidden = false
            viewSlideInFromLeftToRight(views: cell.viewMenuIB)
            
        } else {
            cell.viewMenuIB.isHidden = true
           // viewSlideInFromRightToLeft(views: cell.viewMenuIB)
        }*/
        
        print("lastselectedIndex: \(lastselectedIndex)")
        print("menuClicked: \(menuClicked)")
        if lastselectedIndex == indexPath.section && menuClicked {
            cell.viewMenuIB.isHidden = false
            // viewSlideInFromRightToLeft(views: cell.viewMenuIB)
        }else{
            cell.viewMenuIB.isHidden = true
            viewSlideInFromLeftToRight(views: cell.viewMenuIB)
            
        }
        
        cell.viewParentHeight.constant = cell.lblAyat.requiredHeight + cell.lblTransliteration.requiredHeight + cell.lblTranslation.requiredHeight + 95
        
        return cell
        
    }
    
    @objc func menuTapped(sender: UIButton!) {
        print("Menu Tapped")
        //let model = self.arrSurahs[sender.tag]
        self.lastselectedIndex = sender.tag
         
         let indexP = NSIndexPath(row: 0, section: self.lastselectedIndex)
         //let currentCell = self.quranTableView.cellForRow(at: indexP as IndexPath) as! QuranDetailCell
         
         //currentCell.
         if menuClicked == false {
             menuClicked = true
            // currentCell.viewMenuIB.isHidden = true
            // viewSlideInFromRightToLeft(views: currentCell.viewMenuIB)
         }
         else {
             menuClicked = false
            // currentCell.viewMenuIB.isHidden = false
            // viewSlideInFromLeftToRight(views: currentCell.viewMenuIB)
         }
        
        self.quranTableView.reloadData()
        // self.quranTableView.reloadRows(at: [(indexP as IndexPath)], with: .none)
        
    }
    
    @objc func shareTapped(sender: UIButton!) {
        print("Share Tapped")
        
        let model = arrSurahs[sender.tag]
        print(model.Ayat)
        
        //let apptext = "\("Download Islamic Bundle")\nhttps://itunes.apple.com/us/app/"
        
        let text = ("\(model.Ayat)\n\(model.EnTranslation)\n\(model.SurahName)(\(model.SurahNo):\(model.AyahNo))")
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    @objc func playTapped(sender: UIButton!) {
        print("Play Tapped")
        //print(tag)
        selectedSection = sender.tag
        let model = arrSurahs[sender.tag]
        //print("\(String(format: "%03d", model.SurahNo))\(String(format: "%03d", model.AyahNo))")
        
        concat_surah = "\(String(format: "%03d", model.SurahNo))\(String(format: "%03d", model.AyahNo))"
        audio_url = ("\(fileUrl)\(concat_surah).mp3")
        
        if (!checkFileExist(fileName: "\(concat_surah).mp3")) {
            let url = URL(string: audio_url)
            downloadUsingAlamofire(url: url!, urlStr: audio_url)
        }
        else{
            //print("File already exist")
            self.initAVPlayer()
            
            if isPlaying {
                audioPlayer.pause()
                isPlaying = false
                quranTableView.reloadData()
                //quranTableView.scrollToRow(at: IndexPath(row: 0, section: tag), at: .none, animated: false)
            } else {
                audioPlayer.play()
                isPlaying = true
                quranTableView.reloadData()
                //quranTableView.scrollToRow(at: IndexPath(row: 0, section: tag), at: .none, animated: false)
                
            }
            
            
        }
        
    }
    
    @objc func favTapped(sender: UIButton!) {
        print("Fav Tapped")
        //print(sender.tag)
        
        let model = arrSurahs[sender.tag]
        
        if model.FavAyah == 1{
            DatabaseHelper.UpdateSurahDetail(surahNo: String(model.SurahNo), ayahNo: String(model.AyahNo), favValue: "0")
        }
        else{
            DatabaseHelper.UpdateSurahDetail(surahNo: String(model.SurahNo), ayahNo: String(model.AyahNo), favValue: "1")
        }
        
        languageTable = getValueForKey(keyValue: "LanguageTable")
        arrSurahs.removeAll()
        arrSurahs = DatabaseHelper.getSurahDetail(pos: selectedPos, currTable: languageTable)
        
        quranTableView.reloadData()
        //quranTableView.scrollToRow(at: IndexPath(row: 0, section: selectedSection), at: .none, animated: false)
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        idx = indexPath
    
        //pos = indexPath.section
        
        //gotoRabbanaDetailScreen()
        
    }
    
    /*func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.9, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }*/
    
    func viewSlideInFromRightToLeft(views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition()
        transition!.duration = 0.5
        transition!.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition!.type = CATransitionType.push
        transition!.subtype = CATransitionSubtype.fromRight
        transition!.delegate = self as? CAAnimationDelegate
        views.layer.add(transition!, forKey: nil)
    }
    
    func viewSlideInFromLeftToRight(views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition()
        transition!.duration = 0.5
        transition!.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition!.type = CATransitionType.push
        transition!.subtype = CATransitionSubtype.fromLeft
        transition!.delegate = self as? CAAnimationDelegate
        views.layer.add(transition!, forKey: nil)
    }
    
    func reload(tableView: UITableView) {

        let contentOffset = tableView.contentOffset
        tableView.reloadData()
        tableView.layoutIfNeeded()
        tableView.setContentOffset(contentOffset, animated: false)

    }
    
}

extension QuranDetailVC: QuranDetailDelegate {
    /*func menuTapped(tag: Int) {
        
        print("Menu Clicked: \(tag)")
        
        self.lastselectedIndex = tag
        
        let indexP = NSIndexPath(row: 0, section: self.lastselectedIndex)
        //let currentCell = self.quranTableView.cellForRow(at: indexP as IndexPath) as! QuranDetailCell
        
        //currentCell.
        if menuClicked == false {
            menuClicked = true
           // currentCell.viewMenuIB.isHidden = true
           // viewSlideInFromRightToLeft(views: currentCell.viewMenuIB)
        }
        else {
            menuClicked = false
           // currentCell.viewMenuIB.isHidden = false
           // viewSlideInFromLeftToRight(views: currentCell.viewMenuIB)
        }
       
        self.quranTableView.reloadRows(at: [(indexP as IndexPath)], with: .none)
        //-----------------------------------------------------------------------
        
        /*for item in arrSurahs{
            item.isClicked = false
        }
        
        let iNDEX = getValueForKey(keyValue: "Index")
        
        if iNDEX == String(tag){
            
            let model = arrSurahs[tag]
            model.isClicked = false
            
            setDefaultValue(keyValue: "Index", valueIs: "")
        }
        else{
            
            let model = arrSurahs[tag]
            model.isClicked = true
            
            setDefaultValue(keyValue: "Index", valueIs: String(tag))
        }
        
        quranTableView.reloadData()
        quranTableView.scrollToRow(at: IndexPath(row: 0, section: tag), at: .none, animated: false)*/
        
        //----------------------------------------------------------------------------
        
       /* let indexP = NSIndexPath(row: 0, section: tag)
        print("indexP---> \(indexP)")
        if let cell = quranTableView.cellForRow(at: indexP as IndexPath) as? QuranDetailCell {
                     
            
            if cell.viewMenuIB.isHidden == false {
                cell.viewMenuIB.isHidden = true
                viewSlideInFromRightToLeft(views: cell.viewMenuIB)
            }
            else {
                cell.viewMenuIB.isHidden = false
                viewSlideInFromLeftToRight(views: cell.viewMenuIB)
            }
        }*/
    } */
    
    /*func shareTapped(tag: Int) {
        print("Share Tapped")
        print(tag)
        
        let model = arrSurahs[tag]
        print(model.Ayat)
        
        //let apptext = "\("Download Islamic Bundle")\nhttps://itunes.apple.com/us/app/"
        
        let text = ("\(model.Ayat)\n\(model.EnTranslation)\n\(model.SurahName)(\(model.SurahNo):\(model.AyahNo))")
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    }*/
    
    /*func playTapped(tag: Int) {
        print("Play Tapped")
        //print(tag)
        selectedSection = tag
        let model = arrSurahs[tag]
        //print("\(String(format: "%03d", model.SurahNo))\(String(format: "%03d", model.AyahNo))")
        
        concat_surah = "\(String(format: "%03d", model.SurahNo))\(String(format: "%03d", model.AyahNo))"
        audio_url = ("\(fileUrl)\(concat_surah).mp3")
        
        if (!checkFileExist(fileName: "\(concat_surah).mp3")) {
            let url = URL(string: audio_url)
            downloadUsingAlamofire(url: url!, urlStr: audio_url)
        }
        else{
            //print("File already exist")
            self.initAVPlayer()
            
            if isPlaying {
                audioPlayer.pause()
                isPlaying = false
                quranTableView.reloadData()
                quranTableView.scrollToRow(at: IndexPath(row: 0, section: tag), at: .none, animated: false)
            } else {
                audioPlayer.play()
                isPlaying = true
                quranTableView.reloadData()
                quranTableView.scrollToRow(at: IndexPath(row: 0, section: tag), at: .none, animated: false)
                
            }
            
            
        }
        
    }*/
    
    func initAVPlayer() {
        //print("------------>> \(concat_surah).mp3")
        //print("------------>> \(checkFileExist(fileName: "\(concat_surah).mp3"))")
        if (checkFileExist(fileName: "\(concat_surah).mp3")){
            let fileManager = FileManager.default
            let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("\(concat_surah).mp3")
            print(finalDatabaseURL)
            
            do {
                audioPlayer =  try AVAudioPlayer(contentsOf: finalDatabaseURL)
            } catch {
                // can't load file
                print("Check-Error-playr-> \(error)")
            }
            
            //Slider.maximumValue = Float(audioPlayer.duration)
            audioPlayer.delegate = self
            
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("Audio finished called")
        
        self.audioPlayer.stop()
        isPlaying = false
        
        quranTableView.reloadData()
        //quranTableView.scrollToRow(at: IndexPath(row: 0, section: selectedSection), at: .none, animated: false)
    }
    
    /*@objc func updateTime() {
        let currentTime = Int(audioPlayer.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
            
        lblTime.text = String(format: "%02d:%02d", minutes,seconds) as String
                
        UIView.animate(withDuration: 2, animations: {
          self.Slider.setValue(Float(self.audioPlayer.currentTime), animated: true)
        })
        print(Float (audioPlayer.currentTime))
        print(TimeInterval (audioPlayer.currentTime))
    }*/
    
    /*func favTapped(tag: Int) {
        print("Fav Tapped")
        print(tag)
        
        let model = arrSurahs[tag]
        
        if model.FavAyah == 1{
            DatabaseHelper.UpdateSurahDetail(surahNo: String(model.SurahNo), ayahNo: String(model.AyahNo), favValue: "0")
        }
        else{
            DatabaseHelper.UpdateSurahDetail(surahNo: String(model.SurahNo), ayahNo: String(model.AyahNo), favValue: "1")
        }
        
        
        arrSurahs.removeAll()
        arrSurahs = DatabaseHelper.getSurahDetail(pos: selectedPos, currTable: "En_Translation")//DatabaseHelper.UpdateSurahDetail(surahNo: "1", ayahNo: "1", favValue: "1")
        
        quranTableView.reloadData()
        quranTableView.scrollToRow(at: IndexPath(row: 0, section: selectedSection), at: .none, animated: false)
    }*/
    
    // MARK: - Load URL
    func downloadUsingAlamofire(url: URL, urlStr: String) {
        
        //audioUrl should be of type URL
        let audioFileName = String((url.lastPathComponent)) as NSString
        //print("audioFileName----\(audioFileName)")

        //path extension will consist of the type of file it is, m4a or mp4
        let pathExtension = audioFileName.pathExtension
        print("pathExtension \(pathExtension)")
        
        let hud = JGProgressHUD(style: .dark)
        hud.vibrancyEnabled = true
        if arc4random_uniform(2) == 0 {
            hud.indicatorView = JGProgressHUDPieIndicatorView()
        }
        else {
            hud.indicatorView = JGProgressHUDRingIndicatorView()
        }
        hud.detailTextLabel.text = "0% Complete"
        hud.textLabel.text = "Downloading"
        hud.show(in: self.view)
       
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

            // the name of the file here I kept is yourFileName with appended extension
            //documentsURL.appendPathComponent("yourFileName."+pathExtension)
            documentsURL.appendPathComponent(audioFileName as String)
            return (documentsURL, [.removePreviousFile])
        }
        
        AF.download(urlStr, to: destination)
            .downloadProgress { progress in
                //print("Download Progress: \(progress.fractionCompleted * 100)")
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
                    self.incrementHUD(hud, progresss: (Int)(progress.fractionCompleted * 100))
                }
        }
        .response { response in
            debugPrint(response)
            
            //hud.dismiss(afterDelay: 0.0)
            
            if response.fileURL != nil {
               // print("Response------>  \(response.fileURL!)")
            }
            
           // self.quranTableView.reloadData()
            //self.quranTableView.scrollToRow(at: IndexPath(row: 0, section: self.selectedSection), at: .none, animated: false)
        }
    }
    
    func incrementHUD(_ hud: JGProgressHUD, progresss : Int) {
        let progress1 = progresss//previousProgress + 1
        hud.progress = Float(progress1)/100.0
        hud.detailTextLabel.text = "\(progress1)% Complete"
        
        if progress1 == 100 {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success"
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0)
                 self.quranTableView.reloadData()
                // self.quranTableView.scrollToRow(at: IndexPath(row: 0, section: self.selectedSection), at: .none, animated: false)
                
                
            }
        }
    }
    
    
}


extension UIBarButtonItem
{
    /** Create custom right bar button for reduce space between right bar buttons */
    func initRightButton(imageNamed:String, target:UIViewController, selector:Selector) -> UIBarButtonItem {
        let frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        //Create imageView
        let imageView = UIImageView(frame:frame)
        imageView.image = UIImage(named: imageNamed)
        //Create Button
        let button = UIButton(frame: frame)
        button.addTarget(target, action: selector, for: .touchUpInside)
        //Create View and add imageView and Button
        let view = UIView(frame: frame)
        view.addSubview(imageView)
        view.addSubview(button)
        return UIBarButtonItem(customView: view)
    }
}
