//
//  RelatedAppsVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 30/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class RelatedAppsVC: UIViewController {

    @IBOutlet weak var relatedTableView: UITableView!
    
    var arrRelated = ["Quranic Quiz\nQuiz Books\nQuranic Quiz",
    "40 Rabbana\nEducation\nRead 40 rabbana",
    "Ayah Of The Day\nEducation\nRead Daily Ayah",
    "Hisnul Muslim\nEducation\nDaily Prayers"]
    
    let arrRelatedImage = ["quiz","rabbana","daily_ayah","hisnul"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        relatedTableView.separatorColor = .white
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Related Apps"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
   

}

extension RelatedAppsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRelated.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! RelatedCell
        
        cell.lblAppName.text = self.arrRelated[indexPath.section]
        cell.appimg.image = UIImage(named: arrRelatedImage[indexPath.section])
        
        cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        switch indexPath.section {
        case 0:
            gotoAppStore(appID: "id1054872236", appName: "quranic-surahs-learn-quiz")
            
        case 1:
            gotoAppStore(appID: "id1072699203", appName: "40rabbanas-qurandua-invocation")
            
        case 2:
            gotoAppStore(appID: "id1084368996", appName: "ayah-of-the-day-daily-ayah")
            
        case 3:
            //gotoAppStore(appID: "id1072699203", appName: "40rabbanas-qurandua-invocation")
            showToast(viewControl: self, titleMsg: "", msgTitle: "Not available")
            
        default:
            print("")
        }
        //pos = indexPath.section
        
        //gotoRabbanaDetailScreen()
        
    }
    
    func gotoAppStore(appID : String, appName: String){
        print("https://itunes.apple.com/in/app/\(appName)/\(appID)?mt=8")
        if let url = URL(string: "https://itunes.apple.com/in/app/\(appName)/\(appID)?mt=8")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
    }
    
    func gotoRabbanaDetailScreen() {
        let vc = UIStoryboard.init(name: kRabbanaStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRabbanaDetailControllerID) as? RabbanaDetailVC
        //vc?.arrRabbanaList = arrRabbana
        //vc?.clickedPos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}
