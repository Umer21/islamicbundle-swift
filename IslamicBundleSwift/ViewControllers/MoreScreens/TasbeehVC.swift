//
//  TasbeehVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/11/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import CoreGraphics
import AVFoundation
import MediaPlayer

class TasbeehVC: UIViewController {
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblLimit: UILabel!
    @IBOutlet weak var bead1: UIImageView!
    @IBOutlet weak var bead2: UIImageView!
    @IBOutlet weak var bead3: UIImageView!
    
    var moveAlongPath: CAAnimation!
    var moveBackPath: CAAnimation!
    let forwardPath = UIBezierPath()
    let backwardPath = UIBezierPath()
    
    let goView  = CustomView(frame: CGRect(x: 150, y: 10, width: 50, height: 50))
    let backView  = CustomBackView(frame: CGRect(x:365 , y: 200, width: 50, height: 50))
    
    let startPoint = CGPoint(x: 75, y: 310)//Start
    let endPoint = CGPoint(x: 375, y: 360)//End
    let controlPoint = CGPoint(x: 160, y: 200)//Control point
    
    var avPlayer: AVAudioPlayer?
    var counter = 0
    var totalValue = 33
    var AllTotal = 0
    var isRoundCompleted = false
    var IsMuted = false
    
    @IBAction func btnClick(_ sender: Any) {
        startForwardAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavBar()
        
        //this method is for forward path
        addAnimation()
        //this method is for bacward path
        addBackAnimation()
        
        //Swipe Gesture Added
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func setNavBar() {
        
        let resetButton = UIBarButtonItem(image: UIImage(named: "icon_reset"), style: .plain, target: self, action: #selector(resetButtonTapped))
        
        var limitButton: UIBarButtonItem?
        
        if (self.totalValue == 33){
            limitButton = UIBarButtonItem(image: UIImage(named: "button_limit_33"), style: .plain, target: self, action: #selector(limitButtonTapped))
        }
        else if (self.totalValue == 99) {
            limitButton = UIBarButtonItem(image: UIImage(named: "button_limit_99"), style: .plain, target: self, action: #selector(limitButtonTapped))
        }
        else if (self.totalValue == 999){
            limitButton = UIBarButtonItem(image: UIImage(named: "button_limit_999"), style: .plain, target: self, action: #selector(limitButtonTapped))
        }
        else {
            limitButton = UIBarButtonItem(image: UIImage(named: "icon_c"), style: .plain, target: self, action: #selector(limitButtonTapped))
        }
        
        var soundButton: UIBarButtonItem?
        
        if IsMuted == false {
            soundButton = UIBarButtonItem(image: UIImage(named: "icon_volume"), style: .plain, target: self, action: #selector(soundButtonTapped))
        }
        else{
            soundButton = UIBarButtonItem(image: UIImage(named: "icon_mute"), style: .plain, target: self, action: #selector(soundButtonTapped))
        }
        
        self.navigationItem.rightBarButtonItems = [soundButton!, limitButton!, resetButton]
        self.navigationController?.navigationBar.tintColor = .white
        
    }
    
    @objc func resetButtonTapped() {
        //reset main counter
        counter = 0
        AllTotal = 0
        updateCounter()
        updateTotalCounter()
    }
    
    @objc func limitButtonTapped() {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let oneAction = UIAlertAction(title: "33", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.totalValue = 33
            //reset main counter
            self.counter = 0
            self.updateCounter()
            
            self.setNavBar()
            
        })
        
        let twoAction = UIAlertAction(title: "99", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.totalValue = 99
            //reset main counter
            self.counter = 0
            self.updateCounter()
            
            self.setNavBar()
            
        })
        
        let threeAction = UIAlertAction(title: "999", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.totalValue = 999
            
            //reset main counter
            self.counter = 0
            self.updateCounter()
            
            self.setNavBar()
            
        })
        
        let customAction = UIAlertAction(title: "Custom", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.presentCustomAlert()
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            //self.alertTypeCustom = false
        })
        
        optionMenu.addAction(oneAction)
        optionMenu.addAction(twoAction)
        optionMenu.addAction(threeAction)
        optionMenu.addAction(customAction)
        optionMenu.addAction(cancelAction)
        
        //self.present(optionMenu, animated: true, completion: nil)
        self.present(optionMenu, animated: true, completion: {
            
        })
    }
    
    @objc func soundButtonTapped() {
        if (IsMuted == false) {
            print("mute")
            IsMuted = true
            MPVolumeView.setVolume(0.0)
            setNavBar()
        } else {
            print("unmute")
            IsMuted = false
            MPVolumeView.setVolume(0.5)
            setNavBar()
        }
    }
    
    func presentCustomAlert(){
        
        let alertController = UIAlertController(title: "Custom Option", message: "", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addTextField { (textField: UITextField!) -> Void in
            textField.placeholder = "Please enter value"
            textField.keyboardType = .numberPad
        }
        
        let saveAction = UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let customTextField = alertController.textFields![0] as UITextField
            self.totalValue = (customTextField.text! as NSString).integerValue
            
            if self.totalValue == 0 {
                self.totalValue = 33
            }
            
            //reset main counter
            self.counter = 0
            self.updateCounter()
            
            self.setNavBar()
        })
        
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func playAudio(audioName: String){
        
        let soundPath = Bundle.main.path(forResource: "\(audioName)", ofType: "mp3")!
        let url = URL(fileURLWithPath: soundPath)
        
        do {
            avPlayer = try AVAudioPlayer(contentsOf: url)
            avPlayer?.play()
        } catch {
            // couldn't load file :(
        }
        
        
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                startForwardAnimation()
            case .left:
                print("Swiped left")
                startBackwardAnimation()
            default:
                break
            }
        }
    }
    
    func startForwardAnimation(){
        //playAudio(audioName: "tick")
        
        if counter < totalValue {
            counter = counter + 1
            self.backView.removeFromSuperview()
            initiateAnimation()
            updateCounter()
        }
        
        
        //Reset Counter
        if isRoundCompleted {
            self.counter = 1
            AllTotal = AllTotal + 1
            self.backView.removeFromSuperview()
            initiateAnimation()
            updateCounter()
            updateTotalCounter()
            isRoundCompleted = false
        }
    }
    
    func startBackwardAnimation(){
        //playAudio(audioName: "tock")
        
        if counter >= 0 {
            
            if counter == 0 {
                //no counter increment
                counter = totalValue - 1
            }else{
                counter = counter - 1
            }
            
            self.goView.removeFromSuperview()
            initiateBackAnimation()
            updateCounter()
            
        }
        
    }
    
    func updateCounter() {
        lblLimit.text = "\(counter) / \(totalValue)"
    }
    
    func updateTotalCounter () {
        lblTotal.text = "Total = \(AllTotal)"
    }
    func checkForFinalCounter() {
        if counter == totalValue {
            isRoundCompleted = true
        }else{
            isRoundCompleted = false
        }
    }
    
    func addBackAnimation(){
        
        let moveBackPath = CAKeyframeAnimation(keyPath: "position")
        moveBackPath.path = curevedPath(isReversed: true).cgPath
        moveBackPath.duration = 1
        moveBackPath.repeatCount = 0
        moveBackPath.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        moveBackPath.delegate = self
        self.moveBackPath = moveBackPath
        
    }
    
    func addAnimation() {
        
        let moveForwardPath = CAKeyframeAnimation(keyPath: "position")
        moveForwardPath.path = curevedPath(isReversed: false).cgPath
        moveForwardPath.duration = 1
        moveForwardPath.repeatCount = 0
        //moveForwardPath.calculationMode = kCAAnimationPaced
        moveForwardPath.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        moveForwardPath.delegate = self
        self.moveAlongPath = moveForwardPath
    }
    
    func curevedPath(isReversed: Bool) -> UIBezierPath {
        
        var path = UIBezierPath()
        if isReversed {
            path = createReversePath()
        }else{
            path = createCurvePath()
        }
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.darkGray.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 1.0
        //self.view.layer.addSublayer(shapeLayer)
        self.view.layer.insertSublayer(shapeLayer, at: 0)
        
        return path
    }
    
    
    //MARK:- Custom Curve Path
    func createCurvePath() -> UIBezierPath {
        
        forwardPath.move(to: startPoint)
        forwardPath.addQuadCurve(to: endPoint, controlPoint: controlPoint)
        return forwardPath
    }
    
    
    func initiateAnimation() {
        let layer = createLayer()
        layer.add(moveAlongPath, forKey: "animate forward path")
        
    }
    
    //MARK:- Custom View Path
    func createLayer() -> CALayer {
        
        self.view.addSubview(goView)
        
        //place out of screen constraints
        goView.translatesAutoresizingMaskIntoConstraints = false
        goView.addConstaintsToSuperview(leftOffset: 10, topOffset: -150)
        goView.addConstaints(height: 50, width: 50)
        
        
        let customlayer = goView.layer
        customlayer.bounds = CGRect(x: 0, y: 0, width: 50, height: 50)
        customlayer.position = CGPoint(x: 25, y: 25)
        return customlayer
    }
    
    //MARK:- Backward Animation
    
    func initiateBackAnimation() {
        let layer = createBackLayer()
        layer.add(moveBackPath, forKey: "animate forward path")//animate back Path
    }
    
    func createReversePath() -> UIBezierPath {
        backwardPath.move(to: endPoint)
        backwardPath.addQuadCurve(to: startPoint, controlPoint: controlPoint)
        return backwardPath
    }
    
    func createBackLayer() -> CALayer {
        
        self.view.addSubview(backView)
        
        //place out of screen constraints
        backView.translatesAutoresizingMaskIntoConstraints = false
        backView.addConstaintsToSuperview(leftOffset: 10, topOffset: -150)
        backView.addConstaints(height: 50, width: 50)
        
        let customlayer = backView.layer
        customlayer.bounds = CGRect(x: 375, y: 200, width: 50, height: 50)
        customlayer.position = CGPoint(x: 25, y: 25)
        return customlayer
    }
    
}


extension TasbeehVC: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            self.goView.removeFromSuperview()
            self.backView.removeFromSuperview()
            playAudio(audioName: "tock")
            checkForFinalCounter()
        }
    }
}


class CustomView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    func setUpView() {
        let image = UIImage(named: "bead.png")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        
        addSubview(imageView)
        //backgroundColor = .orange
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class CustomBackView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    func setUpView() {
        let image = UIImage(named: "bead.png")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: 375, y: 200, width: self.bounds.width, height: self.bounds.height)
        
        addSubview(imageView)
        //backgroundColor = .yellow
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}




extension UIView {
    
    public func addConstaintsToSuperview(leftOffset: CGFloat, topOffset: CGFloat) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: self,
                           attribute: .leading,
                           relatedBy: .equal,
                           toItem: self.superview,
                           attribute: .leading,
                           multiplier: 1,
                           constant: leftOffset).isActive = true
        
        NSLayoutConstraint(item: self,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: self.superview,
                           attribute: .top,
                           multiplier: 1,
                           constant: topOffset).isActive = true
    }
    
    public func addConstaints(height: CGFloat, width: CGFloat) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: self,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1,
                           constant: height).isActive = true
        
        
        NSLayoutConstraint(item: self,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1,
                           constant: width).isActive = true
    }
}

extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}
