//
//  AboutUsVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 30/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    @IBOutlet weak var lblAboutUs: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let htmlText = """
        <html>
        <body>
        <p style="color: black;font-size:20px">Inabia offers innovative mobile application design and solution services that connect businesses and users together. \nWe have well experienced people of mobile apps developers and designers, testers, consultants who can provide complete solution from initial idea, design, development, implementation and on-going support
        <br>For more information contact us at <br>development@inabia.com</p>
        </body>
        </html>
        """
        
        //let data = Data(html.utf8)
        
        //if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
            // use your attributed string somehow
            //lblAboutUs.attributedText = attributedString
            
            lblAboutUs.attributedText = htmlText.htmlToAttributedString
       // }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }


}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
