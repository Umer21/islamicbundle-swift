//
//  RelatedCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 30/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class RelatedCell: UITableViewCell {

    @IBOutlet weak var lblAppName: UILabel!
    @IBOutlet weak var appimg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
