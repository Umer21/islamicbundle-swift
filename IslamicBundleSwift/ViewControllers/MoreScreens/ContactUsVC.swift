//
//  ContactUsVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 30/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {

    @IBOutlet weak var lblContactUs: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let html = """
               <html>
               <body>
               <p style="color: black;font-size:20px">We try to be most responsive to your inquiries and mails. Towards this we connect you directly with the people who will respond to your mails and calls.<br><br><b>Head Office</b>\n7901 168th Ave NE, Suite 103 Redmond, WA 98052, USA
               <br><br><b>Phone</b>:425-968-5269
               <br><br><b>Email Address</b>: development@inabia.com
               <br><br><b>Fax</b>: 425-484-7733</p>
               </body>
               </html>
               """
    
               let data = Data(html.utf8)
               
               if let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
                   // use your attributed string somehow
                   lblContactUs.attributedText = attributedString
                
                
               }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
}
