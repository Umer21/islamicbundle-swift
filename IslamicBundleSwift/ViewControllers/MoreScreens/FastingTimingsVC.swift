//
//  FastingTimingsVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 01/12/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import GCCalendar
import Adhan

class FastingTimingsVC: UIViewController {
    
    var calendarView: GCCalendarView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var lblCurrent: UILabel!
    @IBOutlet weak var lblHijriDate: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblCurrentDate: UILabel!
    @IBOutlet weak var lblCurrentLocation: UILabel!
    @IBOutlet weak var lblSeharTime: UILabel!
    @IBOutlet weak var lblIftaarTime: UILabel!
    @IBOutlet weak var imgCurrentNamaz: UIImageView!
    
    var namazName: String = ""
    var namazTime: String = ""
    var countdownTimer = Timer()
    var items =  [PrayerModel]()
    var isPrayerFajr = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addCalendarview()
        
        if isKeyPresentInUserDefaults(key: "CurrentLocation"){
            lblCurrentLocation.text = getValueForKey(keyValue: "CurrentLocation")
        }
        else{
            lblCurrentLocation.text = "Mecca, Saudia Arabia"
        }
        
        setNavBar()
    }
    
    func setNavBar() {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    
    func addCalendarview() {
        
        self.calendarView = GCCalendarView()
        calendarView.delegate = self
        calendarView.displayMode = .week
        
        calendarView.translatesAutoresizingMaskIntoConstraints = false
        //self.hCalendarView.addSubview(calendarView)
        self.view.addSubview(calendarView)
        
        calendarView.topAnchor.constraint(equalTo: self.infoView.bottomAnchor, constant: 12).isActive = true
        calendarView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        calendarView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        calendarView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }
    
    func displayData(fromCalendar: String, dDate: Date)
    {
        let cpdetail = getCurrentPrayerDetail()
        
        switch cpdetail[0].NextNamazName! {
        case Prayer.fajr:
            
            namazName = "Fajr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"fajr_icon")
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Fajr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            break
        case Prayer.sunrise:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Sunrise")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Iftaar"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"magrib_icon")
            
            break
        case Prayer.dhuhr:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Dhuhr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Iftaar"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"magrib_icon")
            
            break
        case Prayer.asr:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Asr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Iftaar"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"magrib_icon")
            
            break
        case Prayer.maghrib:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Maghrib")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Iftaar"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"magrib_icon")
            
            break
        case Prayer.isha:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Isha")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Sehar"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"fajr_icon")
            
            break
        default:
            print("no Namaz")
        }
        
       // lblCurrent.text = namazName + "\n" + namazTime
        
        if fromCalendar == "Yes" {
            let currentDate = dDate.string(format: "dd MMMM yyyy")
            lblHijriDate.text = getHijriDate(gregorianDate: currentDate)
            lblCurrentDate.text = currentDate
        }
        else {
            let currentDate = Date().string(format: "dd MMMM yyyy")
            lblHijriDate.text = getHijriDate(gregorianDate: currentDate)
            lblCurrentDate.text = currentDate
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        self.tabBarController?.tabBar.isHidden = true
        displayData(fromCalendar: "No", dDate: Date())
        getSeharIftaarTimings(dDate: Date())
        startTimer()
    }
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        var date = cal.dateComponents([.year, .month, .day], from: Date())
        
        var lat: Double?
        var lng: Double?
        
        if isKeyPresentInUserDefaults(key: "Latitude"){
            lat = Double (getValueForKey(keyValue: "Latitude"))
            lng = Double (getValueForKey(keyValue: "Longitude"))
        }
        else{
            lat = 21.3891
            lng = 39.8579
        }
        
        let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
        let jurists = getValueForKey(keyValue: "Jurists")
        
        let coordinates = Coordinates(latitude: lat!, longitude: lng!)
        var params = CalculationMethod.karachi.params
        
        switch prayerMethod {
        case "0":
            params = CalculationMethod.karachi.params
            break
        case "1":
            params = CalculationMethod.northAmerica.params
            break
        case "2":
            params = CalculationMethod.muslimWorldLeague.params
            break
        case "3":
            params = CalculationMethod.ummAlQura.params
            break
        case "4":
            params = CalculationMethod.egyptian.params
            break
        case "5":
            params = CalculationMethod.tehran.params
            break
        case "6":
            params = CalculationMethod.other.params
            break
            
        default:
            params = CalculationMethod.karachi.params
            break
        }
        
        switch jurists {
        case "0":
            params.madhab = .shafi
            break
        case "1":
            params.madhab = .hanafi
            break
            
        default:
            params.madhab = .shafi
            break
        }
        
        var prayerTimes = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params)
        
        let current = prayerTimes?.currentPrayer()
        
        var countdown = Date()
        if current == Prayer.isha {
            let today = Date()
            let modifiedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
            date = cal.dateComponents([.year, .month, .day], from: modifiedDate)
            prayerTimes = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params)
            
            let next = prayerTimes!.nextPrayer()
            //print("Next: \(next)")
            countdown = prayerTimes!.time(for: next!)
            //print("Countdown: \(countdown)")
            isPrayerFajr = true
        }
        else {
            let next = prayerTimes!.nextPrayer()
            //print("Next: \(next)")
            countdown = prayerTimes!.time(for: next!)
            //print("Countdown: \(countdown)")
            
            isPrayerFajr = false
        }
        
        let currentDate = Date()
        let calendar = Calendar.current
        
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: countdown as Date)
        
        let countdown1 = "\(String(format:"%02d", diffDateComponents.hour ?? 0)) : \(String(format:"%02d", diffDateComponents.minute ?? 0)) : \(String(format:"%02d", diffDateComponents.second ?? 0))"
        
      
        //print("Next Prayer=================> \(prayerTimes?.nextPrayer())")
        if prayerTimes?.nextPrayer() == Prayer.fajr {
            //print("Next Prayer is fajr")
            //----------------------------------------------------------------
             //print("----------------Fajr-----------------")
            /* var dayComponent = DateComponents()
             dayComponent.day = 1
             let theCalendar = Calendar.current
             let nextDate = theCalendar.date(byAdding: dayComponent, to: Date())*/
             let countdownFajr = prayerTimes?.fajr
             
             let calendarFajr = Calendar.current
             
             let diffDateComponentsFajr = calendarFajr.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: countdownFajr!)
             
             let countdown122Fajr = "\(String(format:"%02d", diffDateComponentsFajr.hour ?? 0)) : \(String(format:"%02d", diffDateComponentsFajr.minute ?? 0)) : \(String(format:"%02d", diffDateComponentsFajr.second ?? 0))"
             lblTimer.text = countdown122Fajr
            // print("Countdown-fajr: \(countdown122Fajr)")
             lblCurrent.text = "Sehar" + "\n" + items[0].prayerTime!
        }
        else  {
            //print("Next Prayer is other than fajr")
            //print("----------------Maghrib-----------------")
            let countdownMaghrib = prayerTimes?.maghrib
            
            let calendarMaghrib = Calendar.current
            
            let diffDateComponentsMaghrib = calendarMaghrib.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: countdownMaghrib!)
            
            let countdown1Maghrib = "\(String(format:"%02d", diffDateComponentsMaghrib.hour ?? 0)) : \(String(format:"%02d", diffDateComponentsMaghrib.minute ?? 0)) : \(String(format:"%02d", diffDateComponentsMaghrib.second ?? 0))"
            lblTimer.text = countdown1Maghrib
            lblCurrent.text = "Iftaar" + "\n" + items[1].prayerTime!
           // print("Countdown-maghrib: \(countdown1Maghrib)")
        }
        
    }
    
    func getSeharIftaarTimings(dDate: Date)
    {
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let date = cal.dateComponents([.year, .month, .day], from: dDate)
        
        var lat: Double?
        var lng: Double?
        
        items.removeAll()
        
        if isKeyPresentInUserDefaults(key: "Latitude"){
            lat = Double (getValueForKey(keyValue: "Latitude"))
            lng = Double (getValueForKey(keyValue: "Longitude"))
        }
        else{
            lat = 21.3891
            lng = 39.8579
        }
        
        let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
        let jurists = getValueForKey(keyValue: "Jurists")
        
        let coordinates = Coordinates(latitude: lat!, longitude: lng!)
        var params = CalculationMethod.karachi.params
        
        switch prayerMethod {
        case "0":
            params = CalculationMethod.karachi.params
            break
        case "1":
            params = CalculationMethod.northAmerica.params
            break
        case "2":
            params = CalculationMethod.muslimWorldLeague.params
            break
        case "3":
            params = CalculationMethod.ummAlQura.params
            break
        case "4":
            params = CalculationMethod.egyptian.params
            break
        case "5":
            params = CalculationMethod.tehran.params
            break
        case "6":
            params = CalculationMethod.other.params
            break
            
        default:
            params = CalculationMethod.karachi.params
            break
        }
        
        switch jurists {
        case "0":
            params.madhab = .shafi
            break
        case "1":
            params.madhab = .hanafi
            break
        
        default:
            params.madhab = .shafi
            break
        }
        
        if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            
            if isKeyPresentInUserDefaults(key: "TimeZone"){
                let tz = getValueForKey(keyValue: "TimeZone")
                formatter.timeZone = TimeZone(identifier: tz)!
            }
            else{
                let tz = "Asia/Riyadh"
                formatter.timeZone = TimeZone(identifier: tz)!
            }
            
            let fModel = PrayerModel()
            fModel.prayerName = "Fajr"
            fModel.prayerTime = formatDateForDisplay1(date: prayers.fajr)
            print("Fajr time:- \(fModel.prayerTime)---\(prayers.fajr)")
            items.append(fModel)
            
            let mModel = PrayerModel()
            mModel.prayerName = "Maghrib"
            mModel.prayerTime = formatDateForDisplay1(date: prayers.maghrib)
            print("Maghrib time:- \(mModel.prayerTime)---\(prayers.maghrib)")
            items.append(mModel)
            
        }
        
        lblSeharTime.text = items[0].prayerTime
        lblIftaarTime.text = items[1].prayerTime
    }
}

extension FastingTimingsVC: GCCalendarViewDelegate {
    
    func calendarView(_ calendarView: GCCalendarView, didSelectDate date: Date, inCalendar calendar: Calendar) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.calendar = calendar
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "dd MMMM yyyy", options: 0, locale: calendar.locale)
        
        displayData(fromCalendar: "Yes", dDate: date)
        getSeharIftaarTimings(dDate: date)
        /*let currentDate = date.string(format: "dd MMMM yyyy")
        lblHijriDate.text = getHijriDate(gregorianDate: currentDate)
        lblCurrentDate.text = currentDate*/
    }
    
    func currentDateSelectedBackgroundColor(calendarView: GCCalendarView) -> UIColor
    {
        return UIColorFromRGB(rgbValue: 0x462D60)
    }
    
    func currentDateTextColor(calendarView: GCCalendarView) -> UIColor
    {
        return UIColorFromRGB(rgbValue: 0x462D60)
    }
    
    
    
}
