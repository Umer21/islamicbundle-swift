//
//  PTrackerCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 01/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class PTrackerCell: UITableViewCell {

    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblPrayerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
