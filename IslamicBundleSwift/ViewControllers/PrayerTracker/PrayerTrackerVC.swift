//
//  PrayerTrackerVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 01/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import FSCalendar_Persian

class PrayerTrackerVC: UIViewController, FSCalendarDelegate, FSCalendarDataSource {

    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var pTrackerTableView: UITableView!
    var arrRelated = ["Tahajud","Fajr","Ishraq","Chasht","Dhuhr","Asr","Maghrib","Isha"]
    
    @IBOutlet weak var sepView: UIView!
    var datesWithEvent = [String]()
    
    var arrPrayerTracker = [PrayerTrackModel]()
    
    var arrPrayerTrackerTemp = [PrayerTrackModel]()
    
    var strDate: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        calendar.delegate = self
        calendar.dataSource = self
        
        sepView.setGradientBackground(colorTop: UIColor.darkGray, colorBottom: UIColor.lightGray)
        
       // let formatter = DateFormatter()
        //formatter.dateFormat = ("M/d/yyyy")
        strDate = Date().string(format: "M/d/yyyy")//formatter.string(from: Date())
        print("AB---First-Current-Date----->  \(strDate)")
        
        //let currentDateForNames = Date().string(format: "M/d/yyyy")
        
        arrPrayerTracker = DatabaseHelper.getAllPrayerTrack(trackerDate: strDate)
        
        arrPrayerTrackerTemp = DatabaseHelper.getAllPrayerDates()
        
        if arrPrayerTrackerTemp.count > 0{
            for dt in arrPrayerTrackerTemp{
                datesWithEvent.append(dt.trackDate!)
            }
        }
        else{
            let fModel = PrayerTrackModel()
            fModel.trackId = 1
            fModel.tahajjud = 0
            fModel.fajr = 0
            fModel.ishraq = 0
            fModel.chasht = 0
            fModel.dhuhr = 0
            fModel.asr = 0
            fModel.maghrib = 0
            fModel.isha = 0
            fModel.trackDate = strDate
            arrPrayerTracker.append(fModel)
        }
        
        calendar.reloadData()
        pTrackerTableView.reloadData()
        
        //calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        strDate = formatter.string(from: date)
        print("AB---Clicked---date-->  \(strDate)")
        
        arrPrayerTracker = DatabaseHelper.getAllPrayerTrack(trackerDate: strDate)
        
        if arrPrayerTracker.count > 0{
            
        }
        else{
            let fModel = PrayerTrackModel()
            fModel.trackId = 1
            fModel.tahajjud = 0
            fModel.fajr = 0
            fModel.ishraq = 0
            fModel.chasht = 0
            fModel.dhuhr = 0
            fModel.asr = 0
            fModel.maghrib = 0
            fModel.isha = 0
            fModel.trackDate = strDate
            arrPrayerTracker.append(fModel)
        }
        
        pTrackerTableView.reloadData()
        //calendar.reloadData()
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        print("NumberOfEvent----PrayerTracker--")
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        let strDate1 = formatter.string(from: date)
        
        arrPrayerTrackerTemp = DatabaseHelper.getAllPrayerDates()
        datesWithEvent.removeAll()
        for dt in arrPrayerTrackerTemp{
            datesWithEvent.append(dt.trackDate!)
        }

        if self.datesWithEvent.contains(strDate1) {
            return 1
        }

        return 0
    }
    
    /*func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        /*if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]*/
        
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        let str = formatter.string(from: date)
        print("Events---->> \(str)")
        
        if self.datesWithEvent.contains(str) {
            print("Events--1-->> \(str)")
            return [UIColor.orange]
        }
        
        return [appearance.eventDefaultColor]
    }*/
    
    /*func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        print("willDisplay---->>")
        
        let dateFormatter3 = DateFormatter()
        dateFormatter3.dateFormat = "M/d/yyyy"
        let dateString = dateFormatter3.string(from: date)

        //display events as dots
        cell.eventIndicator.isHidden = false
        cell.eventIndicator.color = UIColor.blue

        if self.datesWithEvent.contains(dateString){
            cell.eventIndicator.numberOfEvents = 3
        }
    }*/
    
   /* func calendar(calendar: FSCalendar!, shouldOutlineEventsForDate date: Date!) -> Bool! {
        
        print("shouldOutlineEventsForDate---->>")
        
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        strDate = formatter.string(from: date)
        //print("Events---->> \(str)")
        
        if self.datesWithEvent.contains(strDate) {
           // print("Events--1-->> \(str)")
            return true
        }
        return false
    }*/

    // FSCalendarDataSource
    /*func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        
        print("imageFor---->>")
        
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        let str = formatter.string(from: date)
        print("Events---->> \(str)")
        
        if self.datesWithEvent.contains(str) {
            print("Events--1-->> \(str)")
            return UIImage(named: "hollow_circle")
        }
        
        return nil
    }*/
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }

}

extension UIView{
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor){
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.locations = [NSNumber(floatLiteral: 0.0), NSNumber(floatLiteral: 1.0)]
            gradientLayer.frame = bounds

            layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension PrayerTrackerVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRelated.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PTrackerCell
        
        cell.lblPrayerName.text = self.arrRelated[indexPath.section]
        
        if arrPrayerTracker.count > 0{
            let alarmStatus = getPrayerStatusvalues(index: indexPath.section, namaz: arrPrayerTracker)
           
            if alarmStatus == 1{
                
                cell.imgCheck.image = UIImage(named: "icon_prayercheck_green")
            }
            else{
                
                cell.imgCheck.image = UIImage(named: "icon_prayercheck")
            }
        }
        else{
            cell.imgCheck.image = UIImage(named: "icon_prayercheck")
        }
        
        
        cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        //------------------------------
        switch indexPath.section {
        case 0:
            if arrPrayerTracker[0].tahajjud == 1 {
                arrPrayerTracker[0].tahajjud = 0
            }
            else{
                arrPrayerTracker[0].tahajjud = 1
            }
            
            break
        case 1:
            if arrPrayerTracker[0].fajr == 1 {
                arrPrayerTracker[0].fajr = 0
            }
            else{
                arrPrayerTracker[0].fajr = 1
            }
            
            break
        case 2:
            if arrPrayerTracker[0].ishraq == 1 {
                arrPrayerTracker[0].ishraq = 0
            }
            else{
                arrPrayerTracker[0].ishraq = 1
            }
            
            break
        case 3:
            if arrPrayerTracker[0].chasht == 1 {
                arrPrayerTracker[0].chasht = 0
            }
            else{
                arrPrayerTracker[0].chasht = 1
            }
            
            break
        case 4:
            if arrPrayerTracker[0].dhuhr == 1 {
                arrPrayerTracker[0].dhuhr = 0
            }
            else{
                arrPrayerTracker[0].dhuhr = 1
            }
            
            break
        case 5:
            if arrPrayerTracker[0].asr == 1 {
                arrPrayerTracker[0].asr = 0
            }
            else{
                arrPrayerTracker[0].asr = 1
            }
            
            
            break
        case 6:
            if arrPrayerTracker[0].maghrib == 1 {
                arrPrayerTracker[0].maghrib = 0
            }
            else{
                arrPrayerTracker[0].maghrib = 1
            }
            
            break
        case 7:
            if arrPrayerTracker[0].isha == 1 {
                arrPrayerTracker[0].isha = 0
            }
            else{
                arrPrayerTracker[0].isha = 1
            }
            
            break
            
        default:
            print("0")
        }
        //-----------------------------
        
        if DatabaseHelper.CheckPrayerExist(trackerDate: strDate){
            print("AB---------------Update-Date--\(strDate)")
            DatabaseHelper.UpdatePrayerDetail(trackId: 1, tahajud: arrPrayerTracker[0].tahajjud!, fajr: arrPrayerTracker[0].fajr!, Ishraq: arrPrayerTracker[0].ishraq!, chasht: arrPrayerTracker[0].chasht!, dhuhr: arrPrayerTracker[0].dhuhr!, asr: arrPrayerTracker[0].asr!, maghrib: arrPrayerTracker[0].maghrib!, isha: arrPrayerTracker[0].isha!, trackerDate: strDate)
        }
        else{
            print("AB---------------Insert-Date--\(strDate)")
            DatabaseHelper.InsertPrayerTrackDetail(trackId: 1, tahajud: arrPrayerTracker[0].tahajjud!, fajr: arrPrayerTracker[0].fajr!, Ishraq: arrPrayerTracker[0].ishraq!, chasht: arrPrayerTracker[0].chasht!, dhuhr: arrPrayerTracker[0].dhuhr!, asr: arrPrayerTracker[0].asr!, maghrib: arrPrayerTracker[0].maghrib!, isha: arrPrayerTracker[0].isha!, trackerDate: strDate)
        }
        
        pTrackerTableView.reloadData()
        calendar.reloadData()
    }
}
