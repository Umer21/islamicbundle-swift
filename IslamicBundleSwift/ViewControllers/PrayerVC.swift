//
//  PrayerVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import GCCalendar
import Adhan
import MarqueeLabel

class PrayerVC: UIViewController {
    
    var calendarView: GCCalendarView!
    var items =  [PrayerModel]()
    
    @IBOutlet weak var lblPrayerExtras: MarqueeLabel!
    @IBOutlet weak var lblCurrentNamaz: UILabel!
    @IBOutlet weak var imgCurrentNamaz: UIImageView!
    @IBOutlet weak var lblCurrentDate: UILabel!
    let arrMore2 = ["Fajr","Sunrise","Dhuhr","Asr","Maghrib","Isha"]
    
    @IBOutlet weak var prayersTableView: UITableView!
    @IBOutlet weak var infoImageView: UIImageView!
    
    var currPrayer: String?
    var currPosition: Int?
    
    var namazName: String = ""
    var namazTime: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavBar()
        addCalendarview()
        
        
        // getLocalNotificationCount()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(updateTimingsFired), name: Notification.Name("UPDATE_PRAYER_TIMINGS"), object: nil)
        
        prayersTableView.isScrollEnabled = false
        prayersTableView.separatorColor = UIColorFromRGB(rgbValue: 0x462D60)
        
        
        //CurrentNamazTime
        //prayersTableView.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
       /*UIView.animate(withDuration: 12.0, delay: 1, options: ([.curveLinear, .repeat]), animations: {() -> Void in
           self.lblExtraPrayers.center = CGPoint(x: 0 - self.lblExtraPrayers.bounds.size.width / 2, y: self.lblExtraPrayers.center.y)
       }, completion:  { _ in })*/
        
        /*UIView.animate(withDuration: 12.0, delay:1, options: [.repeat], animations: {
            self.lblExtraPrayers.frame = CGRect(x: self.lblExtraPrayers.frame.origin.x - 380, y: self.lblExtraPrayers.frame.origin.y - 0, width: self.lblExtraPrayers.frame.size.width, height: self.lblExtraPrayers.frame.size.height)
        }, completion: nil)*/
        
        getAdhanTimings(dDate: Date())
        
        displayData(fromCalendar: "No", dDate: Date())
        
        //displayData()
        //getAdhanTimings()
        
        //prayersTableView.reloadData()
    }
    
    @objc func updateTimingsFired (){
        
        print("Update-Prayer-Timings---------dDate: Date()---------->>>> ")
        getAdhanTimings(dDate: Date())
        displayData(fromCalendar: "No", dDate: Date())
        //prayersTableView.reloadData()
    }
    
    func displayData(fromCalendar: String, dDate: Date)
    {
        let cpdetail = getCurrentPrayerDetail()
        
        switch cpdetail[0].NextNamazName! {
        case Prayer.fajr:
            print("Fajr")
            
            namazName = "Fajr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"fajr_icon")
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Fajr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            //prayerIndex = 0
            //prayerName = "Fajr"
            break
        case Prayer.sunrise:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Sunrise")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Sunrise"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"afternoon_icon")
            
            //prayerIndex = 1
            //prayerName = "Sunrise"
            break
        case Prayer.dhuhr:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Dhuhr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Dhuhr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"dhuhr_icon")
            
            //prayerIndex = 2
            //prayerName = "Dhuhr"
            break
        case Prayer.asr:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Asr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Asr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"asr_icon")
            
            //prayerIndex = 3
            //prayerName = "Asr"
            break
        case Prayer.maghrib:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Maghrib")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Maghrib"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"magrib_icon")
            
            //prayerIndex = 4
            //prayerName = "Maghrib"
            break
        case Prayer.isha:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Isha")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Isha"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrentNamaz.image = UIImage(named:"fajr_icon")
            
            //prayerIndex = 5
            //prayerName = "Isha"
            break
        default:
            print("no Namaz")
        }
        
        
        lblCurrentNamaz.text = namazName + "\n" + namazTime
        
        if fromCalendar == "Yes" {
            let currentDate = dDate.string(format: "dd MMMM yyyy")
            lblCurrentDate.text = currentDate + "\n" + getHijriDate(gregorianDate: currentDate)
        }
        else {
            let currentDate = Date().string(format: "dd MMMM yyyy")
            lblCurrentDate.text = currentDate + "\n" + getHijriDate(gregorianDate: currentDate)
        }
    }
    
    
    
    func addCalendarview() {
        
        self.calendarView = GCCalendarView()
        calendarView.delegate = self
        calendarView.displayMode = .week
        
        
        calendarView.translatesAutoresizingMaskIntoConstraints = false
        //self.hCalendarView.addSubview(calendarView)
        self.view.addSubview(calendarView)
        
        calendarView.topAnchor.constraint(equalTo: self.infoImageView.bottomAnchor, constant: 12).isActive = true
        calendarView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        calendarView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        calendarView.heightAnchor.constraint(equalToConstant: 80).isActive = true
    }
    
    
    
    func getAdhanTimings(dDate: Date)
    {
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let date = cal.dateComponents([.year, .month, .day], from: dDate)
        
        var lat: Double?
        var lng: Double?
        
        items.removeAll()
        
        if isKeyPresentInUserDefaults(key: "Latitude"){
            //print("Exist")
            lat = Double (getValueForKey(keyValue: "Latitude"))
            //print("P-lat---> \(lat)")
            lng = Double (getValueForKey(keyValue: "Longitude"))
            //print("P-lng---> \(lng)")
        }
        else{
           // print("Not Exist")
            lat = 21.3891
            lng = 39.8579
        }
        
        //-------------
        let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
        print("P-PrayerMethod---> \(prayerMethod)")
        let jurists = getValueForKey(keyValue: "Jurists")
        print("P-Jurists---> \(jurists)")
        
        let coordinates = Coordinates(latitude: lat!, longitude: lng!)
        var params = CalculationMethod.karachi.params
        
        switch prayerMethod {
        case "0":
            params = CalculationMethod.karachi.params
            break
        case "1":
            params = CalculationMethod.northAmerica.params
            break
        case "2":
            params = CalculationMethod.muslimWorldLeague.params
            break
        case "3":
            params = CalculationMethod.ummAlQura.params
            break
        case "4":
            params = CalculationMethod.egyptian.params
            break
        case "5":
            params = CalculationMethod.tehran.params
            break
        case "6":
            params = CalculationMethod.other.params
            break
            
        default:
            params = CalculationMethod.karachi.params
            break
        }
        
        
        switch jurists {
        case "0":
            params.madhab = .shafi
            break
        case "1":
            params.madhab = .hanafi
            break
        
        default:
            params.madhab = .shafi
            break
        }
        //-------------------
       // let coordinates = Coordinates(latitude: lat!, longitude: lng!)
        //var params = CalculationMethod.karachi.params
        //params.madhab = .hanafi
        if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            
            if isKeyPresentInUserDefaults(key: "TimeZone"){
                let tz = getValueForKey(keyValue: "TimeZone")
                print("P-tz--- \(tz)")
                formatter.timeZone = TimeZone(identifier: tz)!
            }
            else{
                let tz = "Asia/Riyadh"
                print("P-tz--- \(tz)")
                formatter.timeZone = TimeZone(identifier: tz)!
            }
            
            let fModel = PrayerModel()
            fModel.prayerName = "Fajr"
            fModel.prayerTime = formatDateForDisplay1(date: prayers.fajr)//prayers.fajr
            items.append(fModel)
            
            let sModel = PrayerModel()
            sModel.prayerName = "Sunrise"
            sModel.prayerTime = formatDateForDisplay1(date: prayers.sunrise)//prayers.fajr
            items.append(sModel)
            
            let dModel = PrayerModel()
            dModel.prayerName = "Dhuhr"
            dModel.prayerTime = formatDateForDisplay1(date: prayers.dhuhr)//prayers.fajr
            items.append(dModel)
            
            let aModel = PrayerModel()
            aModel.prayerName = "Asr"
            aModel.prayerTime = formatDateForDisplay1(date: prayers.asr)//prayers.fajr
            items.append(aModel)
            
            let mModel = PrayerModel()
            mModel.prayerName = "Maghrib"
            mModel.prayerTime = formatDateForDisplay1(date: prayers.maghrib)//prayers.fajr
            items.append(mModel)
            
            let iModel = PrayerModel()
            iModel.prayerName = "Isha"
            iModel.prayerTime = formatDateForDisplay1(date: prayers.isha)//prayers.fajr
            items.append(iModel)
            
            let ishraqPrayer = formatDateWithAddingMinutes(date: prayers.sunrise)
            print("Ishraq-Prayers---\(ishraqPrayer)")
            
            let chaashtPrayer = formatDateWithAddingHours(date: (formatStringFToDate(date: ishraqPrayer)))
            print("Chaasht-Prayers---\(chaashtPrayer)")
            
            lblPrayerExtras.text = "Sunrise : \(formatDateForDisplay1(date: prayers.sunrise)) | Shurooq : \(ishraqPrayer) | Chaasht : \(chaashtPrayer) | Sunset : \(formatDateForDisplay1(date: prayers.maghrib))"
            
            lblPrayerExtras.type = .continuous
            lblPrayerExtras.scrollDuration = 8.0
            lblPrayerExtras.animationCurve = .easeInOut
            lblPrayerExtras.fadeLength = 10.0
            lblPrayerExtras.leadingBuffer = 30.0
            lblPrayerExtras.trailingBuffer = 20.0
            
            
            
           /* UIView.animate(withDuration: 12.0, delay:1, options: [.repeat], animations: {
                self.lblExtraPrayers.frame = CGRect(x: self.lblExtraPrayers.frame.origin.x - 380, y: self.lblExtraPrayers.frame.origin.y - 0, width: self.lblExtraPrayers.intrinsicContentSize.width, height: self.lblExtraPrayers.frame.size.height)
            }, completion: nil)*/
            
        }
        
        
        
        /*for it in items.enumerated() {
            let model = it
            print("P----> \(model.element.prayerName)")
            print("P----> \(model.element.prayerTime)")
        }*/
        
        
        prayersTableView.reloadData()
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let monthlyButton = UIBarButtonItem(image: UIImage(named: "icon_monthly"), style: .plain, target: self, action: #selector(monthlyTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [monthlyButton]
        
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)//2A203C
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Prayer"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    @objc func monthlyTapped() {
        gotoMonthlyPrayerScreen()
    }
    
    func gotoMonthlyPrayerScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kPrayerMonthlyControllerID) as! PrayerMonthlyVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension PrayerVC: GCCalendarViewDelegate {
    
    func calendarView(_ calendarView: GCCalendarView, didSelectDate date: Date, inCalendar calendar: Calendar) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.calendar = calendar
        dateFormatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM yyyy", options: 0, locale: calendar.locale)
        
        //self.navigationItem.title = dateFormatter.string(from: date)
        getAdhanTimings(dDate: date)
        displayData(fromCalendar: "Yes", dDate: date)
    }
    
    func currentDateSelectedBackgroundColor(calendarView: GCCalendarView) -> UIColor
    {
        return UIColorFromRGB(rgbValue: 0x462D60)
    }
    
    func currentDateTextColor(calendarView: GCCalendarView) -> UIColor
    {
        return UIColorFromRGB(rgbValue: 0x462D60)
    }
    
    
}

extension PrayerVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PrayerCell
        
        let model = self.items[indexPath.section]
        
        cell.lblPrayerName.text = model.prayerName
        cell.lblPrayerTime.text = model.prayerTime
        
        //cell.delegate = self
        //cell.btnNamazAlarm.tag = indexPath.section
        
        
        let alarmStatus = getNamazAlarmValue(index: indexPath.section, namaz: model.prayerName!)
        //print("---------alarm-status------\(alarmStatus)")
        if alarmStatus == "1"{
            let image = UIImage(named: "icon_dashboard_bell")
            let stencil = image!.withRenderingMode(.alwaysTemplate)
            cell.btnNamazAlarm.setImage(stencil, for: .normal)
        }
        else{
            let image = UIImage(named: "icon_dashboard_unbell")
            let stencil = image!.withRenderingMode(.alwaysTemplate)
            cell.btnNamazAlarm.setImage(stencil, for: .normal)
        }
        
        let curr_Namaz = namazName//getValueForKey(keyValue: "CurrentNamaz")
        //print("P--Current-namaz  \(curr_Namaz)")
        if (cell.lblPrayerName.text == curr_Namaz)
        {
            cell.backgroundColor = UIColorFromRGB(rgbValue: 0x462D60)
            cell.lblPrayerName.textColor = .white
            cell.lblPrayerTime.textColor = .white
            cell.btnNamazAlarm.tintColor = .white // set a color
        }
        else{
            cell.backgroundColor = .clear
            cell.lblPrayerName.textColor = UIColorFromRGB(rgbValue: 0x462D60)
            cell.lblPrayerTime.textColor = UIColorFromRGB(rgbValue: 0x462D60)
            cell.btnNamazAlarm.tintColor = UIColorFromRGB(rgbValue: 0x462D60)
        }
        
        //cell.btnAlarm.addTarget(self, action: #selector(PrayerVC.onClickedMapButton(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        if indexPath.section == 1{
            showToast(viewControl: self, titleMsg: "", msgTitle: "Sunrise alarm by default is silent")
        }
        else{
            let model = self.items[indexPath.section]
            currPrayer = model.prayerName
            currPosition = indexPath.section
            
            gotoSoundSelectionScreen()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height = self.prayersTableView.bounds.size.height / 6
        
        return height
    }
    
    func gotoSoundSelectionScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: kPrayerSoundControllerID) as! PrayerSoundVC
        vc.currentPrayer = currPrayer
        vc.currentPosition = currPosition
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*@objc func onClickedMapButton(_ sender: Any?) {
     
     print("Click-bell")
     }*/
    
}

extension Date {
    
    func startOfMonth() -> Date {
        let interval = Calendar.current.dateInterval(of: .month, for: self)
        return (interval?.start.toLocalTime())! // Without toLocalTime it give last months last date
    }
    
    func endOfMonth() -> Date {
        let interval = Calendar.current.dateInterval(of: .month, for: self)
        return interval!.end
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone    = TimeZone.current
        let seconds     = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func getNextMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: 1, to: self)
    }

    func getPreviousMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -1, to: self)
    }
    
}

/*extension PrayerVC: PrayerDelegate {
 
 func alarmTapped(tag: Int) {
 let alarmStatus = getNamazAlarmValue(index: tag, namaz: arrMore2[tag])
 
 if(alarmStatus == "1")
 {
 setNamazAlarmValue(index: tag, namaz: arrMore2[tag], value: "0")
 
 }
 else{
 setNamazAlarmValue(index: tag, namaz: arrMore2[tag], value: "1")
 }
 
 deleteAllNotifications()
 
 getAdhanTimingsExp()
 
 //getLocalNotificationCount()
 
 prayersTableView.reloadData()
 
 print(items)
 }
 
 }*/
