//
//  JuristMethodCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class JuristMethodCell: UITableViewCell {

    @IBOutlet weak var imgJSelected: UIImageView!
    @IBOutlet weak var lblJuristMethod: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
