//
//  HighLatitudeCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class HighLatitudeCell: UITableViewCell {

    @IBOutlet weak var imgHSelected: UIImageView!
    @IBOutlet weak var lblHighAltitude: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
