//
//  SettingsVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 04/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation
import GoogleMaps

class SettingsVC: UIViewController {

    @IBOutlet weak var settingTableView: UITableView!
    var indicator = UIActivityIndicatorView()
    let locationManager = CLLocationManager()
    let geocoder = GMSGeocoder()
    
    let arrPMethod = ["Islamic University, Karachi", "Islamic Society of North America", "Muslim World League", "Makkah, Umm al-Qura", "Egyptian General Authority", "Tehran Institute of Geo Physics", "Jafari - Ithna Ashari"]
    
    let arrJMethod = ["Standard (Shafi, Malki, Hanbli)", "Hanafi"]
    
    let arrHA = ["None", "Middle of the night", "1/7 of the night", "Angle based method"]
    
    let arrDA = ["Auto", "+1 hour", "-1 hour"]
    
    let arrQS = ["Show Transliteration", "Hide Transliteration"]
    
    let arrAS = ["Default", "Azan Audio 1","Azan Audio 2"]
    
    let arrHijri = ["0 day", "+1 day","+2 day","-1 day","-2 day"]
    
    var Lat = "12.2122"
    var Long = "65.3434"
    
    var currentLoc: String = ""
    
    let languageNameArr = ["English", "Urdu", "Indonesian", "Bengali", "German", "Hindi", "Malaysian", "Norwegian", "Russian", "Chinese","Tamil", "Albanian" , "Bosnian", "Czech" , "Dutch" , "Italian" , "Japanese" , "Nigerian", "Pashto" , "Persian" , "Polish" , "Portuguese", "Romanian","Somali" , "Spanish", "Swedish" , "Tatar" , "Thai" , "Turkish", "Uzbek"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
       
        
    }
    
    func setNavBar()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Settings"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
        settingTableView.reloadData()
        
    }
    
    func showActionSheetForLocation() {
        // Create the AlertController and add its actions like button in ActionSheet
        let actionSheetController = UIAlertController(title: nil, message: "Select your location", preferredStyle: .actionSheet)
        
        let cameraActionButton = UIAlertAction(title: "Locate Me", style: .default)
        { action -> Void in
            print("Locate me")
            self.getCurrentLocation()
            
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton = UIAlertAction(title: "Set Location Manually", style: .default)
        { action -> Void in
            print("Set Location Manually")
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.navigationController?.navigationBar.barTintColor = kPurpleColor
            autocompleteController.delegate = self
            
            self.present(autocompleteController, animated: true, completion: nil)
            
        }
        actionSheetController.addAction(galleryActionButton)
        
        let dismissActionButton = UIAlertAction(title: "Dismiss", style: .cancel) { action -> Void in
            print("Dismiss")
        }
        actionSheetController.addAction(dismissActionButton)
        
        /*if UIDevice.current.userInterfaceIdiom == .pad {
            
            actionSheetController.popoverPresentationController?.sourceView = self.locationButton
            actionSheetController.popoverPresentationController!.sourceRect = CGRect(x: 0,y: -10,width: 1.0,height: 1.0)
            
        }*/
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func getaddress(latitude: Double, longitude: Double) {
        
        let coordinate = CLLocationCoordinate2DMake(latitude,longitude)
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                //self.locationButton .setTitle("\(lines[0])", for: .normal)
                //self.locationButton.titleLabel?.lineBreakMode = .byTruncatingTail
                //print("-------\(lines[0])")
                //currentAdd(returnAddress: currentAddress)
                
                setDefaultValue(keyValue: "CurrentLocation", valueIs: lines[0])
                
                resetPrayerNotificationsOnSettingsChange()
            }
            
            self.locationManager.stopUpdatingLocation()
        }
        
        settingTableView.reloadData()
    }
    
    func getCurrentLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //If user denied location services
        let status = CLLocationManager.authorizationStatus()
        if status == .denied {
            gotoSettings()
        }
    }
    
    func gotoSettings() {
        let alertController = UIAlertController (title: "Location service are off. Please allow to fetch resturants.", message: "Go to Settings?", preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func gotoPrayerMethod() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingPrayerMethodControllerID) as! SettingPrayerMethodVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoJuristMethod() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingJuristControllerID) as! SettingJuristVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoHighAltitude() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingHighAltitudeControllerID) as! SettingHighAltitudeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoDayLight() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingDayLightControllerID) as! SettingDayLightVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoQuranSetting() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingQuranControllerID) as! SettingQuranVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoAdhanSetting() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingAdhanControllerID) as! SettingAdhanVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoHijriSetting() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingHijriControllerID) as! SettingHijriVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoQuranLanguagesScreen() {
        let sb = UIStoryboard.init(name: kQuranSurahListStoryboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kQuranLanguagesControllerID) as! QuranLanguagesVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        
      /*  let vc = self.storyboard?.instantiateViewController(withIdentifier: kQuranLanguagesControllerID) as! QuranLanguagesVC
        self.navigationController?.pushViewController(vc, animated: true) */
    }
    
}

extension SettingsVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations ==== \(locValue.latitude) \(locValue.longitude)")
        // print("place name = \(latLong(lat: Double (locValue.latitude), long: Double(locValue.longitude)))")
        
        indicator.stopAnimating()
        //apiResturants(Lat: "\(locValue.latitude)", Long: "\(locValue.longitude)")
        Lat = "\(locValue.latitude)"
        print("Latitude === -----------------\(Lat)")
        Long = "\(locValue.longitude)"
        print("Longitude=== -------------------\(Long)")
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (locValue.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (locValue.longitude))
        setTimeZone(lat: locValue.latitude, lng: locValue.longitude)
       /* let location = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
        let timeZone = TimezoneMapper.latLngToTimezone(location)
        
        if let tz = timeZone {
            
            print("---before-splitted--\(String(describing: tz))")
            
            let s = String(describing: tz)
            let splittedTz = s.split(separator: " ")
            
            setDefaultValue(keyValue: "TimeZone", valueIs: String(splittedTz[0]))
            print("--after--splitted---\(splittedTz[0])")
        }*/
        
        print("----get-timeZone---\(getValueForKey(keyValue: "TimeZone"))")
        getaddress(latitude: locValue.latitude, longitude: locValue.longitude)
    }
}

extension SettingsVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        print("Components \(String(describing: place.addressComponents))")
        //locationButton.setTitle(place.formattedAddress, for: .normal)
        
        setDefaultValue(keyValue: "CurrentLocation", valueIs: place.formattedAddress!)
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (place.coordinate.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (place.coordinate.longitude))
        
       // let location = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        
        setTimeZone(lat: place.coordinate.latitude, lng: place.coordinate.longitude)
        /*let timeZone = TimezoneMapper.latLngToTimezone(location)
        
        if let tz = timeZone {
            
            print("---before-splitted--\(String(describing: tz))")
            
            let s = String(describing: tz)
            let splittedTz = s.split(separator: " ")
            
            setDefaultValue(keyValue: "TimeZone", valueIs: String(splittedTz[0]))
            print("--after--splitted---\(splittedTz[0])")
        }*/
        
        settingTableView.reloadData()
        
        resetPrayerNotificationsOnSettingsChange()
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }else if section == 1{
            return 2
        }else if section == 2 {
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 62
    }
    
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.purple
        headerView.textInputMode
        
        return headerView
    }*/
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = UIColorFromRGB(rgbValue: 0x464155)
            //headerView.backgroundView?.backgroundColor = UIColor.purple
            headerView.textLabel?.textColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! SettingsCell
        
        let pMethod = Int(getValueForKey(keyValue: "PrayerMethod"))
        let jMethod = Int(getValueForKey(keyValue: "Jurists"))
        let highA = Int(getValueForKey(keyValue: "HighAltitude"))
        let dayL = Int(getValueForKey(keyValue: "DayLight"))
        let quranS = Int(getValueForKey(keyValue: "QuranSetting"))
        let adhanS = Int(getValueForKey(keyValue: "AdhanSetting"))
        let hijriS = Int(getValueForKey(keyValue: "HijriSetting"))
        let languageName = languageNameArr[Int(getValueForKey(keyValue: "SelectedLanguageIndex")) ?? 0]
            
            if indexPath.section == 0 {
                switch indexPath.row {
                case 0:
                    if isKeyPresentInUserDefaults(key: "CurrentLocation"){
                        currentLoc = getValueForKey(keyValue: "CurrentLocation")
                        print("Current-Location----> \(currentLoc)")
                    }
                    else{
                        currentLoc = "Mecca, Saudia Arabia"
                    }
                    cell.lblHeading.text = "Location"
                    cell.lblValue.text = currentLoc
                /*case 1:
                    cell.lblHeading.text = "Adhan"
                    cell.lblValue.text = arrAS[adhanS ?? 0] */
                case 1:
                    cell.lblHeading.text = "Hijri Correction"
                    cell.lblValue.text = arrHijri[hijriS ?? 0]
                case 2:
                    cell.lblHeading.text = ""
                    cell.lblValue.text = "Reset Notification"
                default:
                    break
                }
                
            }else if indexPath.section == 1 {
                switch indexPath.row {
                case 0:
                    cell.lblHeading.text = "Quran Settings"
                    cell.lblValue.text = arrQS[quranS ?? 0]
                case 1:
                    cell.lblHeading.text = "Quran Language"
                    cell.lblValue.text = languageName
                
                default:
                    break
                }
            }else if indexPath.section == 2 {
                switch indexPath.row {
                case 0:
                    cell.lblHeading.text = "Prayer Times Convention"
                    cell.lblValue.text = arrPMethod[pMethod ?? 0]
                case 1:
                    cell.lblHeading.text = "Asr Calculation - Juristic Method"
                    cell.lblValue.text = arrJMethod[jMethod ?? 0]
               /* case 2:
                    cell.lblHeading.text = "High Latitude Adjustment"
                    cell.lblValue.text = arrHA[highA ?? 0]
                case 3:
                    cell.lblHeading.text = "DayLight Saving Time"
                    cell.lblValue.text = arrDA[dayL ?? 0] */
                default:
                    break
                }
            }
            
            //cell.backgroundColor = kThemeColorGreen
            cell.selectionStyle = .none
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Prayer Times"
        case 1:
            return "Quran"
        case 2:
            return "Calculations"
        default:
            break
        }
        
        return ""
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                showActionSheetForLocation()
            }
            /* if indexPath.row == 1 {
               gotoAdhanSetting()
            } */
            if indexPath.row == 1 {
                gotoHijriSetting()
            }
            
            if indexPath.row == 2 {
                showResetDialog()
            }
        }
        
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                gotoQuranSetting()
            }
            
            if indexPath.row == 1 {
                gotoQuranLanguagesScreen()
            }
        }
        
        if indexPath.section == 2 {
            if indexPath.row == 0 {
               gotoPrayerMethod()
            }
            
            if indexPath.row == 1 {
               gotoJuristMethod()
            }
            
           /* if indexPath.row == 2 {
               gotoHighAltitude()
            }
            
            if indexPath.row == 3 {
               gotoDayLight()
            } */
        }
    }
    
    func showResetDialog()
    {
        let optionMenu = UIAlertController(title: nil, message: "Do you want to reset notifications?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Ok", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cancelAction)
        optionMenu.addAction(yesAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}

