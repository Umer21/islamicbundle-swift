//
//  SettingAdhanVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SettingAdhanVC: UIViewController {
    
    let arrMore1 = ["Default", "Azan Audio 1","Azan Audio 2"]

    @IBOutlet weak var tableviewAdhan: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableviewAdhan.separatorStyle = .none
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }

}

extension SettingAdhanVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMore1.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! AdhanSCell
        
        //let model = self.arrMore1[indexPath.section]
        
        cell.lblAdhanS.text = self.arrMore1[indexPath.section]
        
        let savedMethod = Int(getValueForKey(keyValue: "AdhanSetting"))
        
        if savedMethod == indexPath.section {
            cell.imgASelected.image = UIImage(named: "icon_prayercheck_green")
            cell.lblAdhanS.textColor = kThemeDarkGreen
        }else{
            cell.imgASelected.image = UIImage(named: "icon_prayercheck")
            cell.lblAdhanS.textColor = UIColor.darkGray
        }
        
        //cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        print("Clicked item----\(arrMore1[indexPath.section])")
        
        setDefaultValue(keyValue: "AdhanSetting", valueIs: String (indexPath.section))
        
        tableviewAdhan.reloadData()
        
    }
    
}
