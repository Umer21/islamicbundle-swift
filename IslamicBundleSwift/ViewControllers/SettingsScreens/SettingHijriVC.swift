//
//  SettingHijriVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SettingHijriVC: UIViewController {
    
    let arrMore1 = ["0 day", "+1 day","+2 day","-1 day","-2 day"]

    @IBOutlet weak var tableviewHijri: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableviewHijri.separatorStyle = .none
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }
    
}

extension SettingHijriVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMore1.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! HijriCell
        
        //let model = self.arrMore1[indexPath.section]
        
        cell.lblHijri.text = self.arrMore1[indexPath.section]
        
        let savedMethod = Int(getValueForKey(keyValue: "HijriSetting"))
        
        if savedMethod == indexPath.section {
            cell.imgHSelected.image = UIImage(named: "icon_prayercheck_green")
            cell.lblHijri.textColor = kThemeDarkGreen
        }else{
            cell.imgHSelected.image = UIImage(named: "icon_prayercheck")
            cell.lblHijri.textColor = UIColor.darkGray
        }
        
        //cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        print("Clicked item----\(arrMore1[indexPath.section])")
        
        setDefaultValue(keyValue: "HijriSetting", valueIs: String (indexPath.section))
        
        tableviewHijri.reloadData()
        
    }
    
}
