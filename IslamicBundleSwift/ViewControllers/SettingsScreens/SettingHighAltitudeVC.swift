//
//  SettingHighAltitudeVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SettingHighAltitudeVC: UIViewController {
    
    let arrMore1 = ["None", "Middle of the night", "1/7 of the night", "Angle based method"]

    @IBOutlet weak var tableviewHA: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableviewHA.separatorStyle = .none
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }

}

extension SettingHighAltitudeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMore1.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! HighLatitudeCell
        
        //let model = self.arrMore1[indexPath.section]
        
        cell.lblHighAltitude.text = self.arrMore1[indexPath.section]
        
        let savedMethod = Int(getValueForKey(keyValue: "HighAltitude"))
        
        if savedMethod == indexPath.section {
            cell.imgHSelected.image = UIImage(named: "icon_prayercheck_green")
            cell.lblHighAltitude.textColor = kThemeDarkGreen
        }else{
            cell.imgHSelected.image = UIImage(named: "icon_prayercheck")
            cell.lblHighAltitude.textColor = UIColor.darkGray
        }
        
        //cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        print("Clicked item----\(arrMore1[indexPath.section])")
        
        setDefaultValue(keyValue: "HighAltitude", valueIs: String (indexPath.section))
        
        tableviewHA.reloadData()
        
    }
    
}
