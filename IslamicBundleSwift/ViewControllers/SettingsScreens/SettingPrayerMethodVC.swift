//
//  SettingPrayerMethodVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SettingPrayerMethodVC: UIViewController {
    
    let arrMore1 = ["Islamic University, Karachi", "Islamic Society of North America", "Muslim World League", "Makkah, Umm al-Qura", "Egyptian General Authority", "Tehran Institute of Geo Physics", "Jafari - Ithna Ashari"]

    @IBOutlet weak var tableviewPM: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableviewPM.separatorStyle = .none
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

    }

}

extension SettingPrayerMethodVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMore1.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! SettingPrayerMCell
        
        //let model = self.arrMore1[indexPath.section]
        
        cell.lblPrayerMethod.text = self.arrMore1[indexPath.section]
        
        let savedMethod = Int(getValueForKey(keyValue: "PrayerMethod"))
        
        if savedMethod == indexPath.section {
            cell.imgSelected.image = UIImage(named: "icon_prayercheck_green")
            cell.lblPrayerMethod.textColor = kThemeDarkGreen
        }else{
            cell.imgSelected.image = UIImage(named: "icon_prayercheck")
            cell.lblPrayerMethod.textColor = UIColor.darkGray
        }
        
        //cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        print("Clicked item----\(arrMore1[indexPath.section])")
        
        setDefaultValue(keyValue: "PrayerMethod", valueIs: String (indexPath.section))
        
        resetPrayerNotificationsOnSettingsChange()
        
        tableviewPM.reloadData()
        
    }
    
}
