//
//  HijriCalendarVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 20/08/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import FSCalendar_Persian

class HijriCalendarVC: UIViewController, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance {
    
    @IBOutlet weak var lblGregorianDate: UILabel!
    var currentDate: String = ""
    var eventsDateArray = [PrayerTrackModel]()
    
    
    
    @IBAction func btnPrevious(_ sender: Any) {
        print("----------------Previous--------------------------")
        let currentMonth = self.hijriCalendar.currentPage
        //print("currentMonth----->\(currentMonth)")
        let previousMonth = self.hijriCalendar.gregorian.date(byAdding: .month, value: -1, to: currentMonth)//Calendar.current.date(byAdding: .month, value: -1, to: currentMonth)!
        //print("previous-Month----> \(previousMonth)")
        self.hijriCalendar.setCurrentPage(previousMonth!, animated: true)
    }
    
    
    @IBAction func btnNext(_ sender: Any) {
        
        print("----------------Next--------------------------")
        let currentMonth = self.hijriCalendar.currentPage
        //print("currentMonth------> \(currentMonth)")
        let nextMonth = self.hijriCalendar.gregorian.date(byAdding: .month, value: 1, to: currentMonth)//Calendar.current.date(byAdding: .month, value: 1, to: currentMonth)!
        //print("Next-Month-----> \(nextMonth)")
        self.hijriCalendar.setCurrentPage(nextMonth!, animated: true)
    }
    
    @IBOutlet weak var hijriCalendar: FSCalendar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        hijriCalendar.delegate = self
        hijriCalendar.dataSource = self
        // hijriCalendar.locale = NSLocale.init(localeIdentifier: "fa-IR") as Locale
        //hijriCalendar.calendarHeaderView = NSCalendar.Identifier.islamicUmmAlQura
        
        //hijriCalendar.firstWeekday = 7
        
        hijriCalendar.locale = NSLocale.init(localeIdentifier: "en_US") as Locale
        hijriCalendar.calendarIdentifier = NSCalendar.Identifier.islamicUmmAlQura.rawValue
        hijriCalendar.placeholderType = .none
        hijriCalendar.appearance.headerMinimumDissolvedAlpha = 0
        hijriCalendar.clipsToBounds = true
        
        let hijriCorrection = getValueForKey(keyValue: "HijriSetting")
        print("hijriCorrection---> \(hijriCorrection)")
        
        switch hijriCorrection {
        case "0":
            print("0")
            /*let today = Date()
            let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: today)
            hijriCalendar.today = tomorrow
            
            hijriCalendar.firstWeekday = 0*/
        case "1":
            print("1")
            let today = Date()
            let tomorrow = Calendar.current.date(byAdding: .day, value: +1, to: today)
            hijriCalendar.today = tomorrow
            
            hijriCalendar.firstWeekday = 2
        case "2":
            print("2")
            let today = Date()
            let tomorrow = Calendar.current.date(byAdding: .day, value: +2, to: today)
            hijriCalendar.today = tomorrow
            
            hijriCalendar.firstWeekday = 3
        case "3":
            print("3-correct")
            let today = Date()
            let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: today)
            hijriCalendar.today = tomorrow
            
            hijriCalendar.firstWeekday = 0
        case "4":
            print("4-correct")
            let today = Date()
            let tomorrow = Calendar.current.date(byAdding: .day, value: -2, to: today)
            hijriCalendar.today = tomorrow
            
            hijriCalendar.firstWeekday = 6
        default:
            print("0")
            
        }
        
        /*let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: today)
        hijriCalendar.today = tomorrow
        
        hijriCalendar.firstWeekday = 0*/
        
        currentDate = Date().string(format: "MMM d yyyy")
        lblGregorianDate.text = currentDate
        
        hijriCalendar.reloadData()
        
    }
    
    /*func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
     
     let dateFormatter = DateFormatter()
     dateFormatter.dateFormat = "dd-MM-yyyy"
     // Gregorian to Hijri
     dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
     let GregorianDate = dateFormatter.date(from: "13-07-2016")
     let islamic = NSCalendar.init(calendarIdentifier: .islamicUmmAlQura)
     let components = (islamic?.components(NSCalendar.Unit(rawValue: UInt.max), from: GregorianDate!))!
     let date = NSCalendar.current.date(from: components)!
     calendar.setCurrentPage(date, animated: true)
     }*/
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        currentDate = date.string(format: "MMM d yyyy")
        //lblGregorianDate.text = currentDate
        
        //hijriCalendar.reloadData()
    }
    
    /*func calendar(_ calendar: FSCalendar,appearance: FSCalendarAppearance,titleDefaultColorFor date: Date) -> UIColor?
    {
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        let strDate = formatter.string(from: date)
        
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: today)
        
        let strDateCurrent = tomorrow!.string(format: "M/d/yyyy")
        
        print("strDate---1--> \(strDate)")
        print("strDateCurrent---1--> \(strDateCurrent)")
        if strDateCurrent == strDate {
            return UIColor.white
            // Return UIColor for eventsDateArray
        }
        
        return UIColorFromRGB(rgbValue: 0x462D60)
    }*/
    
    // Return UIColor for Background;
    
    /*func calendar(_ calendar: FSCalendar,appearance: FSCalendarAppearance,fillDefaultColorFor date: Date) -> UIColor?
    {
        let formatter = DateFormatter()
        formatter.dateFormat = ("M/d/yyyy")
        let strDate = formatter.string(from: date)
        
        let today = Date()
        let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: today)
        
        let strDateCurrent = tomorrow!.string(format: "M/d/yyyy")
        
        print("strDate---2--> \(strDate)")
        print("strDateCurrent---2--> \(strDateCurrent)")
        
        if strDateCurrent == strDate {
            return UIColorFromRGB(rgbValue: 0x462D60) // Return UIColor for eventsDateArray
        }
        
        return UIColor.white
    }*/
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
}
