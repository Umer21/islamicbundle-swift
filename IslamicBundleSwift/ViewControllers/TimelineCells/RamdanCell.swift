//
//  RamdanCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umar on 3/20/23.
//  Copyright © 2023 Umar Farooq. All rights reserved.
//

import UIKit

class RamdanCell: UITableViewCell {

    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDaysLeft: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
