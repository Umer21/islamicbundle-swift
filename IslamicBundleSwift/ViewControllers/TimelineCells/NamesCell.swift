//
//  NamesCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class NamesCell: UITableViewCell {

    
    @IBOutlet weak var btnViewMoreKidsName: UIButton!
    @IBOutlet weak var btnShareKidsName: UIButton!
    @IBOutlet weak var lblMeaning: UILabel!
    @IBOutlet weak var lblNameEnglish: UILabel!
    @IBOutlet weak var lblNameArabic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
