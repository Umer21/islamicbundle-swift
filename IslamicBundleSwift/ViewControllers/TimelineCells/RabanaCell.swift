//
//  RabanaCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class RabanaCell: UITableViewCell {

    @IBOutlet weak var btnViewMoreRabbana: UIButton!
    @IBOutlet weak var btnShareRabbana: UIButton!
    @IBOutlet weak var lblRabbanaTranslation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lblRabbanaArabic: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
