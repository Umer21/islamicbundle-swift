//
//  GreetingCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

protocol GreetingsDelegate {
    func btnTapped(tag: Int)
    
}

class GreetingCell: UITableViewCell {
    
    var delegate: GreetingsDelegate?
    
    private let cellReuseIdentifier = "GreetingsCollectionCell"

    @IBOutlet weak var btnShareGreetings: UIButton!
    
    var imgPosition = 0
    
    @IBAction func btnShareGreetings(_ sender: Any) {
       // let button = sender as! UIButton
        print("Image-pos----> \(imgPosition)")
        self.delegate?.btnTapped(tag: imgPosition)
        
    }
    @IBOutlet weak var pageControl: UIPageControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension GreetingCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 14
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! GreetingsCollectionCell
        
        if indexPath.row == 0 {
            cell.imgView.image = UIImage(named: "ahlan_wasahlan2")
        }else if indexPath.row == 1 {
            cell.imgView.image = UIImage(named: "ahlan_wasahlan")
        }else if indexPath.row == 2 {
            cell.imgView.image = UIImage(named: "subhanallah")
        }else if indexPath.row == 3 {
            cell.imgView.image = UIImage(named: "greeting_alhumdulillah")
        }else if indexPath.row == 4 {
            cell.imgView.image = UIImage(named: "allahu_akbar")
        }else if indexPath.row == 5 {
            cell.imgView.image = UIImage(named: "ameen")
        }else if indexPath.row == 6 {
            cell.imgView.image = UIImage(named: "bismillah")
        }else if indexPath.row == 7 {
            cell.imgView.image = UIImage(named: "inna_lillahi")
        }else if indexPath.row == 8 {
            cell.imgView.image = UIImage(named: "Inshallah")
        }else if indexPath.row == 9 {
            cell.imgView.image = UIImage(named: "greeting_aoa")
        }else if indexPath.row == 10 {
            cell.imgView.image = UIImage(named: "greeting_jazakallah")
        }else if indexPath.row == 11 {
            cell.imgView.image = UIImage(named: "greeting_eid")
        }else if indexPath.row == 12 {
            cell.imgView.image = UIImage(named: "greeting_qadar")
        }else if indexPath.row == 13 {
            cell.imgView.image = UIImage(named: "greeting_ramadan")
        }
        
        
        cell.backgroundColor = UIColor.green
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        imgPosition = pageControl.currentPage 
        print(pageControl.currentPage)
    }
    
    //Adjusting the space while scrolling from left and right
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameSize = collectionView.frame.size
        return CGSize(width: frameSize.width, height: frameSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
