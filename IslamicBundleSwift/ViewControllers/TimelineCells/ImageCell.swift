//
//  ImageCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import Adhan

class ImageCell: UITableViewCell {
    var releaseDate: NSDate?
    var countdownTimer = Timer()
    
    @IBOutlet weak var btnFullClick: UIButton!
    @IBOutlet weak var btnImgAlarm: UIButton!
    @IBOutlet weak var lblCountDownTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblIslamicDate: UILabel!
    @IBOutlet weak var lblNamaz: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    //@IBOutlet weak var imgAlarm: UIImageView!
    
    @IBOutlet weak var imgBg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        startTimer()
    }
    
    func startTimer() {
        
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        
       let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        var date = cal.dateComponents([.year, .month, .day], from: Date())
        
        var lat: Double?
        var lng: Double?
        
        if isKeyPresentInUserDefaults(key: "Latitude"){
            //print("Exist")
            lat = Double (getValueForKey(keyValue: "Latitude"))
            lng = Double (getValueForKey(keyValue: "Longitude"))
        }
        else{
            //print("Not Exist")
            lat = 21.3891
            lng = 39.8579
        }
        
        //-------------------------------
        let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
        //print("PrayerMethod---> \(prayerMethod)")
        let jurists = getValueForKey(keyValue: "Jurists")
        //print("Jurists---> \(jurists)")
        
        let coordinates = Coordinates(latitude: lat!, longitude: lng!)
        var params = CalculationMethod.karachi.params
        
        switch prayerMethod {
        case "0":
            params = CalculationMethod.karachi.params
            break
        case "1":
            params = CalculationMethod.northAmerica.params
            break
        case "2":
            params = CalculationMethod.muslimWorldLeague.params
            break
        case "3":
            params = CalculationMethod.ummAlQura.params
            break
        case "4":
            params = CalculationMethod.egyptian.params
            break
        case "5":
            params = CalculationMethod.tehran.params
            break
        case "6":
            params = CalculationMethod.other.params
            break
            
        default:
            params = CalculationMethod.karachi.params
            break
        }
        
        
        switch jurists {
        case "0":
            params.madhab = .shafi
            break
        case "1":
            params.madhab = .hanafi
            break
        
        default:
            params.madhab = .shafi
            break
        }
        //-------------------------------
        
        var prayerTimes = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params)
        
        let current = prayerTimes?.currentPrayer()
        //print("AAA-current \(current!)")
        
        var countdown = Date()
        if current == Prayer.isha {
            let today = Date()
            let modifiedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
            date = cal.dateComponents([.year, .month, .day], from: modifiedDate)
            //print("AAA-date-after \(date)")
            prayerTimes = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params)
            
            let next = prayerTimes!.nextPrayer()
            countdown = prayerTimes!.time(for: next!)
        }
        else {
            let next = prayerTimes!.nextPrayer()
            countdown = prayerTimes!.time(for: next!)
        }

       /* let next = prayerTimes!.nextPrayer()
        let countdown = prayerTimes!.time(for: next!)*/
        
        
        let currentDate = Date()
        let calendar = Calendar.current
        
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: countdown as Date)
        
        let countdown1 = "\(String(format:"%02d", diffDateComponents.hour ?? 0)) : \(String(format:"%02d", diffDateComponents.minute ?? 0)) : \(String(format:"%02d", diffDateComponents.second ?? 0))"
        
        let dateString = countdown.string(format: "yyyy-MM-dd HH:mm:ssZ")
        let dateFormatForNamaz = getFormattedDate(strDate: dateString, currentFomat: "yyyy-MM-dd HH:mm:ssZ", expectedFromat: "hh:mm a")
        
        lblCountDownTime.text = countdown1
        
        if let nm = prayerTimes!.nextPrayer()
        {
            //print("Check Namaz name---????\(nm)")
            if (nm == Prayer.fajr)
            {
               // lblNamaz.text = "Fajr"
            }
            else if(nm == Prayer.sunrise)
            {
               // lblNamaz.text = "Sunrise"
            }
            else if(nm == Prayer.dhuhr)
            {
                //lblNamaz.text = "Dhuhr"
            }
            else if(nm == Prayer.asr)
            {
                //lblNamaz.text = "Asr"
            }
            else if(nm == Prayer.maghrib)
            {
               // lblNamaz.text = "Maghrib"
            }
            else if(nm == Prayer.isha)
            {
                //lblNamaz.text = "Isha"
            }
            
        }
        
        //lblTime.text = dateFormatForNamaz
        
       // print(next!)
        
        /*if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
            switch prayers.nextPrayer() {
            case Prayer.fajr:
                print("Fajr")
                lblNamaz.text = "Fajr"
                //setDefaultValue("Fajr", forKey: "CurrentNamaz")
                setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Fajr")
                setDefaultValue(keyValue: "CurrentNamazTime", valueIs: dateFormatForNamaz)
                break
            case Prayer.sunrise:
                print("Sunrise")
                lblNamaz.text = "Sunrise"
                setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Sunrise")
                setDefaultValue(keyValue: "CurrentNamazTime", valueIs: dateFormatForNamaz)
                break
            case Prayer.dhuhr:
                print("Dhuhr")
                lblNamaz.text = "Dhuhr"
                setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Dhuhr")
                setDefaultValue(keyValue: "CurrentNamazTime", valueIs: dateFormatForNamaz)
                break
            case Prayer.asr:
                print("Asr")
                lblNamaz.text = "Asr"
                setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Asr")
                setDefaultValue(keyValue: "CurrentNamazTime", valueIs: dateFormatForNamaz)
                break
            case Prayer.maghrib:
                print("Maghrib")
                lblNamaz.text = "Maghrib"
                setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Maghrib")
                setDefaultValue(keyValue: "CurrentNamazTime", valueIs: dateFormatForNamaz)
                break
            case Prayer.isha:
                print("Isha")
                lblNamaz.text = "Isha"
                setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Isha")
                setDefaultValue(keyValue: "CurrentNamazTime", valueIs: dateFormatForNamaz)
                break
            default:
                print("no Namaz")
            }
            
            //print(tt[0].NextNamazName!)
            
            //lblNamaz.text = next
            lblTime.text = dateFormatForNamaz
        }*/
        
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
