//
//  DuaCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class DuaCell: UITableViewCell {

    @IBOutlet weak var btnViewMoreDua: UIButton!
    @IBOutlet weak var btnShareDua: UIButton!
    @IBOutlet weak var lblDuaTranslation: UILabel!
    @IBOutlet weak var lblDuaArabic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
