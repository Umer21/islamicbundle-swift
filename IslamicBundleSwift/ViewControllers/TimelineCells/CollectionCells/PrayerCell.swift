//
//  PrayerCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 28/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

protocol PrayerDelegate {
    func alarmTapped(tag: Int)
    
}

class PrayerCell: UITableViewCell {

    var delegate: PrayerDelegate?
    
    @IBOutlet weak var btnNamazAlarm: UIButton!
    @IBOutlet weak var lblPrayerName: UILabel!
    @IBOutlet weak var lblPrayerTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /*@IBAction func btnNamazAlarm(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.alarmTapped(tag: button.tag)
    }*/
    
    /*@IBAction func btnAlarm(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.alarmTapped(tag: button.tag)
        print("Clicked----")
    }*/
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }

}
