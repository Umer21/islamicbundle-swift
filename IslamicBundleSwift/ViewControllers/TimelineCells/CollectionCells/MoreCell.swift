//
//  MoreCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 04/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {

    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var imgMore: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
