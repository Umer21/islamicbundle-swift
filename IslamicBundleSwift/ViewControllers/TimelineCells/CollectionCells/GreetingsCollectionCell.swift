//
//  GreetingsCollectionCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class GreetingsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
}
