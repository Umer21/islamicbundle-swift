//
//  VerseCell.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class VerseCell: UITableViewCell {

    @IBOutlet weak var btnViewMoreQuran: UIButton!
    @IBOutlet weak var btnShareQuran: UIButton!
    @IBOutlet weak var lblSurahTranslation: UILabel!
    @IBOutlet weak var lblSurahArabic: UILabel!
    @IBOutlet weak var lblSurahInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
