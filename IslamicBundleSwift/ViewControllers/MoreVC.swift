//
//  MoreVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class MoreVC: UIViewController {

    @IBOutlet weak var moreTableView: UITableView!
    let cellSpacingHeight: CGFloat = 6
    let arrMore1 = ["The Quran", "Calendar", "Fasting Timings", "Zakat Calculator", "Prayer Tracker", "Kids Name", "99 Names of Allah", "Duas", "Tasbeeh"]
    
    let arrMoreImage1 = ["icon_more_quran","icon_more_calender","icon_more_fasting","icon_more_zakat","icon_tracker","icon_kidname","icon_99names","icon_dailydua","icon_tasbeeh"]
    
    let arrMore2 = ["Related Apps","About Us","Contact Us","Recommend to others","Rate"]
    
    let arrMoreImage2 = ["icon_more_relatedapps","icon_more_aboutt","icon_more_contact","icon_more_share","icon_more_ratee"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "More"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let settingButton = UIBarButtonItem(image: UIImage(named: "icon_nav_setting"), style: .plain, target: self, action: #selector(settingsTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [settingButton]
        
        //UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    @objc func settingsTapped() {
        gotoSettingsScreen()
    }
    
    func gotoSettingsScreen() {
        let sb = UIStoryboard.init(name: kSettingsStroyboard, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: kSettingsConttollerID) as! SettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    func gotoAppStore(appID : String, appName: String){
        print("https://itunes.apple.com/in/app/\(appName)/\(appID)?mt=8")
        if let url = URL(string: "https://itunes.apple.com/in/app/\(appName)/\(appID)?mt=8")
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            else {
                if UIApplication.shared.canOpenURL(url as URL) {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
    }
   

}

extension MoreVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return arrMore1.count
        }
        else if section == 1
        {
            return arrMore2.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))

            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
            label.text = "Features"
           // label.font = UIFont().futuraPTMediumFont(16) // my custom font
            label.textColor = UIColor.white
            label.font = UIFont.boldSystemFont(ofSize: 20.0)// my custom colour
            
            headerView.backgroundColor = UIColorFromRGB(rgbValue: 0x4B415E)
            headerView.addSubview(label)

            return headerView
        }
        else if section == 1 {
            
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))

             let label = UILabel()
             label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
             label.text = "Others"
            // label.font = UIFont().futuraPTMediumFont(16) // my custom font
             label.textColor = UIColor.white
             label.font = UIFont.boldSystemFont(ofSize: 20.0)// my custom colour
             
             headerView.backgroundColor = UIColorFromRGB(rgbValue: 0x4B415E)
             headerView.addSubview(label)

             return headerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! MoreCell
        
        switch indexPath.section {
        case 0:
            cell.lblMore.text = arrMore1[indexPath.row]
            cell.imgMore.image = UIImage(named: arrMoreImage1[indexPath.row])
            return cell
        case 1:
            cell.lblMore.text = arrMore2[indexPath.row]
            cell.imgMore.image = UIImage(named: arrMoreImage2[indexPath.row])
            
            return cell
            
        default:
            break
        }
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.row).")
        
        switch indexPath.section {
        case 0:
            
            switch indexPath.row{
            case 0:
                gotoSurahListScreen()
                
            case 1:
                //calendar
               // gotoSurahListScreen()
                print("Calendar")
                gotoHijriCalendarScreen()
                
            case 2:
                //Fasting Timings
               // gotoSurahListScreen()
                 print("Fasting")
               // gotoJuristicScreen()
                gotoFastingTimeScreen()
                
            case 3:
                //Zakat Calculator
               // gotoSurahListScreen()
                 print("Zakat Calendar")
                gotoZakatCalculatorScreen()
                
            case 4:
                //Prayer Tracker
               // gotoSurahListScreen()
                 print("Prayer Tracker")
                gotoPrayerTrackerScreen()
                
            case 5:
                //Kids Name
               // gotoSurahListScreen()
                 print("Kids Name")
                gotoKidsNameListScreen()
                
            case 6:
                //99Names of Allah
               // gotoSurahListScreen()
                 print("Names of Allah")
                gotoNameListScreen()
                
            case 7:
                //Duas
                //gotoSurahListScreen()
                gotoDuaCategoryScreen()
                 print("Duas")
                
            case 8:
                //Tasbeeh
                gotoTasbeehScreen()
                 print("Tasbeeh")
                
            default:
                break
                
            }
            
        case 1:
            
            switch indexPath.row{
            case 0:
                //Related Apps
                print("Related Apps")
                gotoRelatedAppScreen()
                
            case 1:
                //About Us
                print("About Us")
                gotoAboutScreen()
                
            case 2:
                //Contact Us
                print("Contact Us")
                gotoContactScreen()
                
            case 3:
                //Recommended to others
                print("Recommended to others")
               let shareText = "Islamic Bundle (Powered by Inabia) \n\nThe popular Muslim Community App. Islamic Bundle is all in one Islamic applications and consists of different features. \n\nDownload app now \nIslamic Bundle: \nhttps://itunes.apple.com/us/app/myapp/idxxxxxxxx?ls=1&mt=8"
                
                let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
                present(vc, animated: true)
                
            case 4:
                //Rate
                print("Rate")
                gotoAppStore(appID: "id1139730876", appName: "islamic-bundle")
                
                
            default:
                break
                
            }
            
        default:
            break
        }
        
        
    }
    
    func gotoFastingTimeScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kFastingTimingsControllerID) as? FastingTimingsVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoTasbeehScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kTasbeehControllerID) as? TasbeehVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoHijriCalendarScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kHijriCalendarControllerID) as? HijriCalendarVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoPrayerTrackerScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kPrayerTrackerControllerID) as? PrayerTrackerVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoDuaCategoryScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kDailyDuaListViewControllerID) as? DailyDuaListVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoContactScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kContactUsControllerID) as? ContactUsVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoAboutScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kAboutUsControllerID) as? AboutUsVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoRelatedAppScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRelatedAppsControllerID) as? RelatedAppsVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoNameListScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kName99ListViewControllerID) as? Allah99NamesVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoKidsNameListScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kKidsListViewControllerID) as? KidsNamesVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoZakatCalculatorScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kZakatViewControllerID) as? ZakatCalculatorVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoSurahListScreen() {
        let vc = UIStoryboard.init(name: kQuranSurahListStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kQuranSurahListControllerID) as? QuranSurahListVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func gotoPrayerMethodScreen() {
        let vc = UIStoryboard.init(name: kOnBoardStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kPrayerMethodControllerID) as? PrayerMethodVC
        
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func gotoJuristicScreen() {
        let vc = UIStoryboard.init(name: kOnBoardStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kJuristictsControllerID) as? JuristsVC
        
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func gotoLocationSelectionScreen() {
        let vc = UIStoryboard.init(name: kOnBoardStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kLocationSelectionControllerID) as? LocationSelectionVC
        
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}
