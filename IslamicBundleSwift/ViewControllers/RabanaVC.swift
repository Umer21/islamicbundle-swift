//
//  RabanaVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class RabanaVC: UIViewController {
    
    var arrRabbana = [Rabbana]()
    let cellSpacingHeight: CGFloat = 6
    var pos: Int?

    @IBOutlet weak var rabbanaTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        arrRabbana = DatabaseHelper.getRabbanaList()
       // rabbanaTableView.separatorStyle = .none
        rabbanaTableView.separatorStyle = .none
        rabbanaTableView.backgroundColor = .none
        rabbanaTableView.separatorColor = UIColor.clear
    }
    

   func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "40 Rabbana"
        let bookmarkButton = UIBarButtonItem(image: UIImage(named: "bookmark"), style: .plain, target: self, action: #selector(bookmarkTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [bookmarkButton]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    @objc func bookmarkTapped() {
           gotoBookmarkRabbanaistScreen()
       }
    
    func gotoBookmarkRabbanaistScreen() {
        let vc = UIStoryboard.init(name: kRabbanaStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRabbanaBookmarkListControllerID) as? RabbanaBookmarkListVC
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = false
        
    }

}

extension RabanaVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRabbana.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! RabbanaListCell
        
        let model = self.arrRabbana[indexPath.section]
        cell.lblRabbanaArabic.text = model.RabbanaArabic
        cell.lblRabbanaTranslation.text = model.RabbanaTranslation
        
        cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        pos = indexPath.section
        //print("position-sent--> \(pos)")
        
        gotoRabbanaDetailScreen()
        
    }
    
    func gotoRabbanaDetailScreen() {
        let vc = UIStoryboard.init(name: kRabbanaStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRabbanaDetailControllerID) as? RabbanaDetailVC
        vc?.arrRabbanaList = arrRabbana
        vc?.clickedPos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}

//extension UIView {
//
//    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//         let mask = CAShapeLayer()
//         mask.path = path.cgPath
//         self.layer.mask = mask
//    }
//}
