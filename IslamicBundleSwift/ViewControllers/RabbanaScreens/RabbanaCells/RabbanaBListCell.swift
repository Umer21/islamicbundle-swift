//
//  RabbanaListCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 23/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

protocol RabbanaBookmarkListDelegate {
    func RbookmarkTappedd(tag: Int)
    
}

class RabbanaBListCell: UITableViewCell {

    var delegate: RabbanaBookmarkListDelegate?
    
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var lblTranslation: UILabel!
    @IBOutlet weak var lblArabic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnBookmarkTapped(_ sender: Any) {
        let button = sender as! UIButton
        self.delegate?.RbookmarkTappedd(tag: button.tag)
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
