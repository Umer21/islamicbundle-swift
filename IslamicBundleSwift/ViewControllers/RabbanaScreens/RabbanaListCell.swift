//
//  RabbanaListCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 03/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class RabbanaListCell: UITableViewCell {

    @IBOutlet weak var lblRabbanaTranslation: UILabel!
    @IBOutlet weak var lblRabbanaArabic: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
