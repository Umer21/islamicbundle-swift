//
//  RabbanaBookmarkDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 23/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import AVFoundation

class RabbanaBookmarkDetailVC: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var rabbanaBDTableView: UITableView!
    
    var arrRabbanaBookmarks = [Rabbana]()
    var arrRabbanaList = [Rabbana]()
    
    let cellSpacingHeight: CGFloat = 6
    var pos: Int?
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var SliderIB: UISlider!
    
    var audioPlayer = AVAudioPlayer()
    var isPlaying = false
    var timer:Timer!
    @IBOutlet weak var btn_playIB: UIButton!
    var fileUrl = "https://inabia.com/Apps/rabbanaaudio/rabbanaaudio/" //"http://codinghomes.com/rabbanaaudio/rabbanaaudio/"
    var audio_url = ""
    var fileName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        getBookmarkDuaDetail()
        
        multLineLabeltitle()
        
        rabbanaBDTableView.reloadData()
        rabbanaBDTableView.separatorStyle = .none
        rabbanaBDTableView.backgroundColor = .none
        rabbanaBDTableView.separatorColor = UIColor.clear
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(sleft))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.rabbanaBDTableView.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(sright))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.rabbanaBDTableView.addGestureRecognizer(swipeLeft)
        
        swipeLeft.delegate = self
        swipeRight.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    @objc func sleft(){
        print("Swiped left")
        
        if (pos! > 0)
        {
            pos! -= 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            rabbanaBDTableView.reloadData()
            viewSlideInFromLeftToRight(views: rabbanaBDTableView)
            
            multLineLabeltitle()
        }
    }
    
    @objc func sright(){
        print("Swiped right")
        
        if (pos! < arrRabbanaBookmarks.count - 1)
        {
            pos! += 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            rabbanaBDTableView.reloadData()
            viewSlideInFromRightToLeft(views: rabbanaBDTableView)
            
            multLineLabeltitle()
        }
    }
    
    @IBAction func btnNext(_ sender: Any) {
        if (pos! < arrRabbanaBookmarks.count - 1)
        {
            pos! += 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            rabbanaBDTableView.reloadData()
            viewSlideInFromRightToLeft(views: rabbanaBDTableView)
            
            multLineLabeltitle()
        }
    }
    
    
    @IBAction func btnPrevious(_ sender: Any) {
        if (pos! > 0)
        {
            pos! -= 1
            //arrDuaDetail = DatabaseHelper.getDetailDuaCategoryWise(pos: Int64(pos!))
            rabbanaBDTableView.reloadData()
            viewSlideInFromLeftToRight(views: rabbanaBDTableView)
            
            multLineLabeltitle()
        }
    }
    
    @IBAction func btnPlayB(_ sender: Any) {
        if (!checkFileExist(fileName: fileName)){
            let url = URL(string: audio_url)
            downloadUsingAlamofire(url: url!, urlStr: audio_url)
        }
        else{
            print("File already exist")
            playAudio()
        }
    }
    
    
    @IBAction func changeAudio(_ sender: Any) {
        
    }
    
    func playAudio()
    {
        if isPlaying {
            audioPlayer.pause()
            isPlaying = false
            
            btn_playIB.setImage(UIImage(named: "play_buttonr"), for: UIControl.State.normal)
        } else {
            audioPlayer.play()
            isPlaying = true
            btn_playIB.setImage(UIImage(named: "pause_button"), for: UIControl.State.normal)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        let currentTime = Int(audioPlayer.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
            
        lblTime.text = String(format: "%02d:%02d", minutes,seconds) as String
        
        UIView.animate(withDuration: 2, animations: {
          self.SliderIB.setValue(Float(self.audioPlayer.currentTime), animated: true)
        })
        print(Float (audioPlayer.currentTime))
        print(TimeInterval (audioPlayer.currentTime))
    }
    
    
    func getBookmarkDuaDetail(){
        arrRabbanaBookmarks = DatabaseHelper.GetAllRabbanaBookmark()
        //arrDuaDetail = DatabaseHelper.getAllDetailDuaCategoryWise()
        
        for index in arrRabbanaBookmarks.indices {
            print("\(index)")
            arrRabbanaList.append(DatabaseHelper.getRabbanaDetailForBookmark(id: arrRabbanaBookmarks[index].RId!))
        }
    }
    
    func viewSlideInFromRightToLeft(views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition()
        transition!.duration = 0.5
        transition!.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition!.type = CATransitionType.push
        transition!.subtype = CATransitionSubtype.fromRight
        transition!.delegate = self as? CAAnimationDelegate
        views.layer.add(transition!, forKey: nil)
    }
    
    func viewSlideInFromLeftToRight(views: UIView) {
        var transition: CATransition? = nil
        transition = CATransition()
        transition!.duration = 0.5
        transition!.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition!.type = CATransitionType.push
        transition!.subtype = CATransitionSubtype.fromLeft
        transition!.delegate = self as? CAAnimationDelegate
        views.layer.add(transition!, forKey: nil)
    }
    
    func multLineLabeltitle(){
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = NSTextAlignment.center
        label.text = "\(pos! + 1 )/\(arrRabbanaBookmarks.count)"
        self.navigationItem.titleView = label
    }
    
    func setNavBar()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    // MARK: - Load URL
       func downloadUsingAlamofire(url: URL, urlStr: String) {
           
           //audioUrl should be of type URL
           let audioFileName = String((url.lastPathComponent)) as NSString
           //print("audioFileName----\(audioFileName)")

           //path extension will consist of the type of file it is, m4a or mp4
           let pathExtension = audioFileName.pathExtension
           print("pathExtension----\(pathExtension)")
           
           /*let window = UIApplication.shared.windows.first
           let hud = JGProgressHUD(style: .dark)
           if UIDevice.current.userInterfaceIdiom == .pad {
               hud.transform = hud.transform.scaledBy(x: 2, y: 2)
           }
           
           hud.show(in: window!)*/
           
           let hud = JGProgressHUD(style: .dark)
           hud.vibrancyEnabled = true
           if arc4random_uniform(2) == 0 {
               hud.indicatorView = JGProgressHUDPieIndicatorView()
           }
           else {
               hud.indicatorView = JGProgressHUDRingIndicatorView()
           }
           hud.detailTextLabel.text = "0% Complete"
           hud.textLabel.text = "Downloading"
           hud.show(in: self.view)
          
           let destination: DownloadRequest.Destination = { _, _ in
               var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

               // the name of the file here I kept is yourFileName with appended extension
               //documentsURL.appendPathComponent("yourFileName."+pathExtension)
               documentsURL.appendPathComponent(audioFileName as String)
               return (documentsURL, [.removePreviousFile])
           }
           
           AF.download(urlStr, to: destination)
               .downloadProgress { progress in
                   print("Download Progress: \(progress.fractionCompleted * 100)")
                   DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
                       self.incrementHUD(hud, progresss: (Int)(progress.fractionCompleted * 100))
                   }
           }
           .response { response in
               debugPrint(response)
               
               //hud.dismiss(afterDelay: 0.0)
               
               if response.fileURL != nil {
                   print("Response------>  \(response.fileURL!)")
               }
           }
       }
       
       func incrementHUD(_ hud: JGProgressHUD, progresss : Int) {
           let progress1 = progresss//previousProgress + 1
           hud.progress = Float(progress1)/100.0
           hud.detailTextLabel.text = "\(progress1)% Complete"
           
           if progress1 == 100 {
               DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                   UIView.animate(withDuration: 0.1, animations: {
                       hud.textLabel.text = "Success"
                       hud.detailTextLabel.text = nil
                       hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                   })
                   
                   hud.dismiss(afterDelay: 1.0)
                
                self.rabbanaBDTableView.reloadData()
                   
               }
           }
       }
    
    func initAVPlayer() {
        if (checkFileExist(fileName: fileName)){
            let fileManager = FileManager.default
            let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let finalDatabaseURL = documentsUrl.first!.appendingPathComponent(fileName)
            
            do {
                audioPlayer =  try AVAudioPlayer(contentsOf: finalDatabaseURL)
            } catch {
                // can't load file
                print("Check-Error-playr-> \(error)")
            }
            
            SliderIB.maximumValue = Float(audioPlayer.duration)
            audioPlayer.delegate = self
            
        }
    }

}

extension RabbanaBookmarkDetailVC: UIGestureRecognizerDelegate{
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }
}

extension RabbanaBookmarkDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        switch indexPath.section {
        case 0:
            let cell = rabbanaBDTableView.dequeueReusableCell(withIdentifier: "cell1", for:  indexPath as IndexPath) as! RabbanaBDetailCell1
            
            cell.lblRabbanaBArabic.text = arrRabbanaList[pos!].RabbanaArabic
            cell.lblRabbanaBTransliteration.text = arrRabbanaList[pos!].RabbanaTransliteration
            
            //print(arrRabbanaList[pos!].RId!)
            
            fileName = ("audio\(arrRabbanaList[pos!].RId ?? "00").mp3")
            audio_url = fileUrl + fileName
            //print("Next-Previous- \(audio_url)")
            
            if (checkFileExist(fileName: fileName)) {
               // print("-------------->: Next-pause")
                audioPlayer.pause()
            }
            isPlaying = false
            
            btn_playIB.setImage(UIImage(named: "play_buttonr"), for: UIControl.State.normal)
            
            self.initAVPlayer()
            
            return cell
            
        case 1:
            
            let cell = rabbanaBDTableView.dequeueReusableCell(withIdentifier: "cell2", for:  indexPath as IndexPath) as! RabbanaBDetailCell2
            
            cell.lblRabbanaBTranslation.text = arrRabbanaList[pos!].RabbanaTranslation
            
            return cell
            
        default:
            break
        }
        
        return cell1!
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        //print("You tapped cell number \(indexPath.section).")
        
    }
    
    
}
