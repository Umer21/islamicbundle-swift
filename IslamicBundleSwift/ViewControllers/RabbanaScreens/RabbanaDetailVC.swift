//
//  RabbanaDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 03/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import AVFoundation

class RabbanaDetailVC: UIViewController, AVAudioPlayerDelegate {
    
    var arrRabbanaList = [Rabbana]()
    var clickedPos: Int = 0
    var label = UILabel()
    var fileUrl = "https://inabia.com/Apps/rabbanaaudio/rabbanaaudio/" //http://codinghomes.com/rabbanaaudio/rabbanaaudio/
    var audio_url = ""
    var fileName = ""
    @IBOutlet weak var Slider: UISlider!
    var audioPlayer = AVAudioPlayer()
    var isPlaying = false
    @IBOutlet weak var btn_playIB: UIButton!
    
    @IBOutlet weak var lblTime: UILabel!
    var timer:Timer!
    
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblRabbanaTranslation: UILabel!
    @IBOutlet weak var lblRabbanaTransliteration: UILabel!
    @IBOutlet weak var lblRabbanaArabic: UILabel!
    
    @IBAction func btn_next(_ sender: Any) {
        if clickedPos < 40 {
            clickedPos += 1
            
            fileName = ("audio\(clickedPos).mp3")
            audio_url = fileUrl + fileName
           // print("Next-- \(audio_url)")
           // print("Previous-fileName- \(fileName)")
            
            if (checkFileExist(fileName: fileName)) {
                
               // print("-------------->: Next-pause")
                audioPlayer.pause()
                
            }
            
            isPlaying = false
            
            btn_playIB.setImage(UIImage(named: "play_buttonr"), for: UIControl.State.normal)
            
            addLabel()
            
            showData()
            /*if clickedPos <= 39
            {
                showData()
            }*/
            
        }
        
        setNavBar()
        
    }
    
    @IBAction func btn_previous(_ sender: Any) {
        
         if clickedPos > 0 {
             
            clickedPos -= 1
            
            fileName = ("audio\(clickedPos).mp3")
            audio_url = fileUrl + fileName
          //  print("Previous-- \(audio_url)")
          //  print("Previous-fileName- \(fileName)")
          //  print("Previous-Check- \(checkFileExist(fileName: fileName))")
            
            if (checkFileExist(fileName: fileName)) {
              //  print("Previous-------------->: Previous-pause")
                audioPlayer.pause()
                
            }
            
            
            isPlaying = false
            
            btn_playIB.setImage(UIImage(named: "play_buttonr"), for: UIControl.State.normal)
            
            addLabel()
            
            showData()
         }
        
        setNavBar()
    }
    
    @IBAction func btn_play(_ sender: Any) {
        
        //let urlString = "https://www.inabia.com/folder/download.mp3"
        
        if (!checkFileExist(fileName: fileName)){
            let url = URL(string: audio_url)
            downloadUsingAlamofire(url: url!, urlStr: audio_url)
        }
        else{
            print("File already exist")
            playAudio()
        }
        
        
        /*if let imageURL = getURLFromString(url) {
            download(from: imageURL)
        }*/
    }
    
    @IBAction func changeAudioTime(_ sender: Any) {
        audioPlayer.stop()
        audioPlayer.currentTime = TimeInterval(Slider.value)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    
    func playAudio()
    {
        if isPlaying {
            audioPlayer.pause()
            isPlaying = false
            
            btn_playIB.setImage(UIImage(named: "play_buttonr"), for: UIControl.State.normal)
        } else {
            audioPlayer.play()
            isPlaying = true
            btn_playIB.setImage(UIImage(named: "pause_button"), for: UIControl.State.normal)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        let currentTime = Int(audioPlayer.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
            
        lblTime.text = String(format: "%02d:%02d", minutes,seconds) as String
                
        /*UIView.animate(withDuration: 1.5, animations:  {() in
            //self.Slider.setValue(0.75, animated: true)
            self.Slider.setValue(Float(self.audioPlayer.currentTime), animated: true)
            }, completion:{(Bool)  in
                UIView.animate(withDuration: 1.5, animations: {() in
                    self.Slider.setValue(Float(self.audioPlayer.currentTime), animated: true)
                })
        })*/
        
        UIView.animate(withDuration: 2, animations: {
          self.Slider.setValue(Float(self.audioPlayer.currentTime), animated: true)
        })
        print(Float (audioPlayer.currentTime))
        print(TimeInterval (audioPlayer.currentTime))
        
        /*UIView.animate(withDuration: 0.2, animations: {
            self.Slider.setValue(Float(self.audioPlayer.currentTime), animated: true)
        })*/
    }
    
    
    // MARK: - Load URL
    func downloadUsingAlamofire(url: URL, urlStr: String) {
        
       // print("---->1---R-\(url)")
       // print("---->2---R-\(urlStr)")
        //audioUrl should be of type URL
        let audioFileName = String((url.lastPathComponent)) as NSString
        //print("audioFileName----\(audioFileName)")

        //path extension will consist of the type of file it is, m4a or mp4
        let pathExtension = audioFileName.pathExtension
       // print("pathExtension----\(pathExtension)")
        
        /*let window = UIApplication.shared.windows.first
        let hud = JGProgressHUD(style: .dark)
        if UIDevice.current.userInterfaceIdiom == .pad {
            hud.transform = hud.transform.scaledBy(x: 2, y: 2)
        }
        
        hud.show(in: window!)*/
        
        let hud = JGProgressHUD(style: .dark)
        hud.vibrancyEnabled = true
        if arc4random_uniform(2) == 0 {
            hud.indicatorView = JGProgressHUDPieIndicatorView()
        }
        else {
            hud.indicatorView = JGProgressHUDRingIndicatorView()
        }
        hud.detailTextLabel.text = "0% Complete"
        hud.textLabel.text = "Downloading"
        hud.show(in: self.view)
       
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

            // the name of the file here I kept is yourFileName with appended extension
            //documentsURL.appendPathComponent("yourFileName."+pathExtension)
            documentsURL.appendPathComponent(audioFileName as String)
            return (documentsURL, [.removePreviousFile])
        }
        
        AF.download(urlStr, to: destination)
            .downloadProgress { progress in
               // print("Download Progress: \(progress.fractionCompleted * 100)")
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
                    self.incrementHUD(hud, progresss: (Int)(progress.fractionCompleted * 100))
                }
        }
        .response { response in
            debugPrint(response)
            
            //hud.dismiss(afterDelay: 0.0)
            
            if response.fileURL != nil {
               // print("Response------>  \(response.fileURL!)")
            }
        }
    }
    
    func incrementHUD(_ hud: JGProgressHUD, progresss : Int) {
        let progress1 = progresss//previousProgress + 1
        hud.progress = Float(progress1)/100.0
        hud.detailTextLabel.text = "\(progress1)% Complete"
        
        if progress1 == 100 {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                UIView.animate(withDuration: 0.1, animations: {
                    hud.textLabel.text = "Success"
                    hud.detailTextLabel.text = nil
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                })
                
                hud.dismiss(afterDelay: 1.0)
                
                self.initAVPlayer()
                
            }
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        setNavBar()
        showData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func showData()
    {
        //print("Clicked-ID--> \(arrRabbanaList[clickedPos].RId)")
        //var tt = (Int) (arrRabbanaList[clickedPos].RId!)
        //print("Clicked-ID-2-> \(tt)")
        //print(arrRabbanaList[tt!].RabbanaArabic)
        
        
        lblRabbanaArabic.text = arrRabbanaList[clickedPos].RabbanaArabic
        lblRabbanaTransliteration.text = arrRabbanaList[clickedPos].RabbanaTransliteration
        lblRabbanaTranslation.text = arrRabbanaList[clickedPos].RabbanaTranslation
        
        contentViewHeightConstraint.constant = lblRabbanaArabic.requiredHeight + lblRabbanaTranslation.requiredHeight + lblRabbanaTransliteration.requiredHeight + 100
        
        fileName = ("audio\(clickedPos + 1).mp3")
        audio_url = fileUrl + fileName
        //print("\(audio_url)")
        
        //----------------------
        //let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        //let soundFileURL = documentsDirectory.URLByAppendingPathComponent(fileName)
        
        //--------------
        //let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        //let fileURL = documentURL.appendingPathComponent(fileName)
        
        //---------------------
        initAVPlayer()
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
       // print("Audio finished called")
        
        self.audioPlayer.stop()
        isPlaying = false
        btn_playIB.setImage(UIImage(named: "play_buttonr"), for: UIControl.State.normal)
    }
    
    func initAVPlayer() {
       // print("--------- \(fileName)")
        if (checkFileExist(fileName: fileName)) {
          //  print("Rabbana-Exist---")
            let fileManager = FileManager.default
            let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            let finalDatabaseURL = documentsUrl.first!.appendingPathComponent(fileName)
            
            do {
                audioPlayer =  try AVAudioPlayer(contentsOf: finalDatabaseURL)
            } catch {
                // can't load file
                print("Check-Error-playr-> \(error)")
            }
            
            Slider.maximumValue = Float(audioPlayer.duration)
            audioPlayer.delegate = self
            
        }
    }
    
    func setNavBar() {
        
        /*let backButton = UIBarButtonItem(image: UIImage(named: "icon_dashboard"), style: .plain, target: self, action: #selector(backButtonTapped))
        self.navigationItem.leftBarButtonItems = [backButton]*/
        
       
        let shareButton = UIBarButtonItem(image: UIImage(named: "icon_nav_share"), style: .plain, target: self, action: #selector(shareButtonTapped))
        
        var bookmarkButton: UIBarButtonItem?
        
        if (DatabaseHelper.CheckRabbanaExist(r_id: arrRabbanaList[clickedPos].RId!)){
            bookmarkButton = UIBarButtonItem(image: UIImage(named: "bookmarkb"), style: .plain, target: self, action: #selector(bookmarkButtonTapped))
        }
        else{
            bookmarkButton = UIBarButtonItem(image: UIImage(named: "bookmark"), style: .plain, target: self, action: #selector(bookmarkButtonTapped))
        }
        
        self.navigationItem.rightBarButtonItems = [shareButton, bookmarkButton!]
        self.navigationController?.navigationBar.tintColor = .white
    
        addLabel()
    }
    
    func addLabel()
    {
        label = UILabel()
        label.backgroundColor = .clear
        label.numberOfLines = 2
        //label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .white
        
        if clickedPos < 40
        {
            label.text = "40 Rabbana\n\(clickedPos + 1)/40"
        }
        else{
            label.text = "40 Rabbana\n40/40"
        }
        self.navigationItem.titleView = label
    }
    
    
    @objc func shareButtonTapped() {
        let shareText = ("\(arrRabbanaList[clickedPos].RabbanaArabic ?? "")\n\n\(arrRabbanaList[clickedPos].RabbanaTranslation ?? "")")
        
        let vc = UIActivityViewController(activityItems: [shareText ], applicationActivities: [])
        present(vc, animated: true)
    }
    
    @objc func bookmarkButtonTapped() {
       // print("bookmark tapped \(clickedPos)")
        
        if (DatabaseHelper.CheckRabbanaExist(r_id: arrRabbanaList[clickedPos].RId!)){
            DatabaseHelper.DeleteRabbana(r_id: arrRabbanaList[clickedPos].RId!)
        }
        else{
            DatabaseHelper.InsertRabbana(r_Id: arrRabbanaList[clickedPos].RId!, r_arabic: "1", r_Transliteration: "1", r_translation: "1", r_audio: "1")
        }
        
        setNavBar()
    }

}

/*extension RabbanaDetailVC: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("audioPlayerDidFinishPlaying")
    }

   /* func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        print("audioPlayerDidFinishPlaying")
        
        if flag {
            // After successfully finish song playing will stop audio player and remove from memory
            print("Audio player finished playing")
            //self.audioPlayer?.stop()
            //self.audioPlayer = nil
            // Write code to play next audio.
        }
    }*/
}*/

extension UILabel{

public var requiredHeight: CGFloat {
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.attributedText = attributedText
    label.sizeToFit()
    return label.frame.height
  }
}
