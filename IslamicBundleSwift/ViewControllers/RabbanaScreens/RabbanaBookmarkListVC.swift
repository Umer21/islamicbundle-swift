//
//  RabbanaBookmarkListVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 23/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class RabbanaBookmarkListVC: UIViewController {
    
    @IBOutlet weak var lblNoData: UILabel!
    var arrRabbanaBookmarks = [Rabbana]()
       var arrRabbanaList = [Rabbana]()
       
       let cellSpacingHeight: CGFloat = 6
       var pos: Int?

    @IBOutlet weak var rabbanaBTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        getBookmarkDuas()
        
        rabbanaBTableView.reloadData()
        rabbanaBTableView.separatorStyle = .none
        rabbanaBTableView.backgroundColor = .none
        rabbanaBTableView.separatorColor = UIColor.clear
        
        self.lblNoData.center = self.view.center
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Bookmark Rabbana List"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    func getBookmarkDuas(){
        arrRabbanaBookmarks = DatabaseHelper.GetAllRabbanaBookmark()
        
        if self.arrRabbanaBookmarks.count > 0 {
            self.lblNoData.isHidden = true
            self.rabbanaBTableView.isHidden = false
            
        }else{
            self.lblNoData.isHidden = false
            self.rabbanaBTableView.isHidden = true
        }
        //arrDuaDetail = DatabaseHelper.getAllDetailDuaCategoryWise()
        
        for index in arrRabbanaBookmarks.indices {
            print("\(index)")
            arrRabbanaList.append(DatabaseHelper.getRabbanaDetailForBookmark(id: arrRabbanaBookmarks[index].RId!))
        }
    }
    

}

extension RabbanaBookmarkListVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRabbanaList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! RabbanaBListCell
        
        let model = self.arrRabbanaList[indexPath.section]
        
        cell.delegate = self
        cell.btnBookmark.tag = indexPath.section
        
        cell.lblArabic.text = model.RabbanaArabic
        cell.lblTranslation.text = model.RabbanaTranslation
        
        cell.layer.cornerRadius = 8
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        pos = indexPath.section
        
        gotoBookmarkRabbanaDetailScreen()
        
    }
    
    func gotoBookmarkRabbanaDetailScreen() {
        let vc = UIStoryboard.init(name: kRabbanaStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRabbanaBookmarkDetailControllerID) as? RabbanaBookmarkDetailVC
        vc?.pos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}

extension RabbanaBookmarkListVC: RabbanaBookmarkListDelegate {
    func RbookmarkTappedd(tag: Int) {
        
        
        let model = self.arrRabbanaList[tag]
        
         if (DatabaseHelper.CheckRabbanaExist(r_id: model.RId!)){
                   DatabaseHelper.DeleteRabbana(r_id: model.RId!)
               }
               else{
                   DatabaseHelper.InsertRabbana(r_Id: model.RId!, r_arabic: "1", r_Transliteration: "1", r_translation: "1", r_audio: "1")
               }
        
        arrRabbanaBookmarks.removeAll()
        arrRabbanaList.removeAll()
        getBookmarkDuas()
        rabbanaBTableView.reloadData()
        
    }
}
