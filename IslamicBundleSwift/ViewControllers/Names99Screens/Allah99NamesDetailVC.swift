//
//  Allah99NamesDetailVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 10/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class Allah99NamesDetailVC: UIViewController {
    
    @IBOutlet weak var AllahNamesDetailTableView: UITableView!
    var arrNameList = [AllahName]()
    var clickedPos: Int = 0

    @IBAction func btn_Previous(_ sender: Any) {
        
        if (clickedPos > 0)
        {
            clickedPos -= 1
            AllahNamesDetailTableView.reloadData()
        }
    }
    
    
    @IBAction func btn_Next(_ sender: Any) {
        
        if (clickedPos < arrNameList.count - 1)
        {
            clickedPos += 1
            AllahNamesDetailTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        AllahNamesDetailTableView.separatorStyle = .none
    }
    
    func setNavBar()
    {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "99 Names of Allah"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let settingButton = UIBarButtonItem(image: UIImage(named: "icon_nav_share"), style: .plain, target: self, action: #selector(settingsTapped))
        //self.navigationItem.leftBarButtonItem = signOutButton
        
        self.navigationItem.rightBarButtonItems = [settingButton]
    }
    
    @objc func settingsTapped() {
        
        let shareText = "\(arrNameList[clickedPos].name)\n\(arrNameList[clickedPos].meaning)\n\(arrNameList[clickedPos].desc)"
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true)
    }

}

extension Allah99NamesDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell1 = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell1 == nil {
            cell1 = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
        switch indexPath.section {
        case 0:
            let cell = AllahNamesDetailTableView.dequeueReusableCell(withIdentifier: "cell1", for:  indexPath as IndexPath) as! NameDetailCell1
            
            
            if let imgPath = arrNameList[clickedPos].Id{
                cell.imgNameD.image = UIImage(named: "name_\(String(describing: imgPath))")
            }
            
            
            cell.lblTransliteration.text = arrNameList[clickedPos].name
            cell.lblMeaningD.text = arrNameList[clickedPos].meaning
            
            
            return cell
            
        case 1:
            
            let cell = AllahNamesDetailTableView.dequeueReusableCell(withIdentifier: "cell2", for:  indexPath as IndexPath) as! NameDetailCell2
            
            cell.lblNameDescriptionD.text = arrNameList[clickedPos].desc
            
            return cell
            
            
        case 2:
            
            let cell = AllahNamesDetailTableView.dequeueReusableCell(withIdentifier: "cell3", for:  indexPath as IndexPath) as! NameDetailCell3
            
            cell.lblNameBenefitD.text = arrNameList[clickedPos].benefits
            
            return cell
            
            
        default:
            break
        }
        
        return cell1!
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
    }
    
    
}
