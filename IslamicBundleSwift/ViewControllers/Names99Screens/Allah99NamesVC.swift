//
//  Allah99NamesVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 10/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class Allah99NamesVC: UIViewController {

    var arrAllahNames = [AllahName]()
    let cellSpacingHeight: CGFloat = 6
    var pos: Int?
    
    @IBOutlet weak var NamesListTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        arrAllahNames = DatabaseHelper.getAllahNames()
        NamesListTableView.separatorStyle = .none
        NamesListTableView.backgroundColor = .none
        NamesListTableView.separatorColor = UIColor.clear
        
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "99 Names of Allah"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    

}

extension Allah99NamesVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrAllahNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! NamelistCell
        
        let model = self.arrAllahNames[indexPath.section]
        cell.lblAllahName.text = model.name
        cell.lblAllahNameMeaning.text = model.meaning
        cell.imgName.image = UIImage(named: "name_\(String(describing: model.Id!))")
        
        cell.layer.cornerRadius = 17
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        pos = indexPath.section
        
        gotoNamesDetailScreen()
        
    }
    
    func gotoNamesDetailScreen() {
        let vc = UIStoryboard.init(name: kMoreScreenStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kName99DetailViewControllerID) as? Allah99NamesDetailVC
        vc?.arrNameList = arrAllahNames
        vc?.clickedPos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
}
