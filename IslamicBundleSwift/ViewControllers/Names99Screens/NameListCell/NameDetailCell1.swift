//
//  NameDetailCell1.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class NameDetailCell1: UITableViewCell {

    @IBOutlet weak var lblMeaningD: UILabel!
    @IBOutlet weak var lblTransliteration: UILabel!
    @IBOutlet weak var imgNameD: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
