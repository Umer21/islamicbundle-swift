//
//  NamelistCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class NamelistCell: UITableViewCell {

    @IBOutlet weak var imgName: UIImageView!
    @IBOutlet weak var lblAllahNameMeaning: UILabel!
    @IBOutlet weak var lblAllahName: UILabel!
    
    @IBOutlet weak var lblArabic: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
