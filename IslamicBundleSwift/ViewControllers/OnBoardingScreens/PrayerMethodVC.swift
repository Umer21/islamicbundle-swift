//
//  PrayerMethodVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 18/05/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class PrayerMethodVC: UIViewController {
    
    let arrMore1 = ["Islamic University, Karachi", "Islamic Society of North America", "Muslim World League", "Makkah, Umm al-Qura", "Egyptian General Authority", "Tehran Institute of Geo Physics", "Jafari - Ithna Ashari"]
    
    @IBOutlet weak var MethodTableView: UITableView!
    let cellSpacingHeight: CGFloat = 6
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        MethodTableView.isScrollEnabled = false
        MethodTableView.separatorStyle = .none
        //MethodTableView.separatorColor = UIColorFromRGB(rgbValue: 0x462D60)
        
        copyDatabaseIfNeeded()
        
    }
}

extension PrayerMethodVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMore1.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PrayerMethodCell
        
        //let model = self.arrMore1[indexPath.section]
        
        
        /*let backgroundView = UIView()
        backgroundView.backgroundColor = UIColorFromRGB(rgbValue: 0x462D60)
        cell.selectedBackgroundView = backgroundView*/
        
        
        cell.lblIndex.text = String (indexPath.section + 1)
        cell.lblPrayerMethod.text = self.arrMore1[indexPath.section]
        
        let savedMethod = Int(getValueForKey(keyValue: "PrayerMethod"))
        
        if savedMethod == indexPath.section {
            cell.imgCheck.image = UIImage(named: "icon_prayercheck_green")
            cell.lblPrayerMethod.textColor = kThemeDarkGreen
        }else{
            cell.imgCheck.image = UIImage(named: "icon_prayercheck")
            cell.lblPrayerMethod.textColor = UIColor.darkGray
        }
        
        //cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        print("Clicked item----\(arrMore1[indexPath.section])")
        
        setDefaultValue(keyValue: "PrayerMethod", valueIs: String (indexPath.section))
        
        MethodTableView.reloadData()
        
        /*let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PrayerMethodCell
        cell.lblIndex.textColor = UIColor.white
        cell.lblPrayerMethod.textColor = UIColor.white*/
        //tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        
       // pos = indexPath.section
        
       // gotoRabbanaDetailScreen()
        
    }
    
    /*func tableView(tableView: UITableView, didDeselectItemAtIndexPath indexPath: NSIndexPath)
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PrayerMethodCell
        cell.lblIndex.textColor = UIColorFromRGB(rgbValue: 0x462D60)
        cell.lblPrayerMethod.textColor = UIColorFromRGB(rgbValue: 0x462D60)
        tableView.reloadRows(at: [(indexPath as IndexPath)], with: UITableView.RowAnimation.none)
    }*/
    
    /*func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PrayerMethodCell
        cell.lblIndex.textColor = UIColorFromRGB(rgbValue: 0x462D60)
        cell.lblPrayerMethod.textColor = UIColorFromRGB(rgbValue: 0x462D60)
        
        tableView.reloadData()
    }*/
    
    /*func gotoRabbanaDetailScreen() {
        let vc = UIStoryboard.init(name: kRabbanaStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRabbanaDetailControllerID) as? RabbanaDetailVC
        vc?.arrRabbanaList = arrRabbana
        vc?.clickedPos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }*/
    
}
