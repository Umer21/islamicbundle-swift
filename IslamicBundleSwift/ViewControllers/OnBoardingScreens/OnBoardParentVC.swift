//
//  OnBoardParentVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 28/05/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class OnBoardParentVC: UIViewController, BWWalkthroughViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func btnClick(_ sender: Any) {
        
        gotoWalkThroughScreens()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        gotoWalkThroughScreens()
    }
    
    func gotoWalkThroughScreens(){
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "Onboard", bundle: nil)
        let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
        let page_zero = stb.instantiateViewController(withIdentifier: kPrayerMethodControllerID)
        let page_one = stb.instantiateViewController(withIdentifier: kJuristictsControllerID)
        let page_two = stb.instantiateViewController(withIdentifier: kLocationSelectionControllerID)
        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_zero)
        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        
        walkthrough.modalPresentationStyle = .fullScreen
        self.present(walkthrough, animated: false, completion: nil)
    }
    
    // MARK: - Walkthrough delegate -
    
    func walkthroughPageDidChange(_ pageNumber: Int) {
        print("Current Page \(pageNumber)")
    }
    
    func walkthroughCloseButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }

}
