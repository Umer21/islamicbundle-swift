//
//  LocationSelectionVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 18/05/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class LocationSelectionVC: UIViewController {

    @IBOutlet weak var lblLocationAddress: UILabel!
    var indicator = UIActivityIndicatorView()
    let locationManager = CLLocationManager()
    let geocoder = GMSGeocoder()
    var Lat = "12.2122"
    var Long = "65.3434"
    
    
    
    @IBAction func btnManual(_ sender: Any) {
        print("Set Location Manually")
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.navigationController?.navigationBar.barTintColor = kPurpleColor
        autocompleteController.delegate = self
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    
    @IBAction func btnLocateMe(_ sender: Any) {
        print("Locate me")
        self.getCurrentLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    func getCurrentLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        //If user denied location services
        let status = CLLocationManager.authorizationStatus()
        if status == .denied {
            gotoSettings()
        }
    }
    
    func gotoSettings() {
        let alertController = UIAlertController (title: "Location service are off. Please allow to fetch resturants.", message: "Go to Settings?", preferredStyle: .alert)

        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func getaddress(latitude: Double, longitude: Double) {
        
        let coordinate = CLLocationCoordinate2DMake(latitude,longitude)
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                //self.locationButton .setTitle("\(lines[0])", for: .normal)
                //self.locationButton.titleLabel?.lineBreakMode = .byTruncatingTail
                self.lblLocationAddress.text = lines[0]
                
                setDefaultValue(keyValue: "CurrentLocation", valueIs: lines[0])
                setDefaultValue(keyValue: "LocationFetched", valueIs: "true")
                
                //initiateNotificationSetup()
                //print("-------\(lines[0])")
                //currentAdd(returnAddress: currentAddress)
            }
            
            self.locationManager.stopUpdatingLocation()
        }
        
    }

}

extension LocationSelectionVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations ==== \(locValue.latitude) \(locValue.longitude)")
        // print("place name = \(latLong(lat: Double (locValue.latitude), long: Double(locValue.longitude)))")
        
        indicator.stopAnimating()
        //apiResturants(Lat: "\(locValue.latitude)", Long: "\(locValue.longitude)")
        Lat = "\(locValue.latitude)"
        print("Latitude === -----------------\(Lat)")
        Long = "\(locValue.longitude)"
        print("Longitude=== -------------------\(Long)")
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (locValue.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (locValue.longitude))
        
        let location = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
        let timeZone = TimezoneMapper.latLngToTimezone(location)
        
        if let tz = timeZone {
            
            print("---before-splitted--\(String(describing: tz))")
            
            let s = String(describing: tz)
            let splittedTz = s.split(separator: " ")
            
            setDefaultValue(keyValue: "TimeZone", valueIs: String(splittedTz[0]))
            print("--after--splitted---\(splittedTz[0])")
        }
        
        print("----get-timeZone---\(getValueForKey(keyValue: "TimeZone"))")
        getaddress(latitude: locValue.latitude, longitude: locValue.longitude)
    }
}

extension LocationSelectionVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(String(describing: place.name))")
        print("Components \(String(describing: place.addressComponents))")
        //locationButton.setTitle(place.formattedAddress, for: .normal)
        lblLocationAddress.text = place.formattedAddress
        
        setDefaultValue(keyValue: "CurrentLocation", valueIs: place.formattedAddress!)
        
        setDefaultValue(keyValue: "Latitude", valueIs: String (place.coordinate.latitude))
        setDefaultValue(keyValue: "Longitude", valueIs: String (place.coordinate.longitude))
        
        let location = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        
        setTimeZone(lat: place.coordinate.latitude, lng: place.coordinate.longitude)
        /*let timeZone = TimezoneMapper.latLngToTimezone(location)
        
        if let tz = timeZone {
            
            print("---before-splitted--\(String(describing: tz))")
            
            let s = String(describing: tz)
            let splittedTz = s.split(separator: " ")
            
            setDefaultValue(keyValue: "TimeZone", valueIs: String(splittedTz[0]))
            print("--after--splitted---\(splittedTz[0])")
        }*/
        
        setDefaultValue(keyValue: "LocationFetched", valueIs: "true")
        
        //initiateNotificationSetup()
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

