//
//  JuristsVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 18/05/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class JuristsVC: UIViewController {
    
    let arrMore1 = ["Standard (Shafi, Malki, Hanbli)", "Hanafi"]
    
    let cellSpacingHeight: CGFloat = 6
    var selectedItem: String?

    @IBOutlet weak var juristsTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        juristsTableView.isScrollEnabled = false
        juristsTableView.separatorStyle = .none
        //juristsTableView.separatorColor = UIColorFromRGB(rgbValue: 0x462D60)
    }
    

}

extension JuristsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMore1.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! JuristicCell
        
        //let model = self.arrMore1[indexPath.section]
        /*let backgroundView = UIView()
        backgroundView.backgroundColor = UIColorFromRGB(rgbValue: 0x462D60)
        cell.selectedBackgroundView = backgroundView*/
        
        cell.lblIndex.text = String (indexPath.section + 1)
        cell.lblJuristicMethod.text = self.arrMore1[indexPath.section]
        
        let savedMethod = Int(getValueForKey(keyValue: "Jurists"))
        
        if savedMethod == indexPath.section {
            cell.imgCheck.image = UIImage(named: "icon_prayercheck_green")
            cell.lblJuristicMethod.textColor = kThemeDarkGreen
        }else{
            cell.imgCheck.image = UIImage(named: "icon_prayercheck")
            cell.lblJuristicMethod.textColor = UIColor.darkGray
        }
        
        //cell.layer.cornerRadius = 6
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        print("Clicked item----\(arrMore1[indexPath.section])")
        
        setDefaultValue(keyValue: "Jurists", valueIs: String (indexPath.section))
        
        juristsTableView.reloadData()
        
       // pos = indexPath.section
        
       // gotoRabbanaDetailScreen()
        
    }
    
    /*func gotoRabbanaDetailScreen() {
        let vc = UIStoryboard.init(name: kRabbanaStoryboard, bundle: Bundle.main).instantiateViewController(withIdentifier: kRabbanaDetailControllerID) as? RabbanaDetailVC
        vc?.arrRabbanaList = arrRabbana
        vc?.clickedPos = pos!
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }*/
    
}
