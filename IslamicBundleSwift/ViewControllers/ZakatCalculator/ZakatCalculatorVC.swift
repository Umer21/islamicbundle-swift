//
//  ZakatCalculatorVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umar on 1/18/23.
//  Copyright © 2023 Umar Farooq. All rights reserved.
//

import UIKit

class ZakatCalculatorVC: UIViewController {

    @IBOutlet weak var tfBank: UITextField!
    @IBOutlet weak var tfGold: UITextField!
    @IBOutlet weak var tfCash: UITextField!
    @IBOutlet weak var tfBonds: UITextField!
    @IBOutlet weak var tfOwe: UITextField!
    @IBOutlet weak var tfOtherAmount: UITextField!
    @IBOutlet weak var tfTotal: UITextField!
    
    @IBOutlet weak var tfZakatAmount: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Zakat Calculator"
        
        // Do any additional setup after loading the view.
        tfBank.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        tfGold.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        tfCash.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        tfBonds.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        tfOwe.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        tfOtherAmount.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
        
        let value1 = Int(tfBank.text ?? "") ?? 0
        let value2 = Int(tfGold.text ?? "") ?? 0
        let value3 = Int(tfCash.text ?? "") ?? 0
        let value4 = Int(tfBonds.text ?? "") ?? 0
        let value5 = Int(tfOwe.text ?? "") ?? 0 //amount to pay other
        let value6 = Int(tfOtherAmount.text ?? "") ?? 0
        
        let sum = (value1 + value2 + value3 + value4 + value6) - value5
        
        self.tfTotal.text = "\(sum)"
        
        //percentage calculator
        var zakatAmt = 0.0
        zakatAmt = 0.025 * Double(sum)
        self.tfZakatAmount.text = "\(Int(zakatAmt))"
            
    }
    

}

extension ZakatCalculatorVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
}
