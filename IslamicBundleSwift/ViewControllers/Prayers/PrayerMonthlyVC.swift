//
//  PrayerMonthlyVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/11/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit
import Adhan

class PrayerMonthlyVC: UIViewController {
    
    @IBOutlet weak var tblMonthly: UITableView!
    var itemsMonth =  [PrayerModelMonthly]()
    let cellSpacingHeight: CGFloat = 0.5
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgCurrent: UIImageView!
    
    var namazName: String = ""
    var namazTime: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        displayDateData()
        getCurrentMonthDays()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func getCurrentMonthDays() {
        let calendar = Calendar.current
        let date = Date()
        
        // Calculate start and end of the current year (or month with `.month`):
        let interval = calendar.dateInterval(of: .month, for: date)!
        
        // Compute difference in days:
        let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day!
        //print("============== \(days)")
        
        itemsMonth.removeAll()
        
        for index in (0...days - 1) {
            //print(index)
            getAdhanTimingsMonthly(ind: index)
        }
        
        
        tblMonthly.reloadData()
        //print("first day--- \(Date().startOfMonth())")
        //print("last day--- \(Date().endOfMonth())")
        //print("Next-Month--- \(Date().getNextMonth())")
        //print("Previous-Month--- \(Date().getPreviousMonth())")
        
        
        /*print("----prayer-size-- \(itemsMonth.count)")
        print("first day--- \(Date().startOfMonth())")
        
        for ip in (0...itemsMonth.count - 1) {
            print("Index-- \(ip)")
            print("Prayer-Fajr-- \(itemsMonth[ip].fajrTime)")
            print("Prayer-Sunrise-- \(itemsMonth[ip].sunriseTime)")
            print("Prayer-Dhuhr-- \(itemsMonth[ip].dhuhrTime)")
            print("Prayer-asr-- \(itemsMonth[ip].asrTime)")
            print("Prayer-maghrib-- \(itemsMonth[ip].maghribTime)")
            print("Prayer-isha-- \(itemsMonth[ip].ishaTime)")
        }*/
        
    }
    
    func getAdhanTimingsMonthly(ind : Int)
    {
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        var date = cal.dateComponents([.year, .month, .day], from: Date().startOfMonth())
        let today = Date().startOfMonth()
        //print("today--- \(today)")
        
        let modifiedDate = Calendar.current.date(byAdding: .day, value: ind, to: today)!
        date = cal.dateComponents([.year, .month, .day], from: modifiedDate)
        //print("date----- \(date)")
        
        var lat: Double?
        var lng: Double?
        
        if isKeyPresentInUserDefaults(key: "Latitude"){
            lat = Double (getValueForKey(keyValue: "Latitude"))
            lng = Double (getValueForKey(keyValue: "Longitude"))
        }
        else{
            lat = 21.3891
            lng = 39.8579
        }
        
        let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
        let jurists = getValueForKey(keyValue: "Jurists")
        
        let coordinates = Coordinates(latitude: lat!, longitude: lng!)
        var params = CalculationMethod.karachi.params
        
        switch prayerMethod {
        case "0":
            params = CalculationMethod.karachi.params
            break
        case "1":
            params = CalculationMethod.northAmerica.params
            break
        case "2":
            params = CalculationMethod.muslimWorldLeague.params
            break
        case "3":
            params = CalculationMethod.ummAlQura.params
            break
        case "4":
            params = CalculationMethod.egyptian.params
            break
        case "5":
            params = CalculationMethod.tehran.params
            break
        case "6":
            params = CalculationMethod.other.params
            break
            
        default:
            params = CalculationMethod.karachi.params
            break
        }
        
        switch jurists {
        case "0":
            params.madhab = .shafi
            break
        case "1":
            params.madhab = .hanafi
            break
            
        default:
            params.madhab = .shafi
            break
        }
        
        if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
            let formatter = DateFormatter()
            formatter.timeStyle = .medium
            
            if isKeyPresentInUserDefaults(key: "TimeZone"){
                let tz = getValueForKey(keyValue: "TimeZone")
                formatter.timeZone = TimeZone(identifier: tz)!
            }
            else{
                let tz = "Asia/Riyadh"
                formatter.timeZone = TimeZone(identifier: tz)!
            }
            
            let model = PrayerModelMonthly()
            model.fajrTime = formatDateForDisplay1(date: prayers.fajr)
            model.sunriseTime = formatDateForDisplay1(date: prayers.sunrise)
            model.dhuhrTime = formatDateForDisplay1(date: prayers.dhuhr)
            model.asrTime = formatDateForDisplay1(date: prayers.asr)
            model.maghribTime = formatDateForDisplay1(date: prayers.maghrib)
            model.ishaTime = formatDateForDisplay1(date: prayers.isha)
            itemsMonth.append(model)
        }
    }
    
    func displayDateData()
    {
        //view1.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        //view2.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        //view3.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        //view4.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        //view5.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        //view6.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        let cpdetail = getCurrentPrayerDetail()
        
        switch cpdetail[0].NextNamazName! {
        case Prayer.fajr:
            //print("Fajr")
            
            namazName = "Fajr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrent.image = UIImage(named:"fajr_icon")
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Fajr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            //prayerIndex = 0
            //prayerName = "Fajr"
            break
        case Prayer.sunrise:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Sunrise")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Sunrise"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrent.image = UIImage(named:"afternoon_icon")
            
            //prayerIndex = 1
            //prayerName = "Sunrise"
            break
        case Prayer.dhuhr:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Dhuhr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Dhuhr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrent.image = UIImage(named:"dhuhr_icon")
            
            //prayerIndex = 2
            //prayerName = "Dhuhr"
            break
        case Prayer.asr:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Asr")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Asr"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrent.image = UIImage(named:"asr_icon")
            
            //prayerIndex = 3
            //prayerName = "Asr"
            break
        case Prayer.maghrib:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Maghrib")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Maghrib"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrent.image = UIImage(named:"magrib_icon")
            
            //prayerIndex = 4
            //prayerName = "Maghrib"
            break
        case Prayer.isha:
            
            setDefaultValue(keyValue: "CurrentNamaz", valueIs: "Isha")
            setDefaultValue(keyValue: "CurrentNamazTime", valueIs: cpdetail[0].NextNamazTime!)
            
            namazName = "Isha"
            namazTime = cpdetail[0].NextNamazTime!
            
            imgCurrent.image = UIImage(named:"fajr_icon")
            
            //prayerIndex = 5
            //prayerName = "Isha"
            break
        default:
            print("no Namaz")
        }
        
        let currentDate = Date().string(format: "MMMM yyyy")
        lblDate.text = currentDate + "\n" + getHijriDateForMonthScreen(gregorianDate: currentDate)
    }
    
}


extension PrayerMonthlyVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.itemsMonth.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! prayerMonthlyCell
        
        let model = self.itemsMonth[indexPath.section]
        cell.lblDay.text = String (indexPath.section + 1)
        cell.viewDay.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        cell.lblFajr.text = timeConversion24(time12: model.fajrTime!)
        cell.viewFajr.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        cell.lblSunrise.text = timeConversion24(time12: model.sunriseTime!)//model.sunriseTime
        cell.viewSunrise.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        cell.lblDhuhr.text = timeConversion24(time12: model.dhuhrTime!)//model.dhuhrTime
        cell.viewDhuhr.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        cell.lblAsr.text = timeConversion24(time12: model.asrTime!)//model.asrTime
        cell.viewAsr.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        cell.lblMaghrib.text = timeConversion24(time12: model.maghribTime!)//model.maghribTime
        cell.viewMaghrib.addRightBorderWithColor(color: UIColorFromRGB(rgbValue: 0xd1d1d1), width: 0.5)
        
        cell.lblIsha.text = timeConversion24(time12: model.ishaTime!)//model.ishaTime
                
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.section).")
        
        //let cell = tableView.cellForRow(at: indexPath)
        // For Zoom  in
        //let trans = cell!.contentView.transform.scaledBy(x: 0.1, y: 0.1)
        //view.transform = trans

        // For Zoom Out
        //let trans = cell!.contentView.transform.scaledBy(x: 0.01, y: 0.01)
        //view.transform = trans
        
    }
    
}

extension UIView {
  func addTopBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
    self.layer.addSublayer(border)
  }

  func addRightBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
    self.layer.addSublayer(border)
  }

  func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
    self.layer.addSublayer(border)
  }

  func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
    self.layer.addSublayer(border)
  }
}
