//
//  Song.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 06/05/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation

public struct Song: Equatable {
    let id: Int
    let name: String
    let fileName: String
}
