//
//  SoundCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 30/04/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class SoundCell: UITableViewCell {

    
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var imgSound: UIImageView!
    @IBOutlet weak var lblSound: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
