//
//  prayerMonthlyCell.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 12/11/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class prayerMonthlyCell: UITableViewCell {

    @IBOutlet weak var viewIsha: UIView!
    @IBOutlet weak var viewMaghrib: UIView!
    @IBOutlet weak var viewAsr: UIView!
    @IBOutlet weak var viewDhuhr: UIView!
    @IBOutlet weak var viewSunrise: UIView!
    @IBOutlet weak var viewFajr: UIView!
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var lblDay: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var lblFajr: UILabel!
    @IBOutlet weak var lblSunrise: UILabel!
    @IBOutlet weak var lblDhuhr: UILabel!
    @IBOutlet weak var lblAsr: UILabel!
    @IBOutlet weak var lblMaghrib: UILabel!
    @IBOutlet weak var lblIsha: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
