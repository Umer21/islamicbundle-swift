//
//  PrayerSoundVC.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 29/04/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import UIKit

class PrayerSoundVC: UIViewController {
    
    let cellSpacingHeight: CGFloat = 0
    let audioArr = ["None", "Silent", "Default Notification Sound", "Azan Al Aqsa", "Azan Makkah Fajr", "Azan Madina Fajr", "Azan Turkish"]
    var pos: Int?
    let arrPrayerNames = ["Fajr","Sunrise","Dhuhr","Asr","Maghrib","Isha"]
    var currentPrayer: String?
    var currentPosition: Int?

    @IBOutlet weak var soundTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        soundTableview.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
         
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "\(currentPrayer ?? "") Notification"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    
}

extension PrayerSoundVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.audioArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! SoundCell
        
        //let model = self.audioArr[indexPath.section]
        cell.lblSound.text = self.audioArr[indexPath.row]
        
        if indexPath.row == 0 {
            cell.imgSound.image = UIImage(named: "icon_none")
            cell.btn_play.isHidden = true
        }
        else if indexPath.row == 1 {
            cell.imgSound.image = UIImage(named: "icon_silent")
            cell.btn_play.isHidden = true
        }
        else{
            cell.imgSound.image = UIImage(named: "icon_sound_on")
            cell.btn_play.isHidden = false
        }
        
        let currPrayerStatus = getValueForKey(keyValue: "Sound-" + currentPrayer!)
        print("Current-Prayer-Status > \(currPrayerStatus)")
        
        if (indexPath.row == Int(currPrayerStatus)) {
            
            cell.imgSelected.image = UIImage(named: "icon_prayercheck_green")
        }
        else{
            
            cell.imgSelected.image = UIImage(named: "icon_prayercheck")
        }
        
        cell.btn_play.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        print("You tapped cell number \(indexPath.row).")
        pos = indexPath.row
        
        if let posit = pos {
            let finalvalue = String(posit)
            print(finalvalue)
            
            setDefaultValue(keyValue: "Sound-" + currentPrayer!, valueIs: finalvalue)
            print("Clicked icon: \("Sound-" + currentPrayer!)-----\(finalvalue)")
            
            soundTableview.reloadData()
            
            if posit == 0 {
                setNamazAlarmValue(index: currentPosition!, namaz: currentPrayer!, value: "0")
            }
            else{
                setNamazAlarmValue(index: currentPosition!, namaz: currentPrayer!, value: "1")
            }
            
            resetPrayerNotificationsOnSettingsChange()
        }
        
        
        
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        // Let's localize the index of the button using a helper method
        // and also localize the Song i the database
        if let index = getViewIndexInTableView(tableView: soundTableview, view: sender),
            let song = Azaans.shared.getSong(index.row) {
            print("---index------\(index)")
            print("----song------\(song)")
            // If a the song located is the same it's currently playing just stop
            // playing it and return.
            guard song != Player.shared.currentlyPlaying() else {
                stopCurrentlyPlaying()
                return
            }
            // Stop any playing song if necessary
            stopCurrentlyPlaying()
            // Staart playing the song
            Player.shared.play(this: song)
            // Change the tapped button to a Stop image
            changeButtonImage(sender, play: false)
        }
    }
    
    func getViewIndexInTableView(tableView: UITableView, view: UIView) -> IndexPath? {
        let pos = view.convert(CGPoint.zero, to: tableView)
        return soundTableview.indexPathForRow(at: pos)
    }
    
    func stopCurrentlyPlaying() {
        if let currentSong = Player.shared.currentlyPlaying() {
            Player.shared.stop()
            if let indexStop = Azaans.shared.getPosition(currentSong) {
                print("------------Index-Stop\(indexStop)")
                let cell = soundTableview.cellForRow(at: IndexPath(item: indexStop, section: 0)) as! SoundCell
                changeButtonImage(cell.btn_play, play: true)
            }
        }
    }
    
    func changeButtonImage(_ button: UIButton, play: Bool) {
        UIView.transition(with: button, duration: 0.2,
                          options: .transitionCrossDissolve, animations: {
                            button.setImage(UIImage(named: play ? "icon_azan_play" : "icon_azan_pause"), for: .normal)
        }, completion: nil)
        
    }
    
}

