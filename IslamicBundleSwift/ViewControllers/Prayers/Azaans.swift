//
//  Azaans.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 06/05/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation

class Azaans {
    static var shared = Azaans()
 
    let data: [Song]!
    var count: Int {
        get { return data.count }
    }

    init() {
        data = [
            Song(id: 01,  name: "Silence",  fileName: "silence"),
            Song(id: 02,  name: "Silence",  fileName: "silence"),
            Song(id: 03,  name: "Default_notification",  fileName: "default_notification"),
            Song(id: 04, name: "Azan_Al_Aqsa", fileName: "azan_al_aqsa"),
            Song(id: 05,   name: "Azan_Fajr_Makkah",  fileName: "azan_fajr_makkah"),
            Song(id: 06,   name: "Azan_Madinah_Fajr",  fileName: "azan_madinah_fajr"),
            Song(id: 07,   name: "Azan_Turkish",  fileName: "azan_turkish"),
        ]
    }
    
    func getSong(_ position: Int) -> Song? {
        guard 0...(data.count-1) ~= position else { return nil }
        return data[position]
    }
    
    func getPosition(_ song: Song) -> Int? {
        return data.firstIndex { $0 == song }
    }
}
