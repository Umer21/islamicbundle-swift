//
//  QiblaVC.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/26/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import CoreLocation

class QiblaVC: UIViewController {

    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgCompass: UIImageView!
    
    var lat: Double?
    var lng: Double?
    
    var compassManager  : CompassDirectionManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBar()
        
        
    }
    
    func setNavBar()
    {
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColorFromRGB(rgbValue: 0x2A203C)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "Qibla"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        //UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        //self.navigationController?.isNavigationBarHidden = true
        // self.navigationController!.navigationBar.topItem?.title = "Pending Orders"
        self.tabBarController?.tabBar.isHidden = false
        
        showCompass()
        
    }
    
    func showCompass(){
        lat = Double (getValueForKey(keyValue: "Latitude"))
        lng = Double (getValueForKey(keyValue: "Longitude"))
        
        if isKeyPresentInUserDefaults(key: "CurrentLocation"){
            lblLocation.text = getValueForKey(keyValue: "CurrentLocation")
        }
        else{
            lblLocation.text = "Mecca, Saudia Arabia"
        }
        
       compassManager =  CompassDirectionManager(dialerImageView: imgCompass, pointerImageView: imgCompass)
       compassManager.initManager()
    }
    

}
