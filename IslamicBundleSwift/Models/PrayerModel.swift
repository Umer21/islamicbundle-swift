//
//  PrayerModel.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 28/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class PrayerModel: NSObject{
    
    var alarm: Bool?
    var prayerName: String?
    var prayerTime: String?
    
}


class PrayerModelMonthly: NSObject{
    
    var fajrTime: String?
    var sunriseTime: String?
    var dhuhrTime: String?
    var asrTime: String?
    var maghribTime: String?
    var ishaTime: String?
}
