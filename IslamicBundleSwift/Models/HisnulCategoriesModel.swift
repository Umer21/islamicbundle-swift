//
//  HisnulCategoriesModel.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class HisnulCategoriesModel: NSObject {
    
    var CategoryId: String?
    var ID: String?
    var ArabicDua: String?
    var EnglishDua: String?
}
