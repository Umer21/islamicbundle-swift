//
//  PrayerTrackModel.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 07/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class PrayerTrackModel: NSObject{

    var trackId: Int64?
    var tahajjud: Int64?
    var fajr: Int64?
    var ishraq: Int64?
    var chasht: Int64?
    var dhuhr: Int64?
    var asr: Int64?
    var maghrib: Int64?
    var isha: Int64?
    var trackDate: String?
}
