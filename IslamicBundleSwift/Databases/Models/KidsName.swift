//
//  KidsName.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umar on 1/17/23.
//  Copyright © 2023 Umar Farooq. All rights reserved.
//

import UIKit

class KidsName: NSObject{
    var Id : Int64?
    var name : String?
    var arabic : String?
    var meaning : String?
    var dates : String?
}
