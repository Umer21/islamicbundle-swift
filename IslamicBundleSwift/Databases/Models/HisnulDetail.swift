//
//  HisnulDetail.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 21/07/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class HisnulDetail: NSObject {
    
    var Id: String?
    var chapterId: String?
    var Dua: String?
    var Transliteration: String?
    var Translation: String?
    var Category_Name: String?
}
