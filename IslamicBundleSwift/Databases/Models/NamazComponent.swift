//
//  NamazComponent.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 13/04/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import Adhan

class NamazComponent: NSObject{
 
    var NextNamazName: Prayer?
    var NextNamazTime: String?
}
