//
//  quran.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 25/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class quran: NSObject {
    
    var Id : Int64?
    var Surah_No : Int64?
    var Ayah_No : Int64?
    var Surah_Name : String?
    var Ayat : String?
    var En_Translation : String?
    var Transliteration : String?
    var Fav_Ayah : Int64?
    
}
