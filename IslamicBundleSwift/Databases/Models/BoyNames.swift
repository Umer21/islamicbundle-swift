//
//  BoyNames.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 26/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class BoyNames: NSObject
{
    var BId: String?
    var BoyName: String?
    var NameArabic: String?
    var NameMeaning: String?
    var BDate: String?
    
}
