//
//  SurahList.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 06/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class SurahList: NSObject{

    var sID: Int64?
    var SurahNo: Int64?
    var SurahName: String?
    var SurahNameArabic: String?
    var SurahRukus: Int64?
    var SurahNameMeaning: String?
    var SurahTotalAyaat: Int64?
}
