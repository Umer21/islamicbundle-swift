//
//  Rabbana.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 26/02/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit

class Rabbana: NSObject{
 
    var RId: String?
    var RabbanaArabic: String?
    var RabbanaTransliteration: String?
    var RabbanaTranslation: String?
    var RabbanaAudio: String?
}
