//
//  SurahDetail.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 09/03/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation

class SurahDetail: NSObject{

    var sID: Int64 = 0
    var SurahNo: Int64 = 0
    var AyahNo: Int64 = 0
    var SurahName: String = ""
    var Ayat: String = ""
    var EnTranslation: String = ""
    var Transliteration: String = ""
    var FavAyah: Int64 = 0
    var isClicked: Bool = false
}
