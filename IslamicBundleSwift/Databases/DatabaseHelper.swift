//
//  DatabaseHelper.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/30/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import Foundation
import SQLite

class DatabaseHelper: NSObject {
    
    
    class func getAllahNames() -> [AllahName] {
        var arrAllah = [AllahName]()
        
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            for row in try db.prepare("SELECT id, name, meaning, desc, benefits FROM names") {
                let obj = AllahName()
                
                obj.Id = row[0] as? Int64
                obj.name = row[1] as? String
                obj.meaning = row[2] as? String
                obj.desc = row[3] as? String
                obj.benefits = row[4] as? String
                
                arrAllah.append(obj)
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
        return arrAllah
    }
    
    class func getKidsNames(type: String) -> [KidsName] {
        var arrKidsName = [KidsName]()
        
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            var tableName = "boy_names"
            if (type == "Girls") {
                tableName = "girl_names"
            }
            
            for row in try db.prepare("SELECT b_id, kid_name, arabic, meaning, dates FROM \(tableName)") {
                let obj = KidsName()
                
                obj.Id = row[0] as? Int64
                obj.name = row[1] as? String
                obj.arabic = row[2] as? String
                obj.meaning = row[3] as? String
                obj.dates = row[4] as? String
                
                arrKidsName.append(obj)
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
        return arrKidsName
    }
    
    class func getDataWithTranslationByDates(date: String, current_table: String) -> [quran] {
        var arrQuran = [quran]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            //let tbl_chapters = Table("chapter")
          
            let query = "Select quran .surah_no, quran .ayah_no, quran .text, chapter .name_transliteration , translation_eng .text, table_transliteration .text  from quran inner join chapter on quran .surah_no = chapter .surah_no  inner join translation_eng on quran .surah_no = translation_eng .surah_no and translation_eng .ayah_no = quran .ayah_no  inner join table_transliteration on quran .surah_no = table_transliteration .sura and table_transliteration .aya = quran .ayah_no  inner join tb_dates on quran .surah_no = tb_dates .surah_no and quran .ayah_no = tb_dates .ayah_no where tb_dates .dates  = '\(date)'"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = quran()
                
                //obj.Id = row[0] as? Int64
                obj.Surah_No = row[0] as? Int64
                obj.Ayah_No = row[1] as? Int64
                obj.Ayat = row[2] as? String
                obj.Surah_Name = row[3] as? String
                obj.En_Translation = row[4] as? String
                obj.Transliteration = row[5] as? String
                //obj.Fav_Ayah = row[7] as? Int64
                
                arrQuran.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrQuran
        
        
    }
    
    class func getDetailDuaRandom() -> [Hisnul] {
        var arrHisnul = [Hisnul]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT hisnul_items .chapterid , hisnul_items .ar , hisnul_items .trans , hisnul_items .en, hisnul_chapters .en FROM hisnul_items inner join hisnul_chapters on hisnul_chapters ._id  = hisnul_items .chapterid  order by RANDOM() Limit 1"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = Hisnul()
                
                //obj.Id = row[0] as? Int64
                obj.Id = row[0] as? String
                obj.Dua = row[1] as? String
                obj.Transliteration = row[2] as? String
                obj.Translation = row[3] as? String
                obj.Category_Name = row[4] as? String
                
                arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
        
        
    }
    
    class func getDuaCategories() -> [HisnulCategoriesModel] {
        var arrHisnul = [HisnulCategoriesModel]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT category_id, _id, ar , en  FROM hisnul_chapters"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = HisnulCategoriesModel()
                
                //obj.Id = row[0] as? Int64
                obj.CategoryId = row[0] as? String
                obj.ID = row[1] as? String
                obj.ArabicDua = row[2] as? String
                obj.EnglishDua = row[3] as? String
                
                arrHisnul.append(obj)
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
        
        
    }
    
    class func getNameByDate(date: String) -> [BoyNames] {
        var arrBoyNames = [BoyNames]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM boy_names where boy_names .dates  = '\(date)'"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = BoyNames()
                
                //obj.Id = row[0] as? Int64
                obj.BId = row[0] as? String
                obj.BoyName = row[1] as? String
                obj.NameArabic = row[2] as? String
                obj.NameMeaning = row[3] as? String
                obj.BDate = row[4] as? String
                
                arrBoyNames.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrBoyNames
        
        
    }
    
    class func getRandomRabbanaInfo() -> [Rabbana] {
        var arrRabbana = [Rabbana]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM rabbana order by random() limit 1"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = Rabbana()
                
                //obj.Id = row[0] as? Int64
                obj.RId = row[0] as? String
                obj.RabbanaArabic = row[1] as? String
                obj.RabbanaTransliteration = row[2] as? String
                obj.RabbanaTranslation = row[3] as? String
                obj.RabbanaAudio = row[4] as? String
                
                arrRabbana.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrRabbana
        
        
    }
    
    class func getRabbanaList() -> [Rabbana] {
        var arrRabbana = [Rabbana]()
        do {
            //let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM rabbana"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = Rabbana()
                
                //obj.Id = row[0] as? Int64
                if let rid = row[0] as? Int64{
                    obj.RId = String (rid)
                }
                obj.RabbanaArabic = row[1] as? String
                obj.RabbanaTransliteration = row[2] as? String
                obj.RabbanaTranslation = row[3] as? String
                obj.RabbanaAudio = row[4] as? String
                
                arrRabbana.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrRabbana
        
        
    }
    
    class func getAllDetailDuaCategoryWise() -> [HisnulDetail] {
        var arrHisnul = [HisnulDetail]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT hisnul_items ._id, hisnul_items .chapterid , hisnul_items .ar , hisnul_items .trans , hisnul_items .en, hisnul_chapters .en FROM hisnul_items inner join hisnul_chapters on hisnul_chapters ._id  = hisnul_items .chapterid "
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = HisnulDetail()
                
                //obj.Id = row[0] as? Int64
                if let hid = row[0] as? Int64{
                    obj.Id = String (hid)
                }
                
                if let hcid = row[1] as? Int64{
                    obj.chapterId = String (hcid)
                }
                
                obj.Dua = row[2] as? String
                obj.Transliteration = row[3] as? String
                obj.Translation = row[4] as? String
                obj.Category_Name = row[5] as? String
                
                arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
        
        
    }
    
    //Select quran .surah_no, quran .ayah_no, quran .text, chapter .name_transliteration , translation_eng .text, table_transliteration .text, quran .fav_ayah  from quran inner join chapter on quran .surah_no = chapter .surah_no  inner join translation_eng on quran .surah_no = translation_eng .surah_no and translation_eng .ayah_no = quran .ayah_no  inner join table_transliteration on quran .surah_no = table_transliteration .sura and table_transliteration .aya = quran .ayah_no  where quran .surah_no = 1
    
    class func getDuaDetailForBookmark(id: String, cid: String) -> HisnulDetail {
           //var arrHisnul = [HisnulDetail]()
           let arrHisnul = HisnulDetail()
           do {
               let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
               let db = try Connection(path)
               
               
               let query = "SELECT hisnul_items ._id, hisnul_items .chapterid , hisnul_items .ar , hisnul_items .trans , hisnul_items .en FROM hisnul_items Where _id = \(id) and chapterid = \(cid)"
               
               print(query)
               
               for row in try db.prepare(query) {
                  // let obj = HisnulDetail()
                   
                   //obj.Id = row[0] as? Int64
                   if let hid = row[0] as? Int64{
                       arrHisnul.Id = String (hid)
                   }
                   
                   if let hcid = row[1] as? Int64{
                       arrHisnul.chapterId = String (hcid)
                   }
                   
                   arrHisnul.Dua = row[2] as? String
                   arrHisnul.Transliteration = row[3] as? String
                   arrHisnul.Translation = row[4] as? String
                   arrHisnul.Category_Name = "category"
                   
                   //arrHisnul.append(obj)
                   
                   
                   //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
               }
           } catch {
               //handle error
               print(error)
           }
                   
           return arrHisnul
           
           
       }
    
    class func getDetailDuaCategoryWise(pos: Int64) -> [HisnulDetail] {
        var arrHisnul = [HisnulDetail]()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT hisnul_items ._id, hisnul_items .chapterid , hisnul_items .ar , hisnul_items .trans , hisnul_items .en, hisnul_chapters .en FROM hisnul_items inner join hisnul_chapters on hisnul_chapters ._id  = hisnul_items .chapterid  Where chapterid =  \(pos)"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = HisnulDetail()
                
                //obj.Id = row[0] as? Int64
                if let hid = row[0] as? Int64{
                    obj.Id = String (hid)
                }
                
                if let hcid = row[1] as? Int64{
                    obj.chapterId = String (hcid)
                }
                
                obj.Dua = row[2] as? String
                obj.Transliteration = row[3] as? String
                obj.Translation = row[4] as? String
                obj.Category_Name = row[5] as? String
                
                arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
        
        
    }
    
    class func GetAllHisnulBookmark() -> [HisnulBookmark]  {
        
        var arrHisnul = [HisnulBookmark]()
        do {
           let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM Hisnul_bookmark"
            print(query)
            
            for row in try db.prepare(query) {
                let obj = HisnulBookmark()
                
                obj.Id = row[0] as? String
                obj.Dua = row[1] as? String
                obj.Translation = row[2] as? String
                obj.Audio = row[3] as? String
                
                arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
    }
    
    class func GetHisnulBookmarkExist(h_id: String, h_cid: String) -> [HisnulBookmark]  {
        
        var arrHisnul = [HisnulBookmark]()
        do {
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM Hisnul_bookmark where hisnul_id = '\(h_id)' and hisnul_dua = '\(h_cid)'"
            print(query)
            
            for row in try db.prepare(query) {
                let obj = HisnulBookmark()
                
                obj.Id = row[2] as? String
                obj.Dua = row[3] as? String
                obj.Translation = row[4] as? String
                obj.Audio = row[5] as? String
                
                arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
    }
    
    class func CheckHisnulDuaExist(h_id: String, h_cid: String) -> Bool {
        
        var recordExist: Bool = false
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let query = "SELECT * FROM Hisnul_bookmark where hisnul_id = '\(h_id)' and hisnul_dua = '\(h_cid)'"
            print("query")
            
            for row in try db.prepare(query) {
                
                if row.count > 0{
                    recordExist = true
                }
                else{
                    recordExist = false
                }
            }
            
        } catch {
            //handle error
            print(error)
        }
        
        return recordExist
    }
    
    //---------------------------------------------------------------------------------------
    class func InsertHisnulDua(hisnulId: String, hisnulDua: String, hisnulTranslation: String, audio: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            print("Path---> \(path)")
            
            let query = "Insert into Hisnul_bookmark (hisnul_id, hisnul_dua, hisnul_translation, audio) values (\(hisnulId), \(hisnulDua), \(hisnulTranslation), \(audio))"
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }
        
    }
    
    class func DeleteHisnulDua(hisnul_id: String, hisnul_cid: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let query = "Delete from Hisnul_bookmark where hisnul_id = '\(hisnul_id)' and hisnul_dua = '\(hisnul_cid)'"
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }
        
    }
    //---------------------------------------------------------------------------------------

    
    class func getSurahList() -> [SurahList] {
        var arrSurahList = [SurahList]()
        do {
            //let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM chapter"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = SurahList()
                
                obj.sID = row[0] as? Int64
                obj.SurahNo = row[1] as? Int64
                obj.SurahNameArabic = row[2] as? String
                obj.SurahName = row[3] as? String
                obj.SurahRukus = row[4] as? Int64
                obj.SurahNameMeaning = row[5] as? String
                obj.SurahTotalAyaat = row[6] as? Int64
                
                arrSurahList.append(obj)
                                
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrSurahList
        
        
    }
    
    class func getSurahDetail(pos: Int64, currTable: String) -> [SurahDetail] {
        var arrSurahDetail = [SurahDetail]()
        do {
            //let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            let db = try Connection(path)
            
            
            let query = "Select quran .surah_no, quran .ayah_no, quran .text, chapter .name_transliteration , \(currTable) .text, table_transliteration .text, quran .fav_ayah  from quran inner join chapter on quran .surah_no = chapter .surah_no  inner join \(currTable) on quran .surah_no = \(currTable) .surah_no and \(currTable) .ayah_no = quran .ayah_no  inner join table_transliteration on quran .surah_no = table_transliteration .sura and table_transliteration .aya = quran .ayah_no  where quran .surah_no = \(pos)"
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = SurahDetail()
                
                obj.SurahNo = (row[0] as? Int64)!
                obj.AyahNo = (row[1] as? Int64)!
                obj.Ayat = (row[2] as? String)!
                obj.SurahName = (row[3] as? String)!
                obj.EnTranslation = (row[4] as? String)!
                obj.Transliteration = (row[5] as? String)!
                obj.FavAyah = (row[6] as? Int64)!
                
                arrSurahDetail.append(obj)
                                
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrSurahDetail
    }
    
    class func UpdateSurahDetail(surahNo: String, ayahNo: String, favValue: String) {
        
        do {
            //let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            let db = try Connection(path)
            
            let query = "Update quran set fav_ayah = \(favValue) where surah_no = \(surahNo) and ayah_no = \(ayahNo)"
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
           
        } catch {
            //handle error
            print(error)
        }
        
    }
    
    //Prayer Tracker Queries
    
    class func InsertPrayerTrackDetail(trackId: Int64, tahajud: Int64, fajr: Int64, Ishraq: Int64, chasht: Int64,
                                  dhuhr: Int64, asr: Int64, maghrib: Int64, isha: Int64, trackerDate: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            print("Path---> \(path)")
            //INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY)
            //VALUES (1, 'Paul', 32, 'California', 20000.00 );
            
            let query = "Insert into PrayerTracker (trackerid, tahajjud, fajr, ishraq, chasht, dhuhr, asr, maghrib, isha, trackerDate) values (\(trackId), \(tahajud), \(fajr), \(Ishraq), \(chasht), \(dhuhr), \(asr), \(maghrib), \(isha), '\(trackerDate)')"
            /*let query = "Update quran set fav_ayah = \(favValue) where surah_no = \(surahNo) and ayah_no = \(ayahNo)"*/
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }
        
    }
    
    class func UpdatePrayerDetail(trackId: Int64, tahajud: Int64, fajr: Int64, Ishraq: Int64, chasht: Int64,
    dhuhr: Int64, asr: Int64, maghrib: Int64, isha: Int64, trackerDate: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let query = "Update PrayerTracker set trackerid = \(trackId), tahajjud = \(tahajud), fajr = \(fajr), ishraq = \(Ishraq), chasht = \(chasht), dhuhr = \(dhuhr), asr = \(asr), maghrib = \(maghrib), isha = \(isha) where trackerDate = '\(trackerDate)'"
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
           
        } catch {
            //handle error
            print(error)
        }
        
    }
    
    class func CheckPrayerExist(trackerDate: String) -> Bool {
        
        var recordExist: Bool = false
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let query = "SELECT * FROM PrayerTracker where trackerDate = '\(trackerDate)'"
            
            for row in try db.prepare(query) {
                
                if row.count > 0{
                    recordExist = true
                }
                else{
                    recordExist = false
                }
            }
            
        } catch {
            //handle error
            print(error)
        }
        
        return recordExist
    }
    
    //SELECT hisnul_items .chapterid , hisnul_items .ar , hisnul_items .trans , hisnul_items .en, hisnul_chapters .en FROM hisnul_items inner join hisnul_chapters on hisnul_chapters ._id  = hisnul_items .chapterid  Where chapterid = 1
    
    class func getAllPrayerTrack(trackerDate: String) -> [PrayerTrackModel] {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        var arrSurahList = [PrayerTrackModel]()
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            //let updatedDate = "\'" + trackerDate + "\'"
            //print(updatedDate)
            print("Path---> \(path)")
            //.replacingOccurrences(of: "\"", with: "\'")
            let query = "SELECT * FROM PrayerTracker where trackerDate =  '\(trackerDate)'"//.replacingOccurrences(of: "'\'", with: "")
            
            print(query)
            
            for row in try db.prepare(query) {
                let obj = PrayerTrackModel()
                
                obj.trackId = row[0] as? Int64
                obj.tahajjud = row[1] as? Int64
                obj.fajr = row[2] as? Int64
                obj.ishraq = row[3] as? Int64
                obj.chasht = row[4] as? Int64
                obj.dhuhr = row[5] as? Int64
                obj.asr = row[6] as? Int64
                obj.maghrib = row[7] as? Int64
                obj.isha = row[8] as? Int64
                obj.trackDate = row[9] as? String
                
                arrSurahList.append(obj)
                                
            }
            
        } catch {
            //handle error
            print(error)
        }
                
        return arrSurahList
    }
    
    class func getAllPrayerDates() -> [PrayerTrackModel] {
           
           //let dbPath = getValueForKey(keyValue: "DatabasePath")
           
           var arrSurahList = [PrayerTrackModel]()
           do {
               
               let databaseFileName = "Allah_names.sqlite"
               let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
               
               //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
               let db = try Connection(path)
               
               //let updatedDate = "\'" + trackerDate + "\'"
               //print(updatedDate)
               print("Path---> \(path)")
               //.replacingOccurrences(of: "\"", with: "\'")
               let query = "SELECT * FROM PrayerTracker"
               
               print(query)
               
               for row in try db.prepare(query) {
                   let obj = PrayerTrackModel()
                   
                   obj.trackId = row[0] as? Int64
                   obj.tahajjud = row[1] as? Int64
                   obj.fajr = row[2] as? Int64
                   obj.ishraq = row[3] as? Int64
                   obj.chasht = row[4] as? Int64
                   obj.dhuhr = row[5] as? Int64
                   obj.asr = row[6] as? Int64
                   obj.maghrib = row[7] as? Int64
                   obj.isha = row[8] as? Int64
                   obj.trackDate = row[9] as? String
                   
                   arrSurahList.append(obj)
                                   
               }
               
           } catch {
               //handle error
               print(error)
           }
                   
           return arrSurahList
       }
    
    //---------------Rabbana queries-------------------------------------
    
    class func CheckRabbanaExist(r_id: String) -> Bool {
        
        var recordExist: Bool = false
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let query = "SELECT * FROM Rabbana_bookmark where rabbana_id = '\(r_id)' "
            //print("query")
            
            for row in try db.prepare(query) {
                
                if row.count > 0{
                    recordExist = true
                }
                else{
                    recordExist = false
                }
            }
            
        } catch {
            //handle error
            print(error)
        }
        
        return recordExist
    }
    
    class func DeleteRabbana(r_id: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let query = "Delete from Rabbana_bookmark where rabbana_id = '\(r_id)' "
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }
        
    }
    
    class func InsertRabbana(r_Id: String, r_arabic: String, r_Transliteration: String, r_translation: String, r_audio: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            print("Path---> \(path)")
            
            let query = "Insert into Rabbana_bookmark (rabbana_id, rabbana_arabic, rabbana_transliteration, rabbana_translation, rabbana_audio) values (\(r_Id), \(r_arabic), \(r_Transliteration), \(r_translation),  \(r_audio))"
            
            print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }
        
    }
    
// Mark:- Create Insert language table
    
    class func CreateInsertLanguageTable(querry: String) {
        
        //let dbPath = getValueForKey(keyValue: "DatabasePath")
        
        do {
            
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            //print("Path---> \(path)")
            
            let query = querry
            
            //print(query)
            
            do{
                try db.run(query)
            } catch{
                print(error)
            }
            
        } catch {
            //handle error
            print(error)
        }
        
    }
    
    class func GetAllRabbanaBookmark() -> [Rabbana]  {
        
        var arrHisnul = [Rabbana]()
        do {
           let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM Rabbana_bookmark"
            print(query)
            
            for row in try db.prepare(query) {
                let obj = Rabbana()
                
                obj.RId = row[0] as? String
                obj.RabbanaArabic = row[1] as? String
                obj.RabbanaTransliteration = row[2] as? String
                obj.RabbanaTranslation = row[3] as? String
                obj.RabbanaAudio = row[3] as? String
                
                arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
    }
    
    class func getRabbanaDetailForBookmark(id: String) -> Rabbana {
        //var arrHisnul = [HisnulDetail]()
        let arrHisnul = Rabbana()
        do {
            let path = Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            
            let query = "SELECT * FROM rabbana Where Id = \(id) "
            
            print(query)
            
            for row in try db.prepare(query) {
               // let obj = HisnulDetail()
                
                //obj.Id = row[0] as? Int64
                if let hid = row[0] as? Int64{
                    arrHisnul.RId = String (hid)
                }
                
                arrHisnul.RabbanaArabic = row[1] as? String
                arrHisnul.RabbanaTransliteration = row[2] as? String
                arrHisnul.RabbanaTranslation = row[3] as? String
                arrHisnul.RabbanaAudio = row[4] as? String
                
                //arrHisnul.append(obj)
                
                
                //print("id: \(String(describing: row[0])), name: \(String(describing: row[1]))")
            }
        } catch {
            //handle error
            print(error)
        }
                
        return arrHisnul
        
        
    }
    
    class func tableExist(tableName: String) -> Bool {
        
        var tableExist: Bool = false
        
        do {
            let databaseFileName = "Allah_names.sqlite"
            let path = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"
            
            //let path = dbPath//Bundle.main.path(forResource: "Allah_names", ofType: "sqlite")!
            let db = try Connection(path)
            
            let count:Int64 = try db.scalar(
                "SELECT EXISTS(SELECT name FROM sqlite_master WHERE name = ?)", tableName
                ) as! Int64
            if count>0{
                tableExist = true
            }
            else{
                tableExist = false
            }
        } catch {
            //handle error
            print(error)
        }
        
        return tableExist
        
    }
    
}
