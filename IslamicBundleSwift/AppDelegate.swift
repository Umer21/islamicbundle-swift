//
//  AppDelegate.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/25/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import UserNotifications
import AVFoundation
import Alamofire
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var avPlayer: AVAudioPlayer?
    
    /* func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Notification Click")
    }*/
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("Local Notification")
        
    }
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey(kGoogleMapsServicesAPIKey)
        GMSPlacesClient.provideAPIKey(kGoogleMapsServicesAPIKey)
        self.SetupPushNotification(application: application)
        
        //printMyFonts()
        //let tmpDirURL = FileManager.default.temporaryDirectory
        //print("Temp Dir = \(tmpDirURL)")
        
        
        return true
    }
    
    // Setup appdelegate for push/local notifications
    func SetupPushNotification(application: UIApplication) -> () {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge])
        {(granted,error) in
            if granted{
                
                /*let cc = UNUserNotificationCenter.current()
                var nCount: Int = 0
                print("-----------------------------------------------")
                cc.getPendingNotificationRequests { (notifications) in
                    print("notification-count----before--> \(notifications.count)")
                    nCount = notifications.count
                    /*for item in notifications {
                      print(item.content)
                    }*/
                }*/
    
                DispatchQueue.main.asyncAfter(deadline: .now() +  5.0, execute: {
                    application.registerForRemoteNotifications()
                    
                    /*if nCount == 0
                    {
                        let prayerCount = getNumberOfPrayers()
                        print("prayer-count----> \(prayerCount)")
                        print("prayer-count---->------->")
                        setInitialPrayersAlarms()
                        setInitialAzaanAudios()
                        getAdhanTimingsExp()
                        
                        let nCount1 = getLocalNotificationCount()
                        print("notification-count----after--> \(nCount1)")
                    }*/
                    
                })
                
            } else {
                print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("didReceiveRemoteNotification")
       // resetPrayerNotificationsOnSettingsChange()
    }
    
    // Method: 1 -  Will register app on apns to receieve token
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        print("APNs device token: \(deviceTokenString)")
        setDefaultValue(keyValue: "deviceToken", valueIs: deviceTokenString)
        registerTokenOnServer(dToken: deviceTokenString)
        
        //print("Device Token----->\(getValueForKey(keyValue: "deviceToken"))")
    }
    
    // Method: 2 - Failed registration. Explain why.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications: \(error.localizedDescription)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func registerTokenOnServer(dToken : String) {
        guard let deviceID = UIDevice.current.identifierForVendor?.uuidString else {
           return
        }
        print(deviceID)
        var param = Parameters()
        param = [
            "AuthCode" : "YWRtaW46MTIb==",
            "Token" : dToken,
            "DeviceType" : "IOS",
            "DeviceId" : deviceID,
            "DeviceVersion" : "Version 1.0"]
        
        print("Params-Reg\(deviceID)")
        
        let url = "http://40.122.109.138:8089/api/IslamicBundleApi/AddDeviceToken"
        AF.request(url, method: .post, parameters: param, encoding: URLEncoding.default)
        .responseJSON { response in
            print("Device-Token-Reg: \(response)")
        }
    }


}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Handle push from foreground")
        print("\(notification.request.content.userInfo)")
        
        let userInfo = notification.request.content.userInfo
        self.extracDataFromNotification(userInfo: userInfo, isFromBackground: false)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("\(response.notification.request.content.userInfo)")
        let userInfo = response.notification.request.content.userInfo
        self.extracDataFromNotification(userInfo: userInfo, isFromBackground: true)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func extracDataFromNotification(userInfo: [AnyHashable: Any], isFromBackground: Bool) {
        //let info : NSDictionary = userInfo as NSDictionary
        
        var alertMsg = String()
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject> {
            if let msg = info["alert"] as? String {
                alertMsg = msg
                print("Alert Msg = \(alertMsg)")
            }
        }
        
        let driverID = userInfo[AnyHashable("DriverId")] as? String ?? ""
        let ExpiredDate = userInfo[AnyHashable("ExpiredDate")] as? String ?? ""
        let Rideid = userInfo[AnyHashable("Rideid")] as? Int ?? 0
        let NotificationDate = userInfo[AnyHashable("NotificationDate")] as? String ?? ""
        let TabName = userInfo[AnyHashable("TabName")] as? String ?? ""
        
        print("driverID: \(driverID)")
        print("ExpiredDate: \(ExpiredDate)")
        print("Rideid: \(Rideid)")
        print("NotificationDate: \(NotificationDate)")
        print("TabName: \(TabName)")
        
        
        if isFromBackground {
            //NotificationCenter.default.post(name: GotoDetailScreenNotification.DetailScreenNotification, object: nil)
        }else{
            //Forground notification handling
            print("Foreground Notifications Handling")
            
            //resetPrayerNotificationsOnSettingsChange()
            
        }
        
    }
    
}
