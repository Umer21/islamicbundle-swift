//
//  Protocol.swift
//  SeeNow
//
//  Created by Nouman Gul Junejo on 7/31/18.
//  Copyright © 2018 APP. All rights reserved.
//

import UIKit
//import TTGSnackbar

protocol ReusableView: class {}

extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
    
    static var NibName: String {
        return String(describing: self)
    }
}

protocol Swipeable {}
extension Swipeable where Self: UIViewController {
    func addSwipeRightGesture() {
        let swipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(Self.popViewController))
        swipe.direction = .right
        view.addGestureRecognizer(swipe)
    }
}

extension UIViewController {
    @objc func popViewController() {
        self.navigationController?.popViewController(animated: true)
    }
}



/*
var snackbarError = TTGSnackbar()
protocol ShowsLoader {}
extension ShowsLoader where Self: UIViewController {
    
    func showErrorLoaderWithAction(msg: String, button: String, completionHandler:@escaping () -> ()){
        snackbarError = TTGSnackbar(message: msg,
                                    duration: .long,
                                    actionText: button,
                                    actionBlock: { (Completion) in
            completionHandler()
        })
        
        snackbarError.animationType = .slideFromBottomToTop
        snackbarError.backgroundColor = .white
        snackbarError.messageTextColor = .black
        snackbarError.actionTextColor = .black
        snackbarError.show()
    }
    
    func hideErrorLoader() {
        snackbarError.dismiss()
    }
}


var snackbar = TTGSnackbar()
protocol ShowsAlert {}
extension ShowsAlert where Self: UIViewController {
    
    func showLoader(message:String){
        snackbar = TTGSnackbar(message: message,
                               duration: .long
        )
        snackbar.backgroundColor = .white
        snackbar.messageTextColor = .black
        snackbar.animationType = .slideFromBottomToTop
        snackbar.message = message
        snackbar.show()
        
    }
    
    func hideLoader(){
        snackbar.dismiss()
    }
    
}
*/
