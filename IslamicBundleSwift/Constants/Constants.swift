//
//  Constants.swift
//  Sofiez
//
//  Created by Umar.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Adhan
import UserNotifications
import Toast_Swift
import CoreLocation
import GoogleMaps

//MARK :- Token
let kdeviceToken = "deviceToken"
let kTYpEIOS = "ios"
let kTouchID = "com.BioMetric"

//MARK :- User Type
let kFacebookUser = "Facebook"
let kNativeUser = "Email"

//MARK :- Regex Strings
let RegexAllowOneDecimal = "^[0-9]*((\\.)[0-9]{0,1})?$"

//MARK :- Base Url
let baseUrl = "http://40.122.109.138:8082/api/SofiezApi/"
let authCode = "SYOWFRItZaW46MTIzNA=="

// MARK: - Google MAPS API
let kGoogleMapsServicesAPIKey = "AIzaSyAslD6WmmUKu08HHMW_exsUI9ZpKJ5iOFY"
//"AIzaSyA-NzTxSmhzuqEk_gShgk0D0ppt1B6pu5M"

//let kUpdateProfile = "update"
let kCardList = "card/list"
let KAddFavorite = "add/favorite"
let KRemoveFavorite = "remove/favorite"
let kFavoriteCardList = "favorite/list"


// MARK: - Shared Application Object
let appDelet = UIApplication.shared.delegate as! AppDelegate

// MARK: - Google Maps Api Key.
let googleApiKey = ""


// MARK: - Storyboard IDs
let kLocationControllerID = "locationViewController"
let kLoginControllerID = "loginViewController"
let kVerifyPhoneControllerID = "verifyPhoneViewController"
let kTabControllerID = "tabbarController"
let kNavControllerID = "navigationController"
let kHomeControllerID = "homeViewController"
let kForgotPasswordControllerID = "forgotpasswordViewController"
let kNotificationControllerID = "notificationViewController"
let kFriendTransactionsControllerID = "friendTransactionsViewController"
let kTransactionDetailControllerID = "transactionDetailViewController"
let kRejectTransactionControllerID = "rejectTransactionViewcontroller"
let kTakePhoneControllerID = "takePhoneViewController"
let kTouchIDControllerID = "touchIDViewController"
let kScanQRControllerID = "scanQRViewController"
let kMyQRControllerID = "myQRViewController"
let kMyProfileControllerID = "myProfileViewController"
let kFieldstoShowControllerID = "fieldsViewController"
let kFiltersControllerID = "filtersViewController"
let kSettingsConttollerID = "settingsViewController"
let knewTransactionControllerID = "newtransactionViewController"
let kRabbanaDetailControllerID = "rabbanaDetailViewController"
let kQuranSurahListControllerID = "quranSurahListViewController"
let kQuranDetailControllerID = "quranDetailViewController"
let kPrayerSoundControllerID = "prayerSoundViewController"
let kOnboardParentControllerID = "onboardViewController"

let kPrayerMethodControllerID = "prayerMethodViewController"
let kJuristictsControllerID = "juristictViewController"
let kLocationSelectionControllerID = "locationSelectionViewController"

let kSettingPrayerMethodControllerID = "settingPrayerMethodviewController"
let kSettingJuristControllerID = "settingJuristViewController"
let kSettingHighAltitudeControllerID = "settingHighAltitudeViewController"
let kSettingDayLightControllerID = "settingDaylightViewController"
let kSettingQuranControllerID = "settingQuranViewController"
let kSettingAdhanControllerID = "settingAdhanViewController"
let kSettingHijriControllerID = "settingHijriViewController"
let kLiveChannelControllerID = "liveChannelViewController"
let kRelatedAppsControllerID = "relatedAppsViewController"
let kAboutUsControllerID = "aboutUsViewController"
let kContactUsControllerID = "contactUsViewController"
let kPrayerTrackerControllerID = "prayerTrackerViewController"
let kName99ListViewControllerID = "name99ListViewController"
let kName99DetailViewControllerID = "name99DetailViewController"
let kDailyDuaListViewControllerID = "dailyDuaListViewController"
let kDailyDuaDetailViewControllerID = "dailyDuaCategoryDetailControllerID"
let kDailyDuaBookmarkListControllerID = "dailuDuaBookmarkListViewController"
let kDailyDuaBookmarkDetailControllerID = "dailyDuaBookmarkDetailViewController"
let kRabbanaBookmarkListControllerID = "rabbanaBookmarkListViewController"
let kRabbanaBookmarkDetailControllerID = "rabbanaBookmarkDetailViewController"
let kShahadahViewControllerID = "shahadahViewControllerID"
let kHijriCalendarControllerID = "hijriCalendarViewController"
let kTasbeehControllerID = "tasbeehControllerID"
let kPrayerMonthlyControllerID = "prayerMonthlyControllerID"
let kQuranLanguagesControllerID = "quranLanguagesControllerID"
let kFastingTimingsControllerID = "fastingTimingsControllerID"
let kKidsListViewControllerID = "kidsListViewController"
let kKidsDetailViewControllerID = "kidsDetailViewController"
let kZakatViewControllerID = "zakatViewControllerID"

let kGPSPopUp = "gpsPopUp"
let kDepartmentPopUp = "departmentPopUp"


// MARK:- Storyboards

let kMainStoryboard = "Main"
let kLoginStoryboard = "Login"
let kSignUpStoryboard = "Signup"
let kHomeStoryborad = "Home"
let kNotificationStoryboard = "Notification"
let kSettingsStroyboard = "Settings"
let kRabbanaStoryboard = "Rabbana"
let kQuranSurahListStoryboard = "Quran"
let kOnBoardStoryboard = "Onboard"
let kMoreScreenStoryboard = "MoreScreens"

//MARK:- Transaction Status
let kTransactionRejected = "Transaction Rejected"
let kDisputedPayment = "Disputed Payment"

//MARK:- Theme Colors
let kPurpleColor = UIColor(red: 70/255, green: 45/255, blue: 96/255, alpha: 1.0)
let klightGray = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1.0)
let kTrasactionStatusGreenColor = UIColor(red: 20/255, green: 108/255, blue: 33/255, alpha: 1.0)
let kTrasactionStatusRedColor = UIColor(red: 185/255, green: 29/255, blue: 35/255, alpha: 1.0)
let kTransactionGreenColor = UIColor(red: 0/255, green: 129/255, blue: 141/255, alpha: 1.0)
let kThemeColorGreen = UIColor(red: 150/255, green: 245/255, blue: 225/255, alpha: 1.0)
let kThemeDarkGreen = UIColor(red: 23/255, green: 107/255, blue: 112/255, alpha: 1.0)

//MARK: - Others
func showAlert(title: String, message: String, viewController: UIViewController) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(okAction)
    viewController.present(alertController, animated: true, completion: nil)
}


//MARK:- Notification Identifiers
struct GotoDetailScreenNotification {
    static let DetailScreenNotification = NSNotification.Name("DetailScreenNotification")
}

struct ReloadScreenNotification {
    static let ReloadScreenNotification = NSNotification.Name("ReloadScreenNotification")
}

//MARK:- Enums

enum FilterType: String {
    case Default = "0"
    case ByStatus = "1"
    case ByAmount = "2"
    case ByName = "3"
    case ByCreatedDate = "4"
}


enum ActionType: Int {
    case AcceptPayment = 1
    case AcceptSettlement = 2
    case AcceptWaiver = 3
    case Acknowledge = 4
    case CustomMessage = 5
    case IhavePaid = 6
    case Pay = 7
    case Ping = 8
    case RejectReview = 9
    case RejectSettlement = 10
    case RejectTransaction = 11
    case RejectWaiver = 12
    case RequestWaiver = 13
    case ReviewTransaction = 14
    case SettlementDone = 15
    case TransactionHistory = 16
    case EditTransaction = 17
    case RejectEditReview = 18
}

func getActionString(Id: Int) -> String {
    switch Id {
    case 1:
        return "Accept Payment"
    case 2:
        return "Accept Settlement"
    case 3:
        return "Accept Waiver"
    case 4:
        return "Acknowledge"
    case 5:
        return "Custom Message"
    case 6:
        return "I have Paid"
    case 7:
        return "Pay"
    case 8:
        return "Ping"
    case 9:
        return "Reject Review"
    case 10:
        return "Reject Settlement"
    case 11:
        return "Reject Transaction"
    case 12:
        return "Reject Waiver"
    case 13:
        return "Request Waiver"
    case 14:
        return "Review Transaction"
    case 15:
        return "Settlement Done"
    case 16:
        return "Transaction History"
    default:
        return ""
    }
}

func printMyFonts() {
    print("--------- Available Font names ----------")
    for fontFamilyName in UIFont.familyNames{
        for fontName in UIFont.fontNames(forFamilyName: fontFamilyName){
            print("Family: \(fontFamilyName)     Font: \(fontName)")
        }
    }
}


//MARK: Base64 image conversions

func convertBase64ToImage(imageString: String) -> UIImage {
    let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
    return UIImage(data: imageData)!
}

func convertImageToBase64(image: UIImage) -> String {
    let imageData = image.pngData()!
    return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
}


func isValidateEmail(email:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: email)
}

func isValidData(_ input:String,regexPattern : String) -> Bool {
    do {
        if regexPattern != ""{
            let regex = try NSRegularExpression(pattern: regexPattern, options: NSRegularExpression.Options.caseInsensitive)
            if regex.firstMatch(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count)) != nil {
                return true
            }
        }else{
            return true
        }
    } catch {
        // regex was bad!
    }
    return false
}

/*
func showToast(animated: Bool = true, viewControl: UIViewController, titleMsg: String, msgTitle: String) {
    viewControl.view.makeToast(msgTitle, duration: 3.0, position: .bottom)
}*/

//MARK:- User Defaults Methods
/*
func setUser(userObj: UserObj?) {
    //saving user
    let encodeData: Data = NSKeyedArchiver.archivedData(withRootObject: userObj)
    UserDefaults.standard.set(encodeData, forKey: "User")
    UserDefaults.standard.synchronize()
}

func getUser() -> UserObj? {
    //retriving user
    if let decodeData = UserDefaults.standard.object(forKey: "User") as? Data {
        let user = NSKeyedUnarchiver.unarchiveObject(with: decodeData) as? UserObj
        return user
    }
    return nil
}*/

func clearTouchIdFromKeychain(){
    setDefaultValue(keyValue: "LastLoginUser", valueIs: "")
    /*
    do {
        try keychain.remove("seceretPassword")
    } catch let error {
        print("error: \(error)")
    }*/
}

func setDefaultValue(keyValue: String, valueIs: String)
{
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    usrdefault.set(valueIs, forKey: keyValue)
    usrdefault.synchronize()
}

func getValueForKey(keyValue: String) -> String
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    UserDefaults.standard.synchronize()
    
    let valueForStr = defaults.value(forKey: keyValue)
    if(valueForStr != nil)
    {
        return valueForStr as! String
    }
    else{
        return ""
    }
    
}

func formatDateTime(date: String) -> String {
    //date input formate
    let dateAsString = date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

    //date output format
    let date = dateFormatter.date(from: dateAsString)
    dateFormatter.dateFormat = "dd MMM yyyy"
    let Date12 = dateFormatter.string(from: date!)
    
    //time output format
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.timeZone = NSTimeZone.local
    
    let time = dateFormatter.string(from: date!)
    
    let comibine = Date12 + " at " + time
    return comibine
}

func getCurrentDate() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let finalDate = formatter.string(from: date)
    return finalDate
}

func formatDateForDisplay1(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "h:mm a"
    if isKeyPresentInUserDefaults(key: "TimeZone"){
        let tz = getValueForKey(keyValue: "TimeZone")
        formatter.timeZone = TimeZone(identifier: tz)!
    }
    else{
        let tz = "Asia/Riyadh"
        formatter.timeZone = TimeZone(identifier: tz)!
    }
    return formatter.string(from: date)
}

func formatDateWithAddingHours(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "h:mm a"
    if isKeyPresentInUserDefaults(key: "TimeZone"){
        let tz = getValueForKey(keyValue: "TimeZone")
        formatter.timeZone = TimeZone(identifier: tz)!
    }
    else{
        let tz = "Asia/Riyadh"
        formatter.timeZone = TimeZone(identifier: tz)!
    }
    
    let calendar = Calendar.current
    let newDate = calendar.date(byAdding: .hour, value: 2, to: date)
    
    return formatter.string(from: newDate!)
}

func formatDateWithAddingMinutes(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "h:mm a"
    if isKeyPresentInUserDefaults(key: "TimeZone"){
        let tz = getValueForKey(keyValue: "TimeZone")
        formatter.timeZone = TimeZone(identifier: tz)!
    }
    else{
        let tz = "Asia/Riyadh"
        formatter.timeZone = TimeZone(identifier: tz)!
    }
    
    let calendar = Calendar.current
    let newDate = calendar.date(byAdding: .minute, value: 10, to: date)
    
    return formatter.string(from: newDate!)
}

func formatStringFToDate(date: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a"
    let date = dateFormatter.date(from: date)
    
    return date!
}

func formatDateForDisplayForAlarm(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd hh:mm a"
    return formatter.string(from: date)
}

func formatDate(date: String) -> String {
    //date input formate
    let dateAsString = date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    
    //date output format
    let date = dateFormatter.date(from: dateAsString)
    dateFormatter.dateFormat = "dd MMM yyyy"
    let Date = dateFormatter.string(from: date!)
    
    return Date
}

func getHijriDate(gregorianDate: String) -> String{
    let dateFormatter = DateFormatter()

    dateFormatter.dateFormat = "dd MMMM yyyy"

    let GregorianDate = dateFormatter.date(from: "\(gregorianDate)")

    let islamic = NSCalendar(identifier: NSCalendar.Identifier.islamicUmmAlQura)

    let components = islamic!.components(NSCalendar.Unit(rawValue: UInt.max), from: GregorianDate!)
    
    let finalDate = "\(components.day ?? 00) \(getMonthName(index: components.month!)) \(components.year ?? 00)"

    //print("gethijriDateFunc--------\(components.day ?? 00) - \(getMonthName(index: components.month!)) - \(components.year ?? 00)")
    
    return finalDate
    //resultlable.text = "\(components!.year) - \(components!.month) - \(components!.day)"
}

func getHijriDateForMonthScreen(gregorianDate: String) -> String{
    let dateFormatter = DateFormatter()

    dateFormatter.dateFormat = "MMMM yyyy"

    let GregorianDate = dateFormatter.date(from: "\(gregorianDate)")

    let islamic = NSCalendar(identifier: NSCalendar.Identifier.islamicUmmAlQura)

    let components = islamic!.components(NSCalendar.Unit(rawValue: UInt.max), from: GregorianDate!)
    
    let finalDate = "\(getMonthName(index: components.month!)) \(components.year ?? 00)"

    //print("gethijriDateFunc--------\(getMonthName(index: components.month!)) - \(components.year ?? 00)")
    
    return finalDate
    //resultlable.text = "\(components!.year) - \(components!.month) - \(components!.day)"
}

func getMonthName(index: Int) -> String{
    switch index {
    case 1:
        return "Muharram"
        
    case 2:
        return "Safar"
        
    case 3:
        return "Rabi' al-awwal"
        
    case 4:
        return "Rabi' al-thani"
        
    case 5:
        return "Jumada al-awwal"
        
    case 6:
        return "Jumada al-Thani"
        
    case 7:
        return "Rajab"
        
    case 8:
        return "Sha'aban"
        
    case 9:
        return "Ramadan"
        
    case 10:
        return "Shawwal"
        
    case 11:
        return "Dhu al-Qi'dah"
        
    case 12:
        return "Dhu al-Hijjah"
        
    default:
        return ""
    }
}

func isKeyPresentInUserDefaults(key: String) -> Bool {
    return UserDefaults.standard.object(forKey: key) != nil
}

/*func findDateDiff(time1Str: String, time2Str: String) -> String {
    let timeformatter = DateFormatter()
    timeformatter.dateFormat = "hh:mm a"

    guard let time1 = timeformatter.date(from: time1Str),
        let time2 = timeformatter.date(from: time2Str) else { return "" }

    //You can directly use from here if you have two dates

    let interval = time2.timeIntervalSince(time1)
    let hour = interval / 3600;
    let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
    let intervalInt = Int(interval)
    return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes"
}*/



func getFormattedDate(strDate: String , currentFomat:String, expectedFromat: String) -> String{
    let dateFormatterGet = DateFormatter()
    dateFormatterGet.dateFormat = currentFomat
    if isKeyPresentInUserDefaults(key: "TimeZone"){
        let tz = getValueForKey(keyValue: "TimeZone")
        dateFormatterGet.timeZone = TimeZone(identifier: tz)!
    }
    else{
        let tz = "Asia/Riyadh"
        dateFormatterGet.timeZone = TimeZone(identifier: tz)!
    }

    let date : Date = dateFormatterGet.date(from: strDate)!

    dateFormatterGet.dateFormat = expectedFromat
    return dateFormatterGet.string(from: date)
}

func formatTime(date: String) -> String {
    //date input formate
    let dateAsString = date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    //date output format only time
    let date = dateFormatter.date(from: dateAsString)
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.timeZone = NSTimeZone.local
    let Time12 = dateFormatter.string(from: date!)
    return Time12
}

func formatDateForDisplay(date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd-MM-yyyy"
    return formatter.string(from: date)
}

func formatTimeForDisplay(date: Date) -> String {
    let formatter = DateFormatter()
    //formatter.timeZone = Locale(identifier: "en_US")
    formatter.dateFormat = "h:mm a"//HH:mm
    return formatter.string(from: date)
}

func UIColorFromRGB(rgbValue: UInt) -> UIColor {
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func showToast(animated: Bool = true, viewControl: UIViewController, titleMsg: String, msgTitle: String) {
    viewControl.view.makeToast(msgTitle, duration: 3.0, position: .bottom)
}

func setInitialAzaanAudios()
{
    setDefaultValue(keyValue: "Sound-Fajr" , valueIs: "2")
    setDefaultValue(keyValue: "Sound-Sunrise" , valueIs: "1")
    setDefaultValue(keyValue: "Sound-Dhuhr" , valueIs: "2")
    setDefaultValue(keyValue: "Sound-Asr" , valueIs: "2")
    setDefaultValue(keyValue: "Sound-Maghrib" , valueIs: "2")
    setDefaultValue(keyValue: "Sound-Isha" , valueIs: "2")
}

func setInitialPrayersAlarms() {
    
    setDefaultValue(keyValue: "Fajr", valueIs: "1")
    setDefaultValue(keyValue: "Sunrise", valueIs: "0")
    setDefaultValue(keyValue: "Dhuhr", valueIs: "1")
    setDefaultValue(keyValue: "Asr", valueIs: "1")
    setDefaultValue(keyValue: "Maghrib", valueIs: "1")
    setDefaultValue(keyValue: "Isha", valueIs: "1")
    
    setDefaultValue(keyValue: "SelectedLanguageIndex", valueIs: "0")
    
}

//Mark:- method to get Prayer Alarm Status
func getNamazAlarmValue(index:Int, namaz:String) -> String
{
    var alarm: String = ""
    switch index {
    case 0:
        alarm = getValueForKey(keyValue: namaz)
        
        break
    case 1:
        alarm = getValueForKey(keyValue: namaz)
        
        break
    case 2:
        alarm = getValueForKey(keyValue: namaz)
        
        break
    case 3:
        alarm = getValueForKey(keyValue: namaz)
       
        break
    case 4:
        alarm = getValueForKey(keyValue: namaz)
        
        break
    case 5:
        alarm = getValueForKey(keyValue: namaz)
        
        break
        
    default:
        print("0")
    }
    
    return alarm
}

func getChangedPrayerStatus(index:Int, namaz:[PrayerTrackModel]) -> [PrayerTrackModel]{
    
    namaz[0].trackId = 1
    namaz[0].trackDate = namaz[0].trackDate
    switch index {
    case 0:
        if namaz[0].tahajjud == 1 {
            namaz[0].tahajjud = 0
        }
        else{
            namaz[0].tahajjud = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].chasht = namaz[0].chasht
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].asr = namaz[0].asr
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].isha = namaz[0].isha
        
        break
    case 1:
        if namaz[0].fajr == 1 {
            namaz[0].fajr = 0
        }
        else{
            namaz[0].fajr = 1
        }
        
        namaz[0].tahajjud = namaz[0].tahajjud
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].chasht = namaz[0].chasht
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].asr = namaz[0].asr
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].isha = namaz[0].isha
        
        break
    case 2:
        if namaz[0].ishraq == 1 {
            namaz[0].ishraq = 0
        }
        else{
            namaz[0].ishraq = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].tahajjud = namaz[0].tahajjud
        namaz[0].chasht = namaz[0].chasht
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].asr = namaz[0].asr
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].isha = namaz[0].isha
        
        break
    case 3:
        if namaz[0].chasht == 1 {
            namaz[0].chasht = 0
        }
        else{
            namaz[0].chasht = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].tahajjud = namaz[0].tahajjud
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].asr = namaz[0].asr
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].isha = namaz[0].isha
       
        break
    case 4:
        if namaz[0].dhuhr == 1 {
            namaz[0].dhuhr = 0
        }
        else{
            namaz[0].dhuhr = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].chasht = namaz[0].chasht
        namaz[0].tahajjud = namaz[0].tahajjud
        namaz[0].asr = namaz[0].asr
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].isha = namaz[0].isha
        
        break
    case 5:
        if namaz[0].asr == 1 {
            namaz[0].asr = 0
        }
        else{
            namaz[0].asr = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].chasht = namaz[0].chasht
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].tahajjud = namaz[0].tahajjud
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].isha = namaz[0].isha
        
        break
    case 6:
        if namaz[0].maghrib == 1 {
            namaz[0].maghrib = 0
        }
        else{
            namaz[0].maghrib = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].chasht = namaz[0].chasht
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].asr = namaz[0].asr
        namaz[0].tahajjud = namaz[0].tahajjud
        namaz[0].isha = namaz[0].isha
        
        break
    case 7:
        if namaz[0].isha == 1 {
            namaz[0].isha = 0
        }
        else{
            namaz[0].isha = 1
        }
        
        namaz[0].fajr = namaz[0].fajr
        namaz[0].ishraq = namaz[0].ishraq
        namaz[0].chasht = namaz[0].chasht
        namaz[0].dhuhr = namaz[0].dhuhr
        namaz[0].asr = namaz[0].asr
        namaz[0].maghrib = namaz[0].maghrib
        namaz[0].tahajjud = namaz[0].tahajjud
        
        break
        
    default:
        print("0")
    }
    
    return namaz
}

func getPrayerStatusvalues(index:Int, namaz:[PrayerTrackModel]) -> Int64
{
    var alarm: Int64 = 0
    switch index {
    case 0:
        alarm = namaz[0].tahajjud!
        
        break
    case 1:
        alarm = namaz[0].fajr!
        
        break
    case 2:
        alarm = namaz[0].ishraq!
        
        break
    case 3:
        alarm = namaz[0].chasht!
       
        break
    case 4:
        alarm = namaz[0].dhuhr!
        
        break
    case 5:
        alarm = namaz[0].asr!
        
        break
    case 6:
        alarm = namaz[0].maghrib!
        
        break
    case 7:
        alarm = namaz[0].isha!
        
        break
        
    default:
        print("0")
    }
    
    return alarm
}

func setNamazAlarmValue(index:Int, namaz:String, value: String)
{
    switch index {
    case 0:
        setDefaultValue(keyValue: namaz, valueIs: value)
        
        break
    case 1:
        setDefaultValue(keyValue: namaz, valueIs: value)
        
        break
    case 2:
        setDefaultValue(keyValue: namaz, valueIs: value)
        
        break
    case 3:
        setDefaultValue(keyValue: namaz, valueIs: value)
       
        break
    case 4:
       setDefaultValue(keyValue: namaz, valueIs: value)
        
        break
    case 5:
        setDefaultValue(keyValue: namaz, valueIs: value)
        
        break
        
    default:
        print("0")
    }
    
}

func setTimeZone(lat: Double, lng: Double){
    let location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
    let timeZone = TimezoneMapper.latLngToTimezone(location)
    
    if let tz = timeZone {
        
        print("---before-splitted--\(String(describing: tz))")
        
        let s = String(describing: tz)
        let splittedTz = s.split(separator: " ")
        
        setDefaultValue(keyValue: "TimeZone", valueIs: String(splittedTz[0]))
        print("--after--splitted---\(splittedTz[0])")
        
        if splittedTz[0].contains("Asia"){
            setDefaultValue(keyValue: "PrayerMethod", valueIs: "0")
        }
        else if splittedTz[0].contains("America"){
            setDefaultValue(keyValue: "PrayerMethod", valueIs: "1")
        }
        else if splittedTz[0].contains("Europe"){
            setDefaultValue(keyValue: "PrayerMethod", valueIs: "2")
        }
        else if splittedTz[0].contains("Africa"){
            setDefaultValue(keyValue: "PrayerMethod", valueIs: "4")
        }
        else{
            setDefaultValue(keyValue: "PrayerMethod", valueIs: "1")
        }
    }
}

func getCurrentPrayerDetail() -> [NamazComponent]  {
    let cal = Calendar(identifier: Calendar.Identifier.gregorian)
    //print("AAA-cal--- \(cal)")
    var date = cal.dateComponents([.year, .month, .day], from: Date())
    
    var NamazItems = [NamazComponent]()
    
    var lat: Double?
    var lng: Double?
    
    if isKeyPresentInUserDefaults(key: "Latitude"){
        //print("Exist")
        lat = Double (getValueForKey(keyValue: "Latitude"))
        lng = Double (getValueForKey(keyValue: "Longitude"))
    }
    else{
        //print("Not Exist")
        lat = 21.3891
        lng = 39.8579
    }
    
    
    
   // let coordinates = Coordinates(latitude: lat!, longitude: lng!)
   // var params = CalculationMethod.karachi.params
   // params.madhab = .hanafi
    
    //-------------------------------
    let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
    //print("AAA-PrayerMethod---> \(prayerMethod)")
    let jurists = getValueForKey(keyValue: "Jurists")
    //print("AAA-Jurists---> \(jurists)")
    
    let coordinates = Coordinates(latitude: lat!, longitude: lng!)
    var params = CalculationMethod.karachi.params
    
    
    
    switch prayerMethod {
    case "0":
        params = CalculationMethod.karachi.params
        break
    case "1":
        params = CalculationMethod.northAmerica.params
        break
    case "2":
        params = CalculationMethod.muslimWorldLeague.params
        break
    case "3":
        params = CalculationMethod.ummAlQura.params
        break
    case "4":
        params = CalculationMethod.egyptian.params
        break
    case "5":
        params = CalculationMethod.tehran.params
        break
    case "6":
        params = CalculationMethod.other.params
        break
        
    default:
        params = CalculationMethod.karachi.params
        break
    }
    
    switch jurists {
    case "0":
        params.madhab = .shafi
        break
    case "1":
        params.madhab = .hanafi
        break
    
    default:
        params.madhab = .shafi
        break
    }
    //-------------------------------
    
    var prayerTimes = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params)
    
    let current = prayerTimes!.currentPrayer()
    //print("AAA-current \(current!)")
    
    if current == Prayer.isha {
        let today = Date()
        let modifiedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
        date = cal.dateComponents([.year, .month, .day], from: modifiedDate)
        //print("AAA-date-after \(date)")
        prayerTimes = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params)
        
        let next = prayerTimes!.nextPrayer()
        //print("AAA-next \(next!)")
        let countdown = prayerTimes!.time(for: next!)
        //print("AAA-countdown \(countdown)")
        
        let dateString = countdown.string(format: "yyyy-MM-dd HH:mm:ssZ")
        let dateFormatForNamaz = getFormattedDate(strDate: dateString, currentFomat: "yyyy-MM-dd HH:mm:ssZ", expectedFromat: "hh:mm a")
        
        let temp = NamazComponent()
        temp.NextNamazName = next
        temp.NextNamazTime = dateFormatForNamaz
        NamazItems.append(temp)
    }
    else {
        let next = prayerTimes!.nextPrayer()
        //print("AAA-next \(next!)")
        let countdown = prayerTimes!.time(for: next!)
        //print("AAA-countdown \(countdown)")
        
        //Ramzan Work for Iftar Comming time
        let magrib = prayerTimes?.time(for: .maghrib)
        let dateStringMagrib = magrib!.string(format: "yyyy-MM-dd HH:mm:ssZ")
        let iftarTime = getFormattedDate(strDate: dateStringMagrib, currentFomat: "yyyy-MM-dd HH:mm:ssZ", expectedFromat: "hh:mm a")
        setDefaultValue(keyValue: "ComingIftarTime", valueIs: iftarTime)
        
        let fajar = prayerTimes?.time(for: .fajr)
        let dateStringfajar = fajar!.string(format: "yyyy-MM-dd HH:mm:ssZ")
        let fajarTime = getFormattedDate(strDate: dateStringfajar, currentFomat: "yyyy-MM-dd HH:mm:ssZ", expectedFromat: "hh:mm a")
        setDefaultValue(keyValue: "ComingFajarTime", valueIs: fajarTime)
        
        
        let dateString = countdown.string(format: "yyyy-MM-dd HH:mm:ssZ")
        let dateFormatForNamaz = getFormattedDate(strDate: dateString, currentFomat: "yyyy-MM-dd HH:mm:ssZ", expectedFromat: "hh:mm a")
        
        let temp = NamazComponent()
        temp.NextNamazName = next
        temp.NextNamazTime = dateFormatForNamaz
        NamazItems.append(temp)
    }
    
    //print(prayerTimes)
    //let current = prayerTimes!.currentPrayer()
    
    /*let next = prayerTimes!.nextPrayer()
    print("AAA-Next \(next)")
    let countdown = prayerTimes!.time(for: next!)
    print("AAA-countdown \(countdown)")*/
    
    //print("Current-Namaz--->\(current ?? Prayer.dhuhr)")
    //print("next-Namaz--->\(next ?? Prayer.dhuhr)")
    //print("countdown-Namaz--->\(countdown)")
    //print("countdown-Namaz-String-->\(countdown.string(format: "2020-04-13 12:05:00 +0000"))")
    
   /* let dateString = countdown.string(format: "yyyy-MM-dd HH:mm:ssZ")
    let dateFormatForNamaz = getFormattedDate(strDate: dateString, currentFomat: "yyyy-MM-dd HH:mm:ssZ", expectedFromat: "hh:mm a")*/
    
    // print("CheckDateFormatNamaz---->\(dateFormatForNamaz)")
    
    //let difference = Date().timeIntervalSince(countdown)
    //let dateDiff = findDateDiff(time1Str: "09:54 AM", time2Str: "12:59 PM")
    //print(difference.stringFromTimeInterval())
    
    //counter = difference.stringFromTimeInterval()
    
    /*Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)*/
    //var obj:AnyObject = UIButton()
    
    /*let temp = NamazComponent()
    temp.NextNamazName = next
    temp.NextNamazTime = dateFormatForNamaz
    NamazItems.append(temp)*/
    
    return NamazItems
}

func getLocalNotificationCount() -> Int{
    let cc = UNUserNotificationCenter.current()
    var nCount: Int = 0
    print("-----------------------------------------------")
    cc.getPendingNotificationRequests { (notifications) in
        print("Count----------->>>: \(notifications.count)")
        nCount = notifications.count
        /*for item in notifications {
          print(item.content)
        }*/
    }
    
    return nCount
}

func setAdhanTimingsNotifications()
{
    let alarmStatusFajr = getNamazAlarmValue(index: 0, namaz: "Fajr")
    let alarmStatusSunrise = getNamazAlarmValue(index: 1, namaz: "Sunrise")
    let alarmStatusDhuhr = getNamazAlarmValue(index: 2, namaz: "Dhuhr")
    let alarmStatusAsr = getNamazAlarmValue(index: 3, namaz: "Asr")
    let alarmStatusMaghrib = getNamazAlarmValue(index: 4, namaz: "Maghrib")
    let alarmStatusIsha = getNamazAlarmValue(index: 5, namaz: "Isha")
    
    let prayerCount = getNumberOfPrayers()
    print("prayer-count----> \(prayerCount)")
    
    if prayerCount > 0 {
        
        let finalCount = 64 / prayerCount
        print("final count---> \(finalCount)")
        
        for index in (0...finalCount - 1) {
            print("\(index)..")
            
            let cal = Calendar(identifier: Calendar.Identifier.gregorian)
            let today = Date()
            var modifiedDate = Date()
            var date = DateComponents()
            
            if index == 0
            {
                date = cal.dateComponents([.year, .month, .day], from: today)
            }
            else{
                modifiedDate = Calendar.current.date(byAdding: .day, value: index, to: today)!
                date = cal.dateComponents([.year, .month, .day], from: modifiedDate)
            }
            
            //print("Dates---->  \(date)")
            
            var lat: Double?
            var lng: Double?
            
            if isKeyPresentInUserDefaults(key: "Latitude"){
                //print("Exist")
                lat = Double (getValueForKey(keyValue: "Latitude"))
                lng = Double (getValueForKey(keyValue: "Longitude"))
            }
            else{
                //print("Not Exist")
                lat = 21.3891
                lng = 39.8579
            }
            
            let prayerMethod = getValueForKey(keyValue: "PrayerMethod")
            //print("PrayerMethod---> \(prayerMethod)")
            let jurists = getValueForKey(keyValue: "Jurists")
            //print("Jurists---> \(jurists)")
            
            let coordinates = Coordinates(latitude: lat!, longitude: lng!)
            var params = CalculationMethod.karachi.params
            
            switch prayerMethod {
            case "0":
                params = CalculationMethod.karachi.params
                break
            case "1":
                params = CalculationMethod.northAmerica.params
                break
            case "2":
                params = CalculationMethod.muslimWorldLeague.params
                break
            case "3":
                params = CalculationMethod.ummAlQura.params
                break
            case "4":
                params = CalculationMethod.egyptian.params
                break
            case "5":
                params = CalculationMethod.tehran.params
                break
            case "6":
                params = CalculationMethod.other.params
                break
                
            default:
                params = CalculationMethod.karachi.params
                break
            }
            
            
            
            switch jurists {
            case "0":
                params.madhab = .shafi
                break
            case "1":
                params.madhab = .hanafi
                break
                
                
            default:
                params.madhab = .shafi
                break
            }
            
            
            if let prayers = PrayerTimes(coordinates: coordinates, date: date, calculationParameters: params) {
                let formatter = DateFormatter()
                formatter.timeStyle = .medium
                formatter.dateFormat = "yyyy-MM-dd hh:mm a"
                if isKeyPresentInUserDefaults(key: "TimeZone"){
                    let tz = getValueForKey(keyValue: "TimeZone")
                    formatter.timeZone = TimeZone(identifier: tz)!
                }
                else{
                    let tz = "Asia/Riyadh"
                    formatter.timeZone = TimeZone(identifier: tz)!
                }
                
                let datetimeFajr = formatter.date(from: formatter.string(from: prayers.fajr))//formatter.date(from: formatDateForDisplayForAlarm(date: prayers.fajr))
                let componentsFajr = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datetimeFajr!)
                //print("----------------Fajr-----------------------------------------")
                //print("PrayersTime\(prayers.fajr)")
                //print("String-Format-Fajr-->\(formatter.string(from: prayers.fajr))")
                //print("String-to-date-format--->\(datetimeFajr!)")
                //print("Date-to-dateComponent---->\(componentsFajr)")
                //print("Date-to-dateComponent-H:M->\(componentsFajr.hour)--\(componentsFajr.minute)")
                
                
                if alarmStatusFajr == "1"
                {
                    setLocalNotifications(prayerName: "Fajr", hour: componentsFajr.hour ?? 00, minute: componentsFajr.minute ?? 00,indexx: index, dateComponent: componentsFajr)
                }
                
                let datetimeSunrise = formatter.date(from: formatter.string(from: prayers.sunrise))
                let componentsSunrise = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datetimeSunrise!)
                //print("----------------Sunrise-----------------------------------------")
                //print("PrayersTime\(prayers.sunrise)")
                //print("String-Format-Sunrise-->\(formatter.string(from: prayers.sunrise))")
                //print("String-to-date-format--->\(datetimeSunrise!)")
                //print("Date-to-dateComponent---->\(componentsSunrise)")
                //print("Date-to-dateComponent-H:M->\(componentsSunrise.hour)--\(componentsSunrise.minute)")
                
                if alarmStatusSunrise == "1"
                {
                    setLocalNotifications(prayerName: "Sunrise", hour: componentsSunrise.hour ?? 00, minute: componentsSunrise.minute ?? 00,indexx: index, dateComponent: componentsSunrise)
                }
                
                let datetimeDhuhr = formatter.date(from: formatter.string(from: prayers.dhuhr))
                let componentsDhuhr = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datetimeDhuhr!)
                //print("----------------Dhuhr-----------------------------------------")
                //print("PrayersTime\(prayers.dhuhr)")
                //print("String-Format-Dhuhr-->\(formatter.string(from: prayers.dhuhr))")
                //print("String-to-date-format--->\(datetimeDhuhr!)")
                //print("Date-to-dateComponent---->\(componentsDhuhr)")
                //print("Date-to-dateComponent-H:M->\(componentsDhuhr.hour)--\(componentsDhuhr.minute)")
                
                if alarmStatusDhuhr == "1"
                {
                    setLocalNotifications(prayerName: "Dhuhr", hour: componentsDhuhr.hour ?? 00, minute: componentsDhuhr.minute ?? 00,indexx: index, dateComponent: componentsDhuhr)
                }
                
                let datetimeAsr = formatter.date(from: formatter.string(from: prayers.asr))
                let componentsAsr = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datetimeAsr!)
                //print("----------------Asr-----------------------------------------")
                //print("PrayersTime\(prayers.asr)")
                //print("String-Format-Asr-->\(formatter.string(from: prayers.asr))")
                //print("String-to-date-format--->\(datetimeAsr!)")
                //print("Date-to-dateComponent---->\(componentsAsr)")
                //print("Date-to-dateComponent-H:M->\(componentsAsr.hour)--\(componentsAsr.minute)")
                
                if alarmStatusAsr == "1"
                {
                    setLocalNotifications(prayerName: "Asr", hour: componentsAsr.hour ?? 00, minute: componentsAsr.minute ?? 00,indexx: index, dateComponent: componentsAsr)
                }
                
                let datetimeMaghrib = formatter.date(from: formatter.string(from: prayers.maghrib))
                let componentsMaghrib = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datetimeMaghrib!)
                //print("----------------Magrib-----------------------------------------")
                //print("PrayersTime\(prayers.maghrib)")
                //print("String-Format-Maghrib-->\(formatter.string(from: prayers.maghrib))")
                //print("String-to-date-format--->\(datetimeMaghrib!)")
                //print("Date-to-dateComponent---->\(componentsMaghrib)")
                //print("Date-to-dateComponent-H:M->\(componentsMaghrib.hour)--\(componentsMaghrib.minute)")
                
                if alarmStatusMaghrib == "1"
                {
                    setLocalNotifications(prayerName: "Maghrib", hour: componentsMaghrib.hour ?? 00, minute: componentsMaghrib.minute ?? 00,indexx: index, dateComponent: componentsMaghrib)
                }
                
                let datetimeIsha = formatter.date(from: formatter.string(from: prayers.isha))
                let componentsIsha = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: datetimeIsha!)
                //print("----------------Isha-----------------------------------------")
                //print("PrayersTime\(prayers.isha)")
                //print("String-Format-Isha-->\(formatter.string(from: prayers.isha))")
                //print("String-to-date-format--->\(datetimeIsha!)")
                //print("Date-to-dateComponent---->\(componentsIsha)")
                //print("Date-to-dateComponent-H:M->\(componentsIsha.hour)--\(componentsIsha.minute)")
                
                
                if alarmStatusIsha == "1"
                {
                    setLocalNotifications(prayerName: "Isha", hour: componentsIsha.hour ?? 00, minute: componentsIsha.minute ?? 00,indexx: index, dateComponent: componentsIsha)
                }
                
                
                
            }
        }
        
        getLocalNotificationCount()
        
    }
    
}

func getNumberOfPrayers() -> Int {
    
    let arrMore2 = ["Fajr","Sunrise","Dhuhr","Asr","Maghrib","Isha"]
    var totalcount: Int = 0
    
    for indexx in (0...5) {
        let alarmStatus = getNamazAlarmValue(index: indexx, namaz: arrMore2[indexx])
        if alarmStatus == "1"
        {
            totalcount += 1
        }
        
    }
    
    return totalcount
}

func setLocalNotifications(prayerName: String, hour: Int, minute: Int, indexx: Int, dateComponent: DateComponents)
{
    let audioArr = ["None", "Silent", "Default Notification Sound", "azalaqsa.m4a", "Azan Makkah Fajr", "Azan Madina Fajr", "Azan Turkish"]
    //print("Prayer-Name---- \(prayerName)")
    //print("Prayer-Hour---- \(hour)")
    //print("Prayer-Minute---- \(minute)")
    //print("Index----- \(indexx)")
    //print("---------------------------------------------")
    //print("Date-Component----- \(dateComponent)")
    
    let center = UNUserNotificationCenter.current()
    
    let content = UNMutableNotificationContent()
    
    content.title = "\(prayerName) prayer"
    content.body = "Its time for \(prayerName) prayer"
    
    /*if index == 1
    {
        content.title = "Fajr prayer"
        content.body = "Its time for Fajr prayer"
    }
    else{
        content.title = "Sunrise"
        content.body = "Its time for Sunrise"
    }*/
    
    //let path = Bundle.main.path(forResource: "azan_fajr_makkah", ofType: "mp3")!
    //let soundName = UNNotificationSoundName("az_alaqsa.aiff")
    //content.sound = UNNotificationSound(named: soundName)
    //content.sound = UNNotificationSound.default
    
    let currPrayerStatus = getValueForKey(keyValue: "Sound-" + prayerName)
    //print("Current-Prayer-Status > \(currPrayerStatus)")
    
    switch currPrayerStatus {
    case "0" :
        content.sound = UNNotificationSound.default
    case "1" :
        content.sound = UNNotificationSound.default
    case "2":
        content.sound = UNNotificationSound.default
    case "3":
        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "azalaqsa.m4a"))
    case "4":
        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "azfjrmakkah.m4a"))
    case "5":
        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "azfjrmadinah.m4a"))
    case "6":
        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "azturk.m4a"))
    default:
        content.sound = UNNotificationSound.default
    }
    
    
    content.categoryIdentifier = "\(prayerName)\(hour)\(minute)\(indexx)"
    content.userInfo = ["example": "information"] // You can retrieve this when displaying notification
    
    // Setup trigger time
    var calendar = Calendar.current
    calendar.timeZone = TimeZone.current
    
    /*var dateComponents = DateComponents()
    dateComponents.calendar = Calendar.current
    dateComponents.hour = hour
    
    dateComponents.minute = minute*/
    
    // Set this to whatever date you need
    let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
    
    // Create request
    let uniqueID = UUID().uuidString
    //print("UniqueID---> \(uniqueID)")// Keep a record of this if necessary
    let request = UNNotificationRequest(identifier: uniqueID, content: content, trigger: trigger)
    center.add(request, withCompletionHandler: nil) // Add the notification request
    
    
}

func initiateNotificationSetupForFirsttime(){
    let cc = UNUserNotificationCenter.current()
    var nCount: Int = 0
    //print("-----------------------------------------------")
    cc.getPendingNotificationRequests { (notifications) in
        print("notification-count----before--> \(notifications.count)")
        nCount = notifications.count
        /*for item in notifications {
          print(item.content)
        }*/
    }
    
    DispatchQueue.main.asyncAfter(deadline: .now() +  5.0, execute: {
          if nCount == 0
           {
               let prayerCount = getNumberOfPrayers()
               //print("prayer-count----> \(prayerCount)")
               //print("prayer-count---->------->")
               setInitialPrayersAlarms()
               setInitialAzaanAudios()
               setAdhanTimingsNotifications()
               
               let nCount1 = getLocalNotificationCount()
               print("notification-count----after--> \(nCount1)")
           }
    })
    
   
}

//MARK:- Method for Reset Notification when Notification count is zero
func resetNotificationsIfCountZero(){
    let cc = UNUserNotificationCenter.current()
    var nCount: Int = 0
    //print("---resetNotificationsIfCountZero------")
    cc.getPendingNotificationRequests { (notifications) in
        //print("notification-count----before--> \(notifications.count)")
        nCount = notifications.count
    }
    
    DispatchQueue.main.asyncAfter(deadline: .now() +  5.0, execute: {
        
        resetPrayerNotificationsOnSettingsChange()
         /* if nCount == 0
           {
               let prayerCount = getNumberOfPrayers()
               //print("prayer-count----> \(prayerCount)")
               //print("prayer-count---->------->")
               
               setAdhanTimingsNotifications()
               
               let nCount1 = getLocalNotificationCount()
               //print("notification-count----after--> \(nCount1)")
           }*/
    })
}

//MARK:- Method for Reset Prayer Notification from Settings Screen
func resetPrayerNotificationsOnSettingsChange(){
    deleteAllNotifications()
    setAdhanTimingsNotifications()
}

//MARK:- Method for copying database
func copyDatabaseIfNeeded() {
    // Move database file from bundle to documents folder
    
    let fileManager = FileManager.default
    
    let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
    
    guard documentsUrl.count != 0 else {
        return // Could not find documents URL
    }
    
    let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("Allah_names.sqlite")
    //print("Database Path--before-- \(finalDatabaseURL)")
    
    if !( (try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
        print("DB does not exist in documents folder")
        
        let documentsURL = Bundle.main.resourceURL?.appendingPathComponent("Allah_names.sqlite")
        
        do {
            try fileManager.copyItem(atPath: (documentsURL?.path)!, toPath: finalDatabaseURL.path)
            
            print("Database Path--between-- \(finalDatabaseURL)")
            
            setDefaultValue(keyValue: "DatabasePath", valueIs: finalDatabaseURL.path)
        } catch let error as NSError {
            print("Couldn't copy file to final location! Error:\(error.description)")
        }
        
        
        
    } else {
        print("Database file found at path: \(finalDatabaseURL.path)")
    }
    
}

//MARK:- Table Names For Languages
let TABLE_ENGLISH = "translation_eng"
let TABLE_URDU = "tr_urdu"
let TABLE_INDONESIAN = "tr_indonesian"

let TABLE_BENGALI = "tr_bengali"
let TABLE_GERMAN = "tr_german"
let TABLE_HINDI = "tr_hindi"
let TABLE_MALAYSIAN = "tr_malaysian"
let TABLE_NORWEGIAN = "tr_norwegian"
let TABLE_RUSSIAN = "tr_russian"
let TABLE_CHINESE = "tr_chinese"
let TABLE_TAMIL = "tr_tamil"

let TABLE_ALBANIAN = "tr_albanian"
let TABLE_BOSNIAN = "tr_bosnian"
let TABLE_CZECH = "tr_czech"
let TABLE_DUTCH = "tr_dutch"
let TABLE_ITALIAN = "tr_italian"
let TABLE_JAPANESE = "tr_japanese"
let TABLE_NIGERIAN = "tr_nigerian"
let TABLE_PASHTO = "tr_pashto"
let TABLE_PERSIAN = "tr_persian"
let TABLE_POLISH = "tr_polish"
let TABLE_PORTUGHESE = "tr_portughes"
let TABLE_ROMANIAN = "tr_romanian"
let TABLE_SOMALI = "tr_somali"
let TABLE_SPANISH = "tr_spanish"
let TABLE_SWEDISH = "tr_swedish"
let TABLE_TATAR = "tr_tatar"
let TABLE_THAI = "tr_thai"
let TABLE_TURKISH = "tr_turkish"
let TABLE_UZBEK = "tr_uzbuk"

let baseURLanguages = "https://inabia.com/Apps/languages/download.php?filename="

//MARK:- Languages Url
let url_albanian = baseURLanguages + "tr_albanian"
let url_bosnain = baseURLanguages + "tr_bosnian"
let url_czech = baseURLanguages + "tr_czech"
let url_dutch = baseURLanguages + "tr_dutch"
let url_italian = baseURLanguages + "tr_italian"
let url_japanese = baseURLanguages + "tr_japanese"
let url_nigerian = baseURLanguages + "tr_nigerian"
let url_pashto = baseURLanguages + "tr_pashto"
let url_persian = baseURLanguages + "tr_persian"
let url_polish = baseURLanguages + "tr_polish"
let url_portughese = baseURLanguages + "tr_portughes"
let url_romanian = baseURLanguages + "tr_romanian"
let url_somali = baseURLanguages + "tr_somali"
let url_spanish = baseURLanguages + "tr_spanish"
let url_swedish = baseURLanguages + "tr_swedish"
let url_tatar = baseURLanguages + "tr_tatar"
let url_thai = baseURLanguages + "tr_thai"
let url_turkish = baseURLanguages + "tr_turkish"
let url_uzbek = baseURLanguages + "tr_uzbuk"
let url_bengali = baseURLanguages + "tr_bengali"
let url_german = baseURLanguages + "tr_german"
let url_hindi = baseURLanguages + "tr_hindi"
let url_malaysian = baseURLanguages + "tr_malaysian"
let url_norway = baseURLanguages + "tr_norwegian"
let url_russian = baseURLanguages + "tr_russian"
let url_chinese = baseURLanguages + "tr_chinese"
let url_tamil = baseURLanguages + "tr_tamil"
let url_urdu = baseURLanguages + "tr_urdu"
let url_indonesian = baseURLanguages + "tr_indonesian"

//MARK:- Method for conversion 24 hours
func timeConversion24(time12: String) -> String {
    let dateAsString = time12
    let df = DateFormatter()
    df.dateFormat = "hh:mm a"

    let date = df.date(from: dateAsString)
    df.dateFormat = "HH:mm"

    let time24 = df.string(from: date!)
    return time24
}

func languageUrl(language: String) -> String{
    let baseURL = "https://inabia.com/Apps/languages/download.php?filename=" + language
    return baseURL
}

//MARK:- Method for checking file exist or not
func checkFileExist(fileName: String) -> Bool {
    
    var isExist: Bool = false
    let fileManager = FileManager.default
    
    let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
    
    guard documentsUrl.count != 0 else {
        return false// Could not find documents URL
    }
    
    let finalFileURL = documentsUrl.first!.appendingPathComponent(fileName)
    print("File Path--before-- \(finalFileURL)")
    
    if !( (try? finalFileURL.checkResourceIsReachable()) ?? false)
    {
        print("File does not exist in documents")
        isExist = false
    }
    else
    {
        print("File found at path: \(finalFileURL.path)")
        isExist = true
    }
    
    return isExist
}

func deleteAllNotifications(){
    let center = UNUserNotificationCenter.current()
    //center.removeAllDeliveredNotifications()    // to remove all delivered notifications
    center.removeAllPendingNotificationRequests()   // to remove all pending notifications
    UIApplication.shared.applicationIconBadgeNumber = 0 // to clear the icon notification badge
    
    let nCount1 = getLocalNotificationCount()
    print("notification-count----after-delete-> \(nCount1)")
}

//MARK:- Method to get country code my sending short address
func getCountryPhonceCode (_ country : String) -> String {
    var countryDictionary  = ["AF":"+93",
                              "AL":"+355",
                              "DZ":"+213",
                              "AS":"+1",
                              "AD":"+376",
                              "AO":"+244",
                              "AI":"+1",
                              "AG":"+1",
                              "AR":"+54",
                              "AM":"+374",
                              "AW":"+297",
                              "AU":"+61",
                              "AT":"+43",
                              "AZ":"+994",
                              "BS":"+1",
                              "BH":"+973",
                              "BD":"+880",
                              "BB":"+1",
                              "BY":"+375",
                              "BE":"+32",
                              "BZ":"+501",
                              "BJ":"+229",
                              "BM":"+1",
                              "BT":"+975",
                              "BA":"+387",
                              "BW":"+267",
                              "BR":"+55",
                              "IO":"+246",
                              "BG":"+359",
                              "BF":"+226",
                              "BI":"+257",
                              "KH":"+855",
                              "CM":"+237",
                              "CA":"+1",
                              "CV":"+238",
                              "KY":"+345",
                              "CF":"+236",
                              "TD":"+235",
                              "CL":"+56",
                              "CN":"+86",
                              "CX":"+61",
                              "CO":"+57",
                              "KM":"+269",
                              "CG":"+242",
                              "CK":"+682",
                              "CR":"+506",
                              "HR":"+385",
                              "CU":"+53",
                              "CY":"+537",
                              "CZ":"+420",
                              "DK":"+45",
                              "DJ":"+253",
                              "DM":"+1",
                              "DO":"+1",
                              "EC":"+593",
                              "EG":"+20",
                              "SV":"+503",
                              "GQ":"+240",
                              "ER":"+291",
                              "EE":"+372",
                              "ET":"+251",
                              "FO":"+298",
                              "FJ":"+679",
                              "FI":"+358",
                              "FR":"+33",
                              "GF":"+594",
                              "PF":"+689",
                              "GA":"+241",
                              "GM":"+220",
                              "GE":"+995",
                              "DE":"+49",
                              "GH":"+233",
                              "GI":"+350",
                              "GR":"+30",
                              "GL":"+299",
                              "GD":"+1",
                              "GP":"+590",
                              "GU":"+1",
                              "GT":"+502",
                              "GN":"+224",
                              "GW":"+245",
                              "GY":"+595",
                              "HT":"+509",
                              "HN":"+504",
                              "HU":"+36",
                              "IS":"+354",
                              "IN":"+91",
                              "ID":"+62",
                              "IQ":"+964",
                              "IE":"+353",
                              "IL":"+972",
                              "IT":"+39",
                              "JM":"+1",
                              "JP":"+81",
                              "JO":"+962",
                              "KZ":"+77",
                              "KE":"+254",
                              "KI":"+686",
                              "KW":"+965",
                              "KG":"+996",
                              "LV":"+371",
                              "LB":"+961",
                              "LS":"+266",
                              "LR":"+231",
                              "LI":"+423",
                              "LT":"+370",
                              "LU":"+352",
                              "MG":"+261",
                              "MW":"+265",
                              "MY":"+60",
                              "MV":"+960",
                              "ML":"+223",
                              "MT":"+356",
                              "MH":"+692",
                              "MQ":"+596",
                              "MR":"+222",
                              "MU":"+230",
                              "YT":"+262",
                              "MX":"+52",
                              "MC":"+377",
                              "MN":"+976",
                              "ME":"+382",
                              "MS":"+1",
                              "MA":"+212",
                              "MM":"+95",
                              "NA":"+264",
                              "NR":"+674",
                              "NP":"+977",
                              "NL":"+31",
                              "AN":"+599",
                              "NC":"+687",
                              "NZ":"+64",
                              "NI":"+505",
                              "NE":"+227",
                              "NG":"+234",
                              "NU":"+683",
                              "NF":"+672",
                              "MP":"+1",
                              "NO":"+47",
                              "OM":"+968",
                              "PK":"+92",
                              "PW":"+680",
                              "PA":"+507",
                              "PG":"+675",
                              "PY":"+595",
                              "PE":"+51",
                              "PH":"+63",
                              "PL":"+48",
                              "PT":"+351",
                              "PR":"+1",
                              "QA":"+974",
                              "RO":"+40",
                              "RW":"+250",
                              "WS":"+685",
                              "SM":"+378",
                              "SA":"+966",
                              "SN":"+221",
                              "RS":"+381",
                              "SC":"+248",
                              "SL":"+232",
                              "SG":"+65",
                              "SK":"+421",
                              "SI":"+386",
                              "SB":"+677",
                              "ZA":"+27",
                              "GS":"+500",
                              "ES":"+34",
                              "LK":"+94",
                              "SD":"+249",
                              "SR":"+597",
                              "SZ":"+268",
                              "SE":"+46",
                              "CH":"+41",
                              "TJ":"+992",
                              "TH":"+66",
                              "TG":"+228",
                              "TK":"+690",
                              "TO":"+676",
                              "TT":"+1",
                              "TN":"+216",
                              "TR":"+90",
                              "TM":"+993",
                              "TC":"+1",
                              "TV":"+688",
                              "UG":"+256",
                              "UA":"+380",
                              "AE":"+971",
                              "GB":"+44",
                              "US":"+1",
                              "UY":"+598",
                              "UZ":"+998",
                              "VU":"+678",
                              "WF":"+681",
                              "YE":"+967",
                              "ZM":"+260",
                              "ZW":"+263",
                              "BO":"+591",
                              "BN":"+673",
                              "CC":"+61",
                              "CD":"+243",
                              "CI":"+225",
                              "FK":"+500",
                              "GG":"+44",
                              "VA":"+379",
                              "HK":"+852",
                              "IR":"+98",
                              "IM":"+44",
                              "JE":"+44",
                              "KP":"+850",
                              "KR":"+82",
                              "LA":"+856",
                              "LY":"+218",
                              "MO":"+853",
                              "MK":"+389",
                              "FM":"+691",
                              "MD":"+373",
                              "MZ":"+258",
                              "PS":"+970",
                              "PN":"+872",
                              "RE":"+262",
                              "RU":"+7",
                              "BL":"+590",
                              "SH":"+290",
                              "KN":"+1",
                              "LC":"+1",
                              "MF":"+590",
                              "PM":"+508",
                              "VC":"+1",
                              "ST":"+239",
                              "SO":"+252",
                              "SJ":"+47",
                              "SY":"+963",
                              "TW":"+886",
                              "TZ":"+255",
                              "TL":"+670",
                              "VE":"+58",
                              "VN":"+84",
                              "VG":"+284",
                              "VI":"+340"]
    if countryDictionary[country] != nil {
        return countryDictionary[country]!
    }
        
    else {
        return ""
    }
    
}
