//
//  OSQiblahDirection.swift
//  IslamicBundleSwift
//
//  Created by Inabia1 on 22/06/2020.
//  Copyright © 2020 Umar Farooq. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class CompassDirectionManager: NSObject , CLLocationManagerDelegate{
    
    var dialerImageView : UIImageView!
    var pointerImageView : UIImageView!
    
    private var latOfOrigin = 21.4225
    private var lngOfOrigin = 39.8262
    
    var lat: Double?
    var lng: Double?
    
    private var location: CLLocation?
    
    private let locationManager = CLLocationManager()
    
    var bearingOfKabah = Double()
    
    init(dialerImageView : UIImageView , pointerImageView : UIImageView) {
        self.dialerImageView = dialerImageView
        self.pointerImageView = pointerImageView
    }
    
    
    func initManager(){
        
        locationManager.requestAlwaysAuthorization()
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            
        }
    }
    
    func setOrigin(lat : Double , lng : Double){
        self.latOfOrigin = lat
        self.lngOfOrigin = lng
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        
        
        let north = -1 * heading.magneticHeading * Double.pi/180
        
        let directionOfKabah = bearingOfKabah * Double.pi/180 + north
        
        /*let oldRad: Double = -manager.heading!.trueHeading * .pi / 180.0
        let newRad: Double = -heading.trueHeading * .pi / 180.0
        var theAnimation: CABasicAnimation?
        theAnimation = CABasicAnimation(keyPath: "transform.rotation")
        theAnimation?.fromValue = NSNumber(value: oldRad)
        theAnimation?.toValue = NSNumber(value: newRad)*/
        //tmpRadius = newRad

        /*theAnimation?.duration = 0.02

        if let theAnimation = theAnimation {
            dialerImageView.layer.add(theAnimation, forKey: "animateMyRotation")
            pointerImageView.layer.add(theAnimation, forKey: "animateMyRotation")
        }*/
        
        UIView.animate(withDuration: 0.5) {
          let angle = heading.trueHeading.degreesToRadians // convert from degrees to radians
            //self.dialerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(angle)) // rotate the picture
            
            self.dialerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
            
            self.pointerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(directionOfKabah))
        }
        
       // dialerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(north));
        
        //pointerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(directionOfKabah));
    
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let newLocation = locations.last!
        
        location = newLocation
        
        bearingOfKabah = getBearingBetweenTwoPoints1(location!, latitudeOfOrigin: self.latOfOrigin, longitudeOfOrigin: self.lngOfOrigin) //calculating the bearing of KABAH
    }
    
    
    private func degreesToRadians(_ degrees: Double) -> Double { return degrees * Double.pi / 180.0 }
    
    
    private  func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / Double.pi }
    
    private  func getBearingBetweenTwoPoints1(_ point1 : CLLocation, latitudeOfOrigin : Double , longitudeOfOrigin :Double) -> Double {
        
        if isKeyPresentInUserDefaults(key: "Latitude"){
            print("Exist")
            lat = Double (getValueForKey(keyValue: "Latitude"))
            lng = Double (getValueForKey(keyValue: "Longitude"))
        }
        else{
            print("Not Exist")
            lat = 21.3891
            lng = 39.8579
        }
        
        //10.7362, 61.5545
        
        let lat1 = degreesToRadians(lat!)
        let lon1 = degreesToRadians(lng!)
        
        /*let lat1 = degreesToRadians(point1.coordinate.latitude)
        let lon1 = degreesToRadians(point1.coordinate.longitude)*/
        
        let lat2 = degreesToRadians(latitudeOfOrigin);
        let lon2 = degreesToRadians(longitudeOfOrigin);
        
        let dLon = lon2 - lon1;
        
        let y = sin(dLon) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        var radiansBearing = atan2(y, x);
        if(radiansBearing < 0.0){
            
            radiansBearing += 2 * Double.pi;
            
        }
        
        return radiansToDegrees(radiansBearing)
    }
}
extension FloatingPoint {
    var degreesToRadians: Self { self * .pi / 180 }
    var radiansToDegrees: Self { self * 180 / .pi }
}

extension CGFloat {
  var toRadians: CGFloat { return self * .pi / 180 }
  var toDegrees: CGFloat { return self * 180 / .pi }
}
