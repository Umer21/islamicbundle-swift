//
//  ViewController.swift
//  IslamicBundleSwift
//
//  Created by Muhammad Umair Qureshi on 4/25/19.
//  Copyright © 2019 Umar Farooq. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaults.standard
        
        if !userDefaults.bool(forKey: "walkthroughPresented") {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0 , execute: {
               
                self.gotoWalkThroughScreen()
            })
            
            userDefaults.set(true, forKey: "walkthroughPresented")
            userDefaults.synchronize()
        }
        else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0 , execute: {
                self.gotoHomeScreen()
                
            })
        }
        
    }
    
    func gotoWalkThroughScreen() {
        
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let tabVC = storyboard.instantiateViewController(withIdentifier: kOnboardParentControllerID) as! OnBoardParentVC
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = tabVC
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func gotoHomeScreen() {
        
        let storyboard = UIStoryboard(name: kMainStoryboard, bundle: nil)
        let tabVC = storyboard.instantiateViewController(withIdentifier: kTabControllerID) as! TabbarVC
        
        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        window?.rootViewController = tabVC
        UIView.transition(with: window!, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Sets the status bar to hidden when the view has finished appearing
        UIApplication.shared.isStatusBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        // Sets the status bar to visible when the view is about to disappear
       UIApplication.shared.isStatusBarHidden = false
    }
    
    


}

